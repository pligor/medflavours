<?php
/**
 * @author pligor 
 */
class BackboneController extends Controller
{
	public function actionCrud($action,$class) {
		if($action == 'create') {
		
			$model = new $class();
			$model->attributes = $_GET[$class];	//ALL are included WITH foreign key ;)

			if($model->save()){
				print json_encode( array('id' => $model->id) );
				return;
			}

			throw new CException('failed to create new '.$class);
			/*$method = $_SERVER['REQUEST_METHOD'];
			if($method == 'POST'){
				print $GLOBALS['HTTP_RAW_POST_DATA'];
			}*/
		}
		
		if($action == 'delete') {
			//var_dump($_GET);return;
			$id = $_GET['id'];
			
			$id = Model::str2yii_id($id, $class);
			
			$result = $class::model()->deleteByPk($id); //how many lines were deleted. zero means failed to delete
			
			if($result != 1) {
				throw new CException('failed to delete '.$class.' with id: '.$id);
			}
		}
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}