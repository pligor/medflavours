<?php
class ActivityPeriodController extends Controller
{
	public function init() {
		parent::init();
		$this->defaultAction = null;	//DISABLE default action operation
	}
	
	protected function getModel($id) {
		$class = ucfirst($this->id);
		if($id === null) {
			$model = new $class();
		}
		else {
			$model = Model::load($class, $id);
		}
		return $model;
	}
	
	public function actionRender($id=null) {
		$model = $this->getModel($id);
		$this->renderPartial($this->action->id, compact('model'), false, true);	//process output
	}
	
	public function actionRender_line($id=null) {
		$model = $this->getModel($id);
		$this->renderPartial($this->action->id, compact('model'), false, true);	//process output
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}