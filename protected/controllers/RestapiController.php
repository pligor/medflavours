<?php
/**
 * @todo take care of all the cases with composite keys
 */
class RestapiController extends Controller
{
	//Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers 
     */
    const APPLICATION_ID = 'ASCCPE';
	
	/**
	 * Default response format. either 'json' or 'xml'
	 * @var string
	 */
	protected $format = 'json';
	
	/**
	 * @var string 
	 */
	protected $class = null;
	
	public function init() {
		parent::init();
		$this->defaultAction = 'crud';
	}
	
	/**
	 * Note that for all requests, we will get the requested model (e.g. posts) via the GET model parameter.
	 * For the Get Single Item and Update Item method, we will receive the model's primary key via the GET id parameter.
	 */
	public function actionCrud($class) {
		if(strpos($class, '/') !== false) {
			list($this->class, $id) = explode('/', $class);
		}
		else {
			$this->class = $class;
		}
		
		$requestType = Yii::app()->getRequest()->getRequestType(); //$_SERVER['REQUEST_METHOD'];
		if($requestType == 'POST') {
			$this->create(); //print "we are about to create $class";
		}
		elseif($requestType == 'DELETE') {
			if(!isset($id)) {
				$this->sendResponse(500, 'Error: Parameter <b>id</b> is missing');
			}
			$this->delete($id);	//print "we are about to delete $class with id $id";
		}
		elseif($requestType == 'PUT') {
			if(!isset($id)) {
				$this->sendResponse(500, 'Error: Parameter <b>id</b> is missing');
			}
			$this->update($id);	//print "we are about to update $class with id $id";
		}
		elseif($requestType == 'GET') {
			if(isset($id)) {
				$this->view($id);	//print "we are about to get $class with id $id";
			}
			else {
				$this->index();	//print "we are about to get ALL $class s";
			}
		}
		else {
			$this->sendResponse(500, 'Error: not a valid request type' );
		}
	}
	
	protected function index() {		
		$class = $this->class;
		$models = $class::model()->findAll();
		if( Hlp::isEmpty($models) ) {
			$this->sendResponse( 200, sprintf('No items where found for model <b>%s</b>', $class) );
		}
		
		$rows = array();
		foreach($models as $model) {
			$rows[] = $model->attributes;
		}
		$this->sendResponse(200, CJSON::encode($rows));
	}
	
	protected function view($id) {
		$model = $this->getModel($id);
		$this->sendResponse(200, CJSON::encode($model));
	}
	
	protected function create() {
		$model = new $this->class();
		
		$model->attributes = $this->getAttributes();

		// Try to save the model
		if( $model->save() ) {
			
			//rewrite id, necessary for cases of composite keys, obviously necessary a function that
			//can take composite key and tranform it to a single key with underscore seperated for example
			$attributes = $model->attributes;
			$attributes['id'] = $model->id;	
			
			$this->sendResponse(200, CJSON::encode($attributes));
		}
		else { // Errors occurred
			$msg = $this->renderErrors($model->errors);
			$this->sendResponse(500, $msg);
		}
	}
	
	protected function update($id) {
		$model = $this->getModel($id);				
		
//parse_str($a1, $a2); //parse the PUT parameters //@todo mention it: PARSE STR ??? did not work for me
		
		$model->attributes = $this->getAttributes();
		
		if($model->save()) {
			$this->sendResponse(200, CJSON::encode($model));
		}
		else {
			$msg = $this->renderErrors($model->errors);
			$this->sendResponse(500, $msg);
		}
	}
	
	protected function delete($id) {
		//$this->_sendResponse(501, sprintf('Error: Mode <b>delete</b> is not implemented for model <b>%s</b>',$_GET['model']) );
		$model = $this->getModel($id);
		
		$num = $model->delete();	//returns the number of rows deleted
		if($num>0) {
			$this->sendResponse(200, $num);	//this is the only way to work with backbone
		}
		else {
			$this->sendResponse(500, sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.", $this->class, $id) );
		}
	}
	
	private function sendResponse($status = 200, $body = '', $content_type = 'text/html') {
		$status_header = 'HTTP/1.1 '. $status .' '. $this->getStatusCodeMessage($status);	// set the status
		header($status_header);
		
		header('Content-type: '. $content_type);	// and the content type

		//pages with body are easy
		if($body != '') {
			print $body;	//send the body
		}
		else {	//we need to create the body if none is passed
			$message = '';	// create some body messages

			// this is purely optional, but makes the pages a little nicer to read
			// for your users.  Since you won't likely send a lot of different status codes,
			// this also shouldn't be too ponderous to maintain.
			// I hate switch statement since always some break statement will be forgotten
			if($status == 401) {
				$message = 'You must be authorized to view this page.';
			}
			elseif($status == 404) {
				$message = 'The requested URL '. Yii::app()->getRequest()->getRequestUri() .' was not found.';
			}
			elseif($status == 500) {
				$message = 'The server encountered an error processing your request.';
			}
			elseif($status == 501) {
				$message = 'The requested method is not implemented.';
			}

			// servers don't always have a signature turned on (this is an apache directive "ServerSignature On")
			if( isset($_SERVER['SERVER_SIGNATURE']) && $_SERVER['SERVER_SIGNATURE'] == '') {
				$signature = $_SERVER['SERVER_SIGNATURE'];
			}
			else {
				$signature = $_SERVER['SERVER_SOFTWARE'] .' Server at '. Yii::app()->getRequest()->getServerName() .' Port '. Yii::app()->getRequest()->getServerPort();
			}

			print $this->renderPartial('crud', compact('message','status','signature'), false, true);
			
		}
		Yii::app()->end();
	}
	
	protected function getStatusCodeMessage($status) {
		$codes = array(
			200 => 'OK',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
		);
		return (isset($codes[$status])) ? $codes[$status] : '';
	}
	
	private function getModel($id) {
		$class = $this->class;
		$id = Model::str2yii_id($id, $class);
		$model = $class::model()->findByPk($id);
		
		if($model === null) {
			$this->sendResponse(404, 'No '. $class .' found with id '. $id);
		}
		return $model;
	}
	
	/**
	 * What it does: checks is any of the attributes does not belong to the current model
	 * What id does NOT: check if all the necessary attributes to save are present (this is checked upon actual save)
	 * @return type 
	 */
	private function getAttributes() {
		$model = new $this->class();
		
		$json = file_get_contents('php://input'); //$GLOBALS['HTTP_RAW_POST_DATA'] is not preferred: http://www.php.net/manual/en/ini.core.php#ini.always-populate-raw-post-data
		
		$postedAttrs = CJSON::decode($json,true);	//true means use associative array
		
		foreach($postedAttrs as $attrName => $postedAttr) {
			if( !$model->hasAttribute($attrName) ) {
				$this->sendResponse(500, sprintf('Parameter <b>%s</b> is not allowed for model <b>%s</b>', $attrName, $this->class));
			}
		}
		
		return $postedAttrs;
	}
	
	private function renderErrors($errors) {
		$msg = "<h1>Error</h1>";
		$msg .= sprintf("with model <b>%s</b>", $this->class);
		$msg .= "<ul>";
		foreach($errors as $attribute => $attr_errors) {
			$msg .= "<li>Attribute: $attribute</li>";
			$msg .= "<ul>";
			foreach($attr_errors as $attr_error) {
				$msg .= "<li>$attr_error</li>";
			}
			$msg .= "</ul>";
		}
		$msg .= "</ul>";
		return $msg;
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}