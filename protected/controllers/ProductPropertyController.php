<?php

class ProductPropertyController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionRender_line($id=null) {
		$class = ucfirst($this->id);
		
		if($id === null) {
			$model = new $class();
		}
		else {
			$model = Model::load($class, $id);
		}
		
		//$this->render('render',compact('form'));
		$this->renderPartial('render_line', compact('model'), false, true);	//process output
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}