<?php

/**
 * Entry point for admin module
 * @todo REMEMBER to remove force copy from getAssetsUrl method in WebModule on production version
 */
class AdminModule extends WebModule {

    /**
     *Define the default controller for the module
     * Defaults to 'Site'
     * @var string
     */
    public $defaultController = 'Main';
    
    public function init() {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'admin.models.*',
            'admin.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
			//GENERIC
			$url = $this->getJsUrl() .'/generic.js';
			Yii::app()->clientScript->registerScriptFile($url);
			$url = $this->getJsLibraryUrl() .'/prototypes.js';
			Yii::app()->clientScript->registerScriptFile($url);
			
			//JQUERY PLUGINS
			$url = $this->getJsLibraryUrl().'/jquery.tmpl.js';
            //Yii::app()->clientScript->registerScriptFile($url);
			FileHelper::safeRegisterScriptFile($url);
			
			$url = $this->getJsLibraryUrl().'/jquery.tooltip.js';
            Yii::app()->clientScript->registerScriptFile($url);
			$url = $this->getJsLibraryUrl().'/jquery.tooltip.css';
			Yii::app()->clientScript->registerCssFile($url);
			
			//BACKBONE
			$url = $this->getJsLibraryUrl().'/underscore.js';
            Yii::app()->clientScript->registerScriptFile($url);
			$url = $this->getJsLibraryUrl().'/json2.js';
            Yii::app()->clientScript->registerScriptFile($url);
			
			$url = $this->getJsLibraryUrl().'/backbone.js';
            Yii::app()->clientScript->registerScriptFile($url);
			//FileHelper::safeRegisterScriptFile($url);
			
			$url = $this->getJsLibraryUrl().'/jquery.tipsy.js';
			FileHelper::safeRegisterScriptFile($url);
			
            return true;
        }
        else {
            return false;
		}
    }

	public function getJsUrl() {
		return $this->getAssetsUrl() .'/javascript';
	}
	
	public function getJsLibraryUrl() {
		return $this->getJsUrl() .'/library';
	}
}
