<?php

/**
 * A set of utility functions pertaining specifically to admin 
 */
class AdminUtils extends Utils {

    /**
     *
     * @param type $file
     */
    public static function registerAdminCssFile($file, $media = '') {
        _cs()->registerCSSFile(self::_adminAssetUrl("css/{$file}.css"), $media);
    }

    /**
     *
     * @param type $file
     */
    public static function registerAdminJsFile($file) {
        _cs()->registerScriptFile(self::_adminAssetUrl("js/{$file}.js"));
    }

    /**
     *
     * @param string $asset
     * @return string
     */
    private static function _adminAssetUrl($asset='') {
        $alias = "admin.assets";
        $assetUrl = _app()->assetManager->publish(Yii::getPathOfAlias($alias), false, -1, Debug::isDebug()) . '/' . $asset;
        return $assetUrl;
    }

    /**
     *
     * @param string $image
     * @return string The url of the image asset
     */
    public static function adminImageUrl($image) {
        return self::_adminAssetUrl("images/$image");
    }

    /**
     * Create an admin URL
     * @param string $route
     * @param array $params
     * @param string $ampersand
     * @return string A url string
     */
    public static function aUrl($route, $params=array(), $ampersand='&') {
        return Yii::app()->createUrl('admin/'.$route, $params, $ampersand);
    }

}

?>
