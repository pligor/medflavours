//$(document).ready(function() {
	FormInput = Backbone.Model.extend({
		label: 'label',
		model_name: null,
		attr_name: null,
		type: 'text',
		value: null
	});
	
	FormInputView = Backbone.View.extend({
		//model is passed as a parameter to the constructor
		tagName: 'div',
		itsTmpl: null,
		events: {
			//'click a': 'clicked'
			'change' : 'changed'
		},
		initialize: function() {
			_.bindAll(this,'getValue', 'setValue', 'changed');
			this.model.on('change:value',this.setValue);
			this.itsTmpl = $('script#input_tmpl');		//remember to include template script in final html
		},
		changed: function(ev) {
			//ev.preventDefault();
			
			var value = this.getValue();
			
			this.model.set({
				value: this.getValue()
			},{
				silent: true
			});
			
			//console.log(this.model.get('value'));
			//console.log(this.model);
		},
		render: function() {
			var el = $(this.el);
			el.addClass('row');
			el.css('display','inline-block');
			var html = this.itsTmpl.tmpl(this.model.toJSON());
			el.append(html);
			
			//console.log(this.el);
			
			return this;
		},
		getValue: function() {
			var input = $(this.el).children('input');
			return input.val();
		},
		setValue: function(model, value) {
			//console.log(value);
			//console.log(this.model);
			var input = $(this.el).children('input');
			input.val(value);
		}
		/*,
		removeItem: function() {
			//console.log('remove now the li');
			this.remove();
		}*/
	});
	
	FormInputs = Backbone.Collection.extend({
		model : FormInput,
		initialize: function() {
			//_.bindAll(this);	//http://stackoverflow.com/questions/7887595/underscore-bindall-preserving-the-this-context
			_.bindAll(this,'add_from_row');
		},
		add_from_row: function(row) {	//NOT USED
			//console.log(row);
			row = $(row);
			var label = row.children('label');
			var input = row.children('input');
			
			var label_value = label.text();
			var input_type = input.attr('type');
			var input_value = input.attr('value');	//might be an empty string
			//var input_type = input
			/*
			Participant.msgPanes.collection.add({
				id: target_name,
				target_name: target_name
			});
			//*/
		}
	});
	
	FormInputsView = Backbone.View.extend({
		//el is included in the constructor

		initialize: function() {
			this.collection = new FormInputs();			//will NOT pass as parameter

			_.bindAll(this,'add_forminput','remove_forminput');

			this.collection.on('add',this.add_forminput);
			this.collection.on('remove',this.remove_forminput);
			
			/*
			var rows = $(this.el).children('div');
			rows.each(_.bind(function(key,row){
				this.collection.add_from_row(row);
			},this));
			*/
		},

		add_forminput: function(model) {
			//console.log("form input added at last !!");
			var formInputView = new FormInputView({model: model});
			formInputView.render();
			$(this.el).append( formInputView.el );
		},

		remove_forminput: function(model) {
		},

		render: function() {
			return this;
		}
	});
	
	FormLine = Backbone.Model.extend({
		//id: will be the model id
		forminputs: null,	//collection
		//action_button: null, //button for add or remove	//NOT USED
		initialize: function() {
			//this.set('forminputs', new FormInputsView());
			_.bindAll(this,'remove_db','add_db','formInputsFromJson');
		},
		remove_db: function() {
			if( this.isNew() ) {
				return;
			}
			
			$.ajax({
				url: this.collection.remove_action_url,
				data: {
					id: this.id
				},
				method: 'GET',
				success: _.bind(function(response) {
					if(response==0) {
						alert("deletion from db failed");
						return;
					}
					
					this.collection.remove(this);
					//console.log(response);
				},this),
				error: function() {
					alert("ajax failed");
				}
			});
		},
		add_db: function(input_data) {
			//console.log(this.collection);
			
			var data = merge_options(input_data,this.collection.extra_params);
			
			$.ajax({
				url: this.collection.add_action_url,
				data: data,
				method: 'GET',
				success: _.bind(function(response) {
					if(response == "") {
						alert("addition to db failed");
						return;
					}
					
					var obj = $.parseJSON(response);
					
					//obj
					//console.log(obj); return;
					
					var model = new FormLine({
						id: obj.id
					});

					//*
					this.collection.add(model);
					
					model.formInputsFromJson(obj.inputs);
					
					this.trigger('added_new_model');
				},this),
				error: function() {
					alert("ajax failed");
				}
			});
		},
		formInputsFromJson: function(json) {
			for(var attrname in json) {
				//console.log(json[attrname]);
				this.get('forminputs').collection.add(json[attrname]);
			}
		}
	});
	
	FormLineView = Backbone.View.extend({
		//model is passed as a parameter to the constructor ;)
		tagName: 'div',
		buttonTmpl: null,	//we do not use a template
		events: {
			//'click input': 'button_clicked'
		},

		initialize: function() {
			_.bindAll(this,'render_button','button_clicked','removed','clearInputs');
			
			this.model.on('remove',this.removed);
			this.model.on('added_new_model',this.clearInputs);
			
			this.buttonTmpl = $('script#button_tmpl');		//remember to include template script in final html
		},
		clearInputs: function() {
			//console.log( this.model.get('forminputs') );
			this.model.get('forminputs').collection.each(function(forminput){
				//console.log(forminput);				
				//console.log( forminput.get('value') );
				forminput.set({
					value: ' '	//@todo FOR SOME BIZARRE REASON IF THE STRING IS "" THE SET EVENT IS NOT FIRED (here we use single space)
				});
				//console.log( forminput.get('value') );
			});
		},
		removed: function() {
			this.remove();
		},
		button_clicked: function(ev) {
			ev.preventDefault();
			
			if(this.model.isNew()) {
				var input_data = grabInputData($(this.el));	//@todo LATER GRAB the values directly from the models! this is not backbone logic
				this.model.add_db(input_data);
			}
			else {
				this.model.remove_db();
			}
			
			//var username = this.model.get('username');
			//$(document).trigger('init_dm',{username: username});
		},
		render: function() {
			if(!this.model.isNew()) {
				$(this.el).attr('id', this.model.id);
			}
			
			//this.model.get('forminputs').el = this.el;	//CONNECT VIEWS
			this.model.set('forminputs', new FormInputsView({	//BETTER RECREATE VIEW
				el: this.el
			}));
			
			this.render_button();

			//console.log(this.model.get('forminputs').el);
			//$(this.el).append(html);
			return this;
		},
		render_button: function() {
			var alt;
			var src;
			if(this.model.isNew()) {
				alt = 'add';
				src = this.model.collection.add_icon_url;
			}
			else {
				alt = 'rem';
				src = this.model.collection.remove_icon_url;
			}
			
			var html = $('<div class="row buttons"></div>');
			html.css('display', 'inline-block');
			
			html.append( this.buttonTmpl.tmpl({
				alt: alt,
				src: src
			}) );
			
			//console.log(html);
			$(this.el).append(html);
			
			html.on('click',this.button_clicked);	//assign handler
		}
		/*,
		removeItem: function() {
			//console.log('remove now the li');
			this.remove();
		}*/
	});
	
	FormLines = Backbone.Collection.extend({
		model : FormLine,
		
		add_action_url: null,
		remove_action_url: null,
		
		add_icon_url: null,
		remove_icon_url: null,
		
		extra_params: null,
		
		initialize: function() {
			//_.bindAll(this,'add_from_row');
		}
	});
	
	FormLinesView = Backbone.View.extend({
		//el: //here is initialized from php

		initialize: function() {
			//this.collection = new FormLines();	//we pass it as parameter

			_.bindAll(this,'add_formline','remove_formline');

			this.collection.on('add',this.add_formline);
			this.collection.on('remove',this.remove_formline);
		},
		add_formline: function(model) {
			//console.log("a new formline was added");
			var formlineView = new FormLineView({model: model});
			var html = $(formlineView.render().el);
						
			var rows = $(this.el).children('div');
			
			var dummy_row = undefined;
			
			rows.each(function(){
				var row = $(this);
				if(row.attr('id') === undefined) {
					dummy_row = row;
				}
			});
			//console.log(rows);
			
			if(dummy_row === undefined) {
				$(this.el).append(html);
			}
			else {
				dummy_row.before(html);
			}
			
		},
		remove_formline: function(model) {
		},
		render: function() {
			return this;
		}
	});
//});