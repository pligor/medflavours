MultiLine_vthree = Backbone.Model.extend({
	//id: will be the model id
	defaults: {

	},
	initialize: function() {
		//this.set('forminputs', new FormInputsView());
		_.bindAll(this,'added','attach_view');
		this.on('add', this.added);
	},
	added: function(model) {
		$.ajax({
			url: model.collection.render_action_url,
			data: {
				id: model.id
			},
			method: 'GET',
			context: this,
			success: function(response) {
//documentation: Any event that is triggered on a model in a collection will also be triggered on the collection directly
				this.trigger('content_received',response,this);
			},
			error: function() {
				alert("ajax failed");
			}
		});
	},
	attach_view: function() {
		var id = this.id;	//this must not be undefined
		if( this.isNew() ) {
			id = '';
		}

		var selector = 'div#' + this.collection.model_name +'_'+ id;

		var multilineView = new MultiLine_vthreeView({
			model: this,
			el: selector	//el: 'div#<?php print get_class($model) .'_'. $model->id; ?>'
		});			
		multilineView.render_button();
		
		//console.log(multilineView);
	}
});

MultiLine_vthreeView = Backbone.View.extend({
	//model is passed as a parameter to the constructor ;)
	tagName: 'div',
	className: 'form_line',

	events: {
		'change': 'changed'
	},

	initialize: function() {
		_.bindAll(this,'render_button','button_clicked','removed','clearInputs','disable_inputs','getElementByAttr');

		this.model.on('remove',this.removed);
	},
	clearInputs: function() {
		/*var forminputs = $(this.el).find('input');
		forminputs.each(function(key,forminput){	//backbone's "each" function and jquery's are NOT the same
			$(forminput).val('');
		});*/

		var attrs = this.model.attributes;

		for(var key in attrs) {
			var $element = this.getElementByAttr(key);

			if($element.length == 1) {	//ean vrethike ena mono antikeimeno tote ola kalws!
				$element.val('');	//clear
			}
		}

	},
	getElementByAttr: function(key) {
		var $el = $(this.el);
		var $class = this.model.collection.model_name;

		//var curId = $class +'_'+ key;	//we had to sacrifice id method because of yii's autocomplete
		//return $el.find('#'+curId);
		var nameSelector = '[name=' + '"' + $class + '[' + key + ']' + '"' + "]";		//[name='HelloClass[secattr]']
		return $el.find(nameSelector);
	},
	changed: function() {
		//dynamically get all the values ;)
		var attrs = this.model.attributes;

		var newAttrs = {};

		for(var key in attrs) {
			var $element = this.getElementByAttr(key);

			if($element.length == 1) {	//ean vrethike ena mono antikeimeno tote ola kalws!
				newAttrs[key] = $element.val();
			}
		}
		//console.log(newAttrs);
		this.model.set(newAttrs,{
			silent: true
		});
	},
	removed: function() {
		this.remove();
	},
	button_clicked: function(ev) {
		ev.preventDefault();

		if(this.model.isNew()) {
			this.model.collection.create(this.model.attributes,{wait: true});
			this.clearInputs();
		}
		else {
			this.model.destroy({
				wait: true
			});
		}
	},
	render: function(content) {
		$(this.el).append(content);
		
		//WE ONLY KEEP THIS FOR CONSISTENCY AMONG DIVS
		//ACTUALLY WE DO NOT NEED SINCE WE ARE STATEFUL ;)
		if(!this.model.isNew()) {
			var div_id = this.model.collection.model_name +'_'+ this.model.id
			$(this.el).attr('id',div_id);
		}

		this.render_button();

		return this;
	},
	disable_inputs: function() {	//@todo this refers only to inputs (must be made stateful like e.g. clearInputs
		$(this.el).find('input').each( function(key,forminput) {
			$(forminput).attr('disabled',1);
		});
	},
	render_button: function() {
		var alt;
		var src;
		if(this.model.isNew()) {
			alt = 'add';
			src = this.model.collection.add_icon_url;
		}
		else {
			alt = 'remove';
			src = this.model.collection.remove_icon_url;
		}

		var html = $('<div class="row buttons"></div>');
		//html.css('display', 'inline-block');

		var button_template = this.model.collection.button_template;

		html.append( button_template.tmpl({
			alt: alt,
			src: src
		}) );

		//console.log(html);
		$(this.el).append(html);

		html.on('click',this.button_clicked);	//assign handler
	}
});

MultiLine_vthrees = Backbone.Collection.extend({
	model : MultiLine_vthree,

	url: null,

	model_name: null,

	render_action_url: null,

	add_icon_url: null,
	remove_icon_url: null,

	button_template: null,

	initialize: function() {
		this.button_template = $('script#button_tmpl');		//remember to include template script in final html
		//_.bindAll(this,'.....');
	}
});

MultiLine_vthreesView = Backbone.View.extend({
	//el: //is initialized from php

	initialize: function() {
		//collection is passed as a parameter

		_.bindAll(this,'add_multiline','remove_multiline','attach_views');

		this.collection.on('content_received', this.add_multiline);

		this.collection.on('remove', this.remove_multiline);
		
		this.collection.on('reset', this.attach_views);
	},
	attach_views: function(collection) {
		collection.each(function(model,key){
			model.attach_view();
		});
	},
	add_multiline: function(content,model) {
		var multilineView = new MultiLine_vthreeView({
			model: model
		});

		//REMEMBER: the order matters a lot! First attach div to webpage and THEN attach the javascript on the div
		//otherwise the javascript is not enabled
		var html = multilineView.el;

		//$(this.el).prepend(html);
		$(this.el).append(html);

		/*var rows = $(this.el).children('div');
		var dummy_row = undefined;
		rows.each(function(){
			var row = $(this);
			if(row.attr('id') === undefined) {
				dummy_row = row;
			}
		});
		if(dummy_row === undefined) {
			$(this.el).append(html);
		}
		else {
			dummy_row.before(html);
		}*/

		multilineView.render(content);

	},
	remove_multiline: function(model) {
	},
	render: function() {
		return this;
	}
});