ddaccordion.init({
    headerclass: "submenuheader", //Shared CSS class name of headers group
    contentclass: "submenu", //Shared CSS class name of contents group
    revealtype: "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
    mouseoverdelay: 100, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
    collapseprev: false, //Collapse previous content (so only one open at any time)? true/false 
    defaultexpanded: [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
    onemustopen: false, //Specify whether at least one header should be open always (so never all headers closed)
    animatedefault: false, //Should contents open by default be animated into view?
    persiststate: true, //persist state of opened contents within browser session?
    toggleclass: ["", ""], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
    /*
    togglehtml: ["suffix", "<img src='images/arrow_down_grey.png' class='statusicon' />", "<img src='images/arrow_down_grey.png' class='statusicon' />"], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
    */
    animatespeed: "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
    oninit:function(headers, expandedindices){ //custom code to run when headers have initialized
        //do nothing
    },
    onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
        //do nothing
    }
});

$(document).ready(function(){
    $('.help_box a').click(function(){
        if($('ul.help_menu').css('display') == 'none')
            $('em.arrowSprite').css('background-position','0px -48px');
        else
            $('em.arrowSprite').css('background-position','0px -32px');
        $('ul.help_menu').slideToggle('medium');
    });
    $(".tooltipFooter").tipsy({gravity: 's'});
    $(".tooltipHelpMenu").tipsy({gravity: 'w'});
});