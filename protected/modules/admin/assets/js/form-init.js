$(document).ready(function(){
    $('input').customInput();
    $(".tooltip").tipsy({gravity: 'w'});
    $("a#tips").click(function(){
        if($("#tipsText").css('display') == 'none')
        {
            $("#tipsText:hidden").slideDown();
            $("a#tips").html("Hide Tips");
            $("a#tips").attr("title", "Hide Tips");
        }else
        {
            $("#tipsText:visible").slideUp();
            $("a#tips").html("Show Tips");
            $("a#tips").attr("title", "Show Tips");
        }
    });
    $("a.infoSprite").click(function(){
        $("a#tips").trigger("click");
    });
    $(".myButton").click(function(){
        $(this).attr("disabled", "disabled");
        $("#backForm").submit();
    });
});