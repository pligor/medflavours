$(document).ready(function(){
    $('input').customInput();
    $("a#forgot").click(function(){
        if($("#forgetMyPass").css('display') == 'none')
            $("#forgetMyPass:hidden").slideDown();
        else
            $("#forgetMyPass:visible").slideUp();
    });
    $("form#login_form").validate();
    $("form#forgetMyPass").validate();
});