<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>E-Tourism Platform</title>
		<?php /*
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
		 */ ?>
        <?php
        _cs()->registerCoreScript('jquery');
        _cs()->registerCoreScript('jquery-ui');
        AdminUtils::registerAdminJsFile('jquery.validate.min');
        AdminUtils::registerAdminJsFile('custom-input.jquery');
        AdminUtils::registerAdminJsFile('ddaccordion');
        AdminUtils::registerAdminJsFile('general-init');
        AdminUtils::registerAdminJsFile('main-init');
        AdminUtils::registerAdminJsFile('jquery.pager');
        AdminUtils::registerAdminJsFile('jquery.tipsy');
        
        AdminUtils::registerAdminCssFile('reset');
        AdminUtils::registerAdminCssFile('main');
        AdminUtils::registerAdminCssFile('menu');
        AdminUtils::registerAdminCssFile('table');
        AdminUtils::registerAdminCssFile('form');
        AdminUtils::registerAdminCssFile('pager');
        AdminUtils::registerAdminCssFile('tipsy');
//        AdminUtils::registerAdminCssFile('jquery-ui-1.7.2.custom');
        ?>
        <style type="text/css">.tipsy{margin:-5px 0 0 0}</style>
    </head>
    <body class="twoColFixLtHdr">
        <div class="right_top_box">
            <div class="top_hold">
<!--                <div class="help_box"><a title="Need Help?" href="javascript:void(0)" class="helpLink"> <em class="icon16 helpSprite"></em> <span>Need Help?</span> <img src="backOffice/images/help_line.gif" alt="line" /> <em class="icon16 arrowSprite"></em> </a>
                    <ul class="help_menu spriteMenu">
                        <li><a href="#" title="Support Forum | Available Soon" class="tooltipHelpMenu"><em class="support_icon"></em>Support Forum</a></li>
                        <li><a href="#" title="Submit a ticket"><em class="ticket_icon"></em>Submit a ticket</a></li>
                        <li><a href="#" title="Suggest a Feature"><em class="suggest_icon"></em>Suggest a Feature</a></li>
                        <li><a href="#" title="Report a bug"><em class="report_icon"></em>Report a bug</a></li>
                        <li><a href="#" title="Support videos | Available Soon" class="tooltipHelpMenu"><em class="support_videos_icon"></em>Support videos</a></li>
                    </ul>
                </div>-->
                <div class="support_box"><em class="icon16 supportSprite"></em><span>For support call us on  +30 211 10 89 970</span></div>
            </div>
            <div class="logged_box">
                <p><em class="icon16 userSprite"></em>You Logged in as: <?php echo _user()->username; ?>
					<a href="<?php
					//print $this->createUrl(AdminUtils::aurl('logout');
					print $this->createUrl('main/logout');
					?>" title="Logout"><em class="icon16 loggedSprite"></em>Log out</a></p>
            </div>
            <div class="ClearFloat"></div>
        </div>
		<?php ///* see inside modules/admin/views/layouts/admin.php to remove comments and reenable header and footer ?>
        <div id="header"><?php echo CHtml::link(CHtml::image(AdminUtils::adminImageUrl('e-tourism_platform_logo_all.png'), 'E-tourism Platform', array('width' => 292, 'height' => 43)), _url('#')); ?>
            <div class="volume">ver 3.0</div>
<!--            <div class="white_box"><?php echo CHtml::image(AdminUtils::adminImageUrl('hotels_big_icon.png'), 'Hotels' , array('width' => 32, 'height' => 30)); ?>
                <h1>Hotels</h1>
            </div>-->
<!--            <div class="top_arrow"></div>-->
            <br class="clearfloat" />
        </div>
		<?php //*/ ?>
        <div id="container">
            <div id="sidebar1">
                <?php $this->widget('Menu'); ?>
            </div>
            <div id="mainContentFull">
                <?php echo $content; ?>
            </div>
            <br class="clearfloat" />
        </div>
		<?php ///* ?>
        <div id="footer">
            <div class="footer_hold">
                <div class="footer_menu spriteMenu"> <a href="#" title="Support Forum | Available Soon" class="tooltipFooter"><em class="support_icon"></em>Support Forum</a> <a href="#" title="Submit a ticket"><em class="ticket_icon"></em>Submit a ticket</a> <a href="#" title="Suggest a Feature"><em class="suggest_icon"></em>Suggest a Feature</a> <a href="#" title="Report a bug"><em class="report_icon"></em>Report a bug</a> <a href="#" title="Support videos | Available Soon" class="tooltipFooter"><em class="support_videos_icon"></em>Support videos</a> </div>
                <div class="copyright"> Copyright 2012 Nelios.com | advanced Web Development <em class="neliosLogo neliosWhite"></em> </div>
            </div>
        </div>
		<?php //*/ ?>
    </body>
</html>