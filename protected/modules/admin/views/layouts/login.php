<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php
            AdminUtils::registerAdminCssFile('reset');
            AdminUtils::registerAdminCssFile('login');
            AdminUtils::registerAdminCssFile('custom-inputs');
            _cs()->registerCoreScript('jquery');
            AdminUtils::registerAdminJsFile('jquery.validate.min');
            AdminUtils::registerAdminJsFile('custom-input.jquery');
            AdminUtils::registerAdminJsFile('login-init');
        ?>
    </head>
    <body>
        <div class="header" align="center"><?php echo CHtml::image(AdminUtils::adminImageUrl('e-tourism_platform_logo.png'), 'Nelios E-Tourism Platform', array('width' => 230, 'height' => 187)); ?>
            <div class="volume">ver 3.0</div>
        </div>
        <div class="main">
            <?php echo $content; ?>
        </div>
        <div class="footer">
            <p>Copyright 2010 Nelios.com | advanced Web Development</p>
            <em class="neliosLogo neliosBlack"></em>
            <div class="ClearFloat"></div>
        </div>
    </body>
</html>