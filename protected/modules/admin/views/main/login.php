<?php
//print md5('admin');
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login_form',
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
        ));
?>
<label for="username"><em class="icon16 userSprite"></em>Username</label>
<?php echo $form->textField($model, 'username', array('title' => 'Enter your username', 'maxlength' => 20)); ?>
<?php echo $form->error($model, 'username'); ?>

<label for="password"><em class="icon16 passwordSprite"></em>Password</label>
<?php echo $form->passwordField($model, 'password', array('title' => 'Enter your password', 'maxlength' => 20)); ?>
<?php echo $form->error($model, 'password'); ?>

<div class="remember_me">
    <?php echo $form->checkBox($model, 'rememberMe'); ?>
    <?php echo $form->label($model, 'rememberMe'); ?>
    <?php echo $form->error($model, 'rememberMe'); ?>
    <div class="ClearFloat"></div>
</div>
<input type="submit" id="submit" class="submit" value="" />
    <div class="ClearFloat"></div>
    <?php $this->endWidget(); ?>
<a id="forgot" href="javascript:void(0)" title="Lost your password?">Lost your password?</a>
<form method="post" id="forgetMyPass" action="#">
    <input class="required email" type="text" title="Enter your email address" name="forgotten_email" maxlength="35" />
    <div id="mailNotice"></div>
    <input type="submit" value="Send me the password" class="forgot_submit"/>
    <div class="ClearFloat"></div>
</form>
<div class="ClearFloat"></div>