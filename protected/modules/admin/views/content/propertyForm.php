<?php

return array(
    'attributes' => array(
        'id' => 'backForm',
    ),
    'elements' => array(
        'property' => array(
            'type' => 'form',
            'elements' => array(
                'name' => array(
                    'type' => 'text',
                ),
            ),
        ),
    ),
    'buttons' => _formButtons(),
);