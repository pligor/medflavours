<?php

return array(
    'attributes' => array(
        'id' => 'backForm',
        'enctype' => 'multipart/form-data'
    ),
    'elements' => array(
        'category' => array(
            'type' => 'form',
            'elements' => array(
                'parent_id' => array(
                    'type' => 'dropdownlist',
                    'items' => Category::model()->getTree(),
                    'prompt' => '----',
                    'label' => 'Parent',
                ),
                'name' => array(
                    'type' => 'text',
                ),
                'description' => array(
					'type' => 'external.cleditor.ECLEditor',
                    'attributes' => array(
                        'options' => array(
                            'width' => '500',
                            'height' => 250,
                            'useCSS' => true,
                            'controls' => _controls(),
                        ),
                    ),
                ),
            ),
        ),
        '<hr />',
        '<h2>SEO</h2>',
        'seo' => array(
            'type' => 'form',
            'elements' => array(
                'url' => array(
                    'type' => 'text',
                ),
                'title' => array(
                    'type' => 'text',
                ),
                'description' => array(
                    'type' => 'text',
                ),
                'keywords' => array(
                    'type' => 'text',
                ),
            ),
        ),
    ),
    'buttons' => _formButtons(),
);