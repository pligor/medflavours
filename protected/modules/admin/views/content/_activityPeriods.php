<h3>Active periods</h3>
<div id="periods">
    <?php $c = 0; ?>
    <?php if (isset($existing)) : ?>
        <?php foreach($existing as $period) : ?>

            <?php $this->renderPartial('_activityPeriodLine', array('period' => $period, 'c' => $c)); ?>
            <?php ++$c; ?>

        <?php endforeach; ?>
    <?php endif; ?>
    
    <?php $this->renderPartial('_activityPeriodLine', array('id' => $c)); ?>

    <?php echo CHtml::hiddenField('count', $c+1, array('id' => 'id')); ?>
</div>
<script>
    function addPeriod()
    {
        var id = parseInt($("#id").val());

        $.ajax({
            url: "<?php echo _aUrl('content/activityPeriodLine'); ?>",
            data: {
                id: id
            },
            method: 'GET',
            success: function(response)
            {
                $("#periods").append(response);
            }
        });

        $("#periodFrom_"+(id-1)).parent().siblings("#link").html('<a href="#" onclick="js:removePeriod(\'periodRow_'+(id-1)+'\'); return false;" class="add-period"><img alt="" src=\'<?php echo AdminUtils::adminImageUrl('remove.png'); ?>\' class="add-period-btn"></a>');

        $("#id").val(id+1);
    }
    function removePeriod(obj)
    {
        $("#"+obj).remove()
    }
    function removePeriodFromDB(obj, id)
    {
        $.ajax({
            url: "<?php echo _aUrl('content/removeActivityPeriod'); ?>",
            data: {
                id: id
            },
            method: 'GET',
            success: function(response) {
                $("#"+obj).remove();
            }
        })
    }
</script>