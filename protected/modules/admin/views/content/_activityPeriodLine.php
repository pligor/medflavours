<?php if (isset($id)) : ?>
    <div id="periodRow_<?php echo $id; ?>">
        <div class="row field_from">
            <?php echo CHtml::label('From', 'from'); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => "periodFrom[$id]",
                // additional javascript options for the date picker plugin
                'options' => array(
                    'showAnim' => 'fold',
                    'altFormat' => 'dd-mm-yy',
                    'dateFormat' => 'yy/mm/dd',
                    'beforeShow' => 'js:function(input, inst){inst.dpDiv.css({marginTop: -input.offsetHeight + "px", marginLeft: input.offsetWidth + "px"});}'
                ),
                'htmlOptions' => array(
                    'class' => 'short-text'
                ),
            ));
            ?>
        </div>
        <div class="row field_to">
            <?php echo CHtml::label('To', 'to'); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => "periodTo[$id]",
                // additional javascript options for the date picker plugin
                'options' => array(
                    'showAnim' => 'fold',
                    'altFormat' => 'dd-mm-yy',
                    'dateFormat' => 'yy/mm/dd',
                    'beforeShow' => 'js:function(input, inst){inst.dpDiv.css({marginTop: -input.offsetHeight + "px", marginLeft: input.offsetWidth + "px"});}'
                ),
                'htmlOptions' => array(
                    'class' => 'short-text'
                ),
            ));
            ?>
        </div>
        <span id="link"><?php echo CHtml::link(CHtml::image(AdminUtils::adminImageUrl('add.png'), '', array('class' => 'add-period-btn')), '#', array('class' => 'add-period', 'onclick' => 'js:addPeriod(); return false;')); ?></span>
        <div class="ClearFloat"></div>
    </div>
    <script>
        jQuery(function($) {
            jQuery('#periodFrom_<?php echo $id; ?>').datepicker({'showAnim':'fold'});
            jQuery('#periodTo_<?php echo $id; ?>').datepicker({'showAnim':'fold'});
        }); 
    </script>
<?php else : ?>
    <div id="periodRow_<?php echo $c; ?>">
        <div class="row field_from">
            <?php echo CHtml::label('From', 'from'); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => "periodFrom[$c]",
                'value' => $period->from,
                // additional javascript options for the date picker plugin
                'options' => array(
                    'showAnim' => 'fold',
                    'altFormat' => 'dd-mm-yy',
                    'dateFormat' => 'yy/mm/dd',
                ),
                'htmlOptions' => array(
                    'class' => 'short-text',
                    'disabled' => true
                ),
            ));
            ?>
        </div>
        <div class="row field_to">
            <?php echo CHtml::label('To', 'to'); ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'name' => "periodTo[$c]",
                'value' => $period->to,
                // additional javascript options for the date picker plugin
                'options' => array(
                    'showAnim' => 'fold',
                    'altFormat' => 'dd-mm-yy',
                    'dateFormat' => 'yy/mm/dd',
                ),
                'htmlOptions' => array(
                    'class' => 'short-text',
                    'disabled' => true
                ),
            ));
            ?>
        </div>
        <span id="link"><?php echo CHtml::link(CHtml::image(AdminUtils::adminImageUrl('remove.png'), '', array('class' => 'add-period-btn')), '#', array('class' => 'add-period', 'onclick' => 'js:removePeriodFromDB("periodRow_' . $c . '","' . $period->id . '"); return false;')); ?></span>
        <div class="ClearFloat"></div>
    </div>
    <script>
        jQuery(function($) {
            jQuery('#periodFrom_<?php echo $c; ?>').datepicker({'showAnim':'fold'});
            jQuery('#periodTo_<?php echo $c; ?>').datepicker({'showAnim':'fold'});
        }); 
    </script>
<?php endif; ?>
