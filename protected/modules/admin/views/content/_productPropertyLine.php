<?php if (isset($id)) : ?>
    <div id="propertyRow_<?php echo $id; ?>">
        <div class="row field_name">
            <?php echo CHtml::label('Name', 'name'); ?>
            <?php echo CHtml::dropDownList("propertyName[$id]", null, CHtml::listData(Property::model()->findAll(), 'id', 'name'), array('class' => 'short-text', 'prompt' => '---')); ?>
        </div>
        <div class="row field_value">
            <?php echo CHtml::label('Value', 'value'); ?>
            <?php echo CHtml::textField("propertyValue[$id]", null, array('class' => 'short-text')); ?>
        </div>
        <span id="link"><?php echo CHtml::link(CHtml::image(AdminUtils::adminImageUrl('add.png'), '', array('class' => 'add-property-btn')), '#', array('class' => 'add-period', 'onclick' => 'js:addProperty(); return false;')); ?></span>
        <div class="ClearFloat"></div>
    </div>
<?php else : ?>
    <div id="propertyRow_<?php echo $c; ?>">
        <div class="row field_name">
            <?php echo CHtml::label('Name', 'name'); ?>
            <?php echo CHtml::dropDownList("propertyName[$c]", $property->property->id, CHtml::listData(Property::model()->findAll(), 'id', 'name'), array('class' => 'short-text')); ?>
        </div>
        <div class="row field_value">
            <?php echo CHtml::label('Value', 'value'); ?>
            <?php echo CHtml::textField("propertyValue[$c]", $property->value, array('class' => 'short-text')); ?>
        </div>
        <span id="link"><?php echo CHtml::link(CHtml::image(AdminUtils::adminImageUrl('remove.png'), '', array('class' => 'add-property-btn')), '#', array('class' => 'add-period', 'onclick' => 'js:removePropertyFromDB("propertyRow_'.$c.'","'.$property->id.'"); return false;')); ?></span>
        <div class="ClearFloat"></div>
    </div>
<?php endif; ?>
