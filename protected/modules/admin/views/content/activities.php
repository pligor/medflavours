<div class="tableTop topToolbar">
    <a class="addButton" id="addButton" title="Add <?php echo $type; ?>" href="<?php echo _aUrl('content/add'.$type); ?>"><em class="icon16 addItem"></em>Add <?php echo $type; ?></a>
    <div class="ClearFloat"></div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $activities->search(),
    'filter' => $activities,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'name',
        ),
        array(
            'name' => 'location_id',
            'header' => 'Location',
            'value' => '$data->location->name',
            'filter' => Location::model()->getTree(),
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '100px'),
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => '',
            'updateButtonLabel' => '',
            'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
            'updateButtonUrl' => '_aUrl("content/edit'.$type.'", array("id" => $data->id))',
            'deleteButtonImageUrl' => '',
            'deleteButtonLabel' => '',
            'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
            'deleteButtonUrl' => '_aUrl("content/delete'.$type.'", array("id" => $data->id))',
        ),
    ),
));
?>