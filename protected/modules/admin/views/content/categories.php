<div class="tableTop topToolbar">
    <?php if ($parent_id) : ?>
        <a class="deleteButton" id="deleteButton" title="Go back" href="<?php echo _aUrl('content/categories', array('parent_id' => $previous_id)); ?>"><em class="icon16 deleteAll"></em>Go back</a>
    <?php endif; ?>
    <a class="addButton" id="addButton" title="Add Category" href="<?php echo _aUrl('content/addCategory'); ?>"><em class="icon16 addItem"></em>Add Category</a>
    <div class="ClearFloat"></div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $categories->search(),
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'name',
            'value' => 'count($data->subcategories) ? CHtml::link($data->name, _aUrl("content/categories", array("parent_id" => $data->id))) : $data->name',
            'type' => 'raw',
        ),
        array(
            'name' => 'description',
            'type' => 'raw',
        ),
        array(
            'header' => 'Sub',
            'value' => 'count($data->subcategories)',
            'htmlOptions' => array('class' => 'grid-count-column')
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '100px'),
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => '',
            'updateButtonLabel' => '',
            'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
            'updateButtonUrl' => '_aUrl("content/editCategory", array("id" => $data->id))',
            'deleteButtonImageUrl' => '',
            'deleteButtonLabel' => '',
            'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
            'deleteButtonUrl' => '_aUrl("content/deleteCategory", array("id" => $data->id))',
            'deleteConfirmation' => "This will delete all subcategories and all related products. Are you sure you wish to delete this category?",
        ),
    ),
));
?>