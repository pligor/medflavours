<?php
//d($form->model->errors);
?>
<div class="tableMain">
    <?php
    echo $form->renderBegin();
    echo $form->renderButtons();
    echo $form->renderElements();
    ?>
    
	<?php /*
    <?php if( isset($gMap) && $gMap) : ?>
        <label>Click on the map to set the location</label>
        <?php $gMap->renderMap(array(), Yii::app()->language); ?>
    <?php endif; ?>
	//*/ ?>
        
    <?php if( isset($photo) && $photo ) : ?>
        <hr /><h3>Photos</h3>
        <?php
        $this->widget('CMultiFileUpload', array(
            'name' => 'images',
            'accept' => 'jpg|gif|jpeg|png',
        ));
        ?>
        <div class="photos">
            <?php foreach ($photo['photos'] as $p) : ?>
                <?php
				$photo_class = get_class($p);
				$photo_id = $p->id;
				$photo_link_id = $photo_class . '_' .$photo_id;
                print CHtml::ajaxLink(
					CHtml::image(Utils::imageUrl('photos/' . $p->photo)),
					_aUrl('content/removePhoto', array('id' => $photo_id, 'class' => $photo_class)),
					array(	//ajax options
						//'update' => '.photos',
						'success' => "js:function(response) {
							$('#$photo_link_id').remove();
						}"
					),
					array(	//html options (attributes)
						'title' => 'Remove this image',
						'id' => $photo_link_id,
					)
                );
                ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
		
    <?php /* if(isset($multiline) && $multiline) : ?>
        <hr />
        <?php
			$this->renderPartial($multiline['view'], array(
				'model' => $multiline['model'],
				'existing' => $multiline['existing'],
			));
		?>
    <?php endif; //*/ ?>
		
    <?php
    print $form->renderButtons();
    print $form->renderEnd();
    ?>
</div>

<?php
if(isset($activityPeriods) && $activityPeriods) {
	print "<hr/>";
	$this->widget('ext.widgets.MultilineWidget_vthree', array(	//remember to configure your model according to the multiline specs
		'title' => 'Activity Periods',
		'model_name' => $activityPeriods['model_name'],
		'models' => $activityPeriods['models'],
		'params' => $activityPeriods['params'],	//here is provided the activity_id
	));
}
?>

<?php
if(isset($addons) && $addons) {
	print "<hr/>";
	$this->widget('ext.widgets.MultilineWidget_vthree',array(	//remember to configure your model according to the multiline specs
		'title' => 'Add Ons',
		'model_name' => $addons['model_name'],
		'models' => $addons['models'],
		'params' => $addons['params'],	//here is provided the activity_id
	));
}
?>

<?php
if(isset($activityProperties) && $activityProperties) {
	print "<hr/>";
	$this->widget('ext.widgets.MultilineWidget_vthree',array(	//remember to configure your model according to the multiline specs
		'title' => 'Activity Properties',
		'model_name' => $activityProperties['model_name'],
		'models' => $activityProperties['models'],
		'params' => $activityProperties['params'],	//here is provided the activity_id
	));
}
?>

<?php
if(isset($productProperties) && $productProperties) {
	print "<hr/>";
	$this->widget('ext.widgets.MultilineWidget_vthree',array( //remember to configure your model according to the multiline specs
		'title' => 'Product Properties',
		'model_name' => $productProperties['model_name'],
		'models' => $productProperties['models'],
		'params' => $productProperties['params'],	//here is provided the product_id
	));
}
?>

<?php if(isset($packs)): ?>
	<div class="tableTop topToolbar">
		<a class="addButton" id="addButton" title="Add Pack" href="<?php
		print $this->createUrl('content/create_pack',array('product_id'=>$form['product']->model->id));
		?>">
			<em class="icon16 addItem"></em>Add Pack
		</a>
		<div class="ClearFloat"></div>
	</div>
<?php endif; ?>

<?php if(isset($packs) && $packs): ?>
	<?php
	$pack = new Pack('search');
	$pack->product_id = $form['product']->model->id;

	///*
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider' => $pack->search(),
		//'filter' => $pack,
		//'pager' => array('class' => 'CLinkPager', 'header' => ''),
		'template' => '{items}',	//actually we have disabled summary and pager: http://www.yiiframework.com/doc/api/1.1/CBaseListView#template-detail
		'cssFile' => '',	//http://www.yiiframework.com/doc/api/1.1/CGridView#cssFile-detail
		'columns' => array(
			array(
				'name' => 'name',
				'header' => 'Title',
			),
			array(
				'name' => 'stock',
				//'header' => 'Category',
				//'value' => '$data->category->name',
				//'filter' => Category::model()->getTree(),
				//'filter' => CHtml::listData(Supplier::model()->findAll(), 'id', 'name'),
			),
			array(
				'name' => 'photo',
				//'value' => 'CHtml::image($data->photoFullpath(), $data->photo, array())',
				'value' => '$data->photoUrl()',
				'type' => 'image',
				'cssClassExpression' => '"image_column"',
			),
			array(
				'class' => 'CButtonColumn',
				'header' => 'Tools',
				'headerHtmlOptions' => array('width' => '100px'),
				'template' => '{update}{delete}',
				
				'updateButtonImageUrl' => '',
				'updateButtonLabel' => '',
				'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
				'updateButtonUrl' => '$this->grid->controller->createUrl("content/edit_pack", array("id" => $data->id))',
				//http://www.yiiframework.com/doc/api/1.1/CButtonColumn#updateButtonUrl-detail
				
				'deleteButtonImageUrl' => '',
				'deleteButtonLabel' => '',
				'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
				'deleteButtonUrl' => '$this->grid->controller->createUrl("content/delete_pack", array("id" => $data->id))',
				//http://www.yiiframework.com/doc/api/1.1/CButtonColumn#deleteButtonUrl-detail
				'deleteConfirmation' => "Are you sure you wish to delete this pack?",
			),
		),
	));
	//*/
	?>
<?php endif; ?>

<?php if(isset($addonGroups)): ?>
	<div class="tableTop topToolbar">
		<a class="addButton" id="addButton" title="Add Addon Group" href="<?php
		print $this->createUrl('content/create_AddonGroup', array('activity_id' => $form['activity']->model->id));
		?>">
			<em class="icon16 addItem"></em>Add Addon Group
		</a>
		<div class="ClearFloat"></div>
	</div>
<?php endif; ?>

<?php if(isset($addonGroups) && $addonGroups): ?>
	<?php
	$addonGroup = new AddonGroup('search');
	$addonGroup->activity_id = $form['activity']->model->id;

	///*
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider' => $addonGroup->search(),
		//'filter' => $addonGroup,
		//'pager' => array('class' => 'CLinkPager', 'header' => ''),
		'template' => '{items}',	//actually we have disabled summary and pager: http://www.yiiframework.com/doc/api/1.1/CBaseListView#template-detail
		'cssFile' => '',	//http://www.yiiframework.com/doc/api/1.1/CGridView#cssFile-detail
		'columns' => array(
			array(
				'name' => 'name',
				'header' => 'Title',
			),
			array(
				'name' => 'selection_style',
				//'header' => 'Selection Style',
				'value' => 'SelectionStyle::getText($data->selection_style)',
				//'filter' => Category::model()->getTree(),
				//'filter' => CHtml::listData(Supplier::model()->findAll(), 'id', 'name'),
			),
			array(
				'class' => 'CButtonColumn',
				'header' => 'Tools',
				'headerHtmlOptions' => array('width' => '100px'),
				'template' => '{update}{delete}',
				
				'updateButtonImageUrl' => '',
				'updateButtonLabel' => '',
				'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
				'updateButtonUrl' => '$this->grid->controller->createUrl("content/edit_AddonGroup", array("id" => $data->id))',
				//http://www.yiiframework.com/doc/api/1.1/CButtonColumn#updateButtonUrl-detail
				
				'deleteButtonImageUrl' => '',
				'deleteButtonLabel' => '',
				'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
				'deleteButtonUrl' => '$this->grid->controller->createUrl("content/delete_AddonGroup", array("id" => $data->id))',
				//http://www.yiiframework.com/doc/api/1.1/CButtonColumn#deleteButtonUrl-detail
				'deleteConfirmation' => "Are you sure you wish to delete this addon group?",
			),
		),
	));
	//*/
	?>
<?php endif; ?>

<script id="input_tmpl" type="text/x-jquery-tmpl">
	<?php
	//GIA TO SPAN CLASS REQUIRED THA PREPEI NA DOUME AN GINETAI IF H AN EXOUME TEMPLATE MESA SE TEMPLATE
	//<label class="required" for="Activity_price">Price<span class="required">*</span></label>
	//<input id="Activity_price" type="text" value="200.00" name="Activity[price]">
	?>
	<?php //<div class="row"> ?>
		<label for="${model_name}_${attr_name}">${label}</label>
		<input id="${model_name}_${attr_name}" type="${type}" name="${model_name}[${attr_name}]" value="${value}">
	<?php //</div> ?>	
</script>

<div id="testdiv">
	<?php
	//test here
	//print $this->module->getJsLibraryUrl();
	?>
</div>