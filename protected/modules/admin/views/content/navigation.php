<?php
//d($target_ids);
//print $this->route;
?>

<?php
//BE AWARE: Might need to restart apache server if you move the extension!
//BE AWARE: if you don't give nodes with clear path you are doomed (you must have ancestors)

foreach($this->modelNames as $pos => $class) {
	$table_name = $class::model()->getTableName();
	
	$mtreeviewConfig[$pos] = array( //http://macinville.com/129/managing-nested-setadjacency-models-yii/
		'collapsed' => false,
		'animated' => 'fast',

		//---MTreeView options from here
		'table' => $table_name, //what table the menu would come from
		'hierModel' => 'adjacency', //hierarchy model of the table
		'fields' => array ( //declaration of fields
			'text' => 'name', //no `text` column, use `name` instead
			'alt' => false, //skip using `alt` column
			'id_parent' => 'parent_id', //no `id_parent` column, use `parent_id` instead
			'task' => false,
			'icon' => false,
			'url' => array('/admin/content/navigation&model_clicked='.$class,array('model_id'=>'id',)),
			'tooltip' => false,
			'position' => 'name',	//column
		),
	);
	
	if($model_targeted == $class) {
		if(!empty($target_ids)) {
			$target_ids = implode(',', $target_ids);
		}
		else {
			$target_ids = 0;
		}
		$mtreeviewConfig[$pos]['conditions'] = array(
			//'visible=:visible',array(':visible'=>1,),
			//'name=:name',array(':name' => 'Bioproducts',),
			'id IN ('.$target_ids.')',array(':dummy'=>'bound',),
		);//other conditions if any*/
	}
}
?>

<?php
$this->widget('external.MTreeView.MTreeView',$mtreeviewConfig['left']);
?>

<br/>
<br/>
<b><i>content in between based on categories OR location</i></b>
<br/>
<br/>

<?php
$this->widget('external.MTreeView.MTreeView',$mtreeviewConfig['right']);
?>