<?php

return array(
    'attributes' => array(
        'id' => 'backForm',
    ),
    'elements' => array(
        'static' => array(
            'type' => 'form',
            'elements' => array(
                'content' => array(
					'type' => 'external.cleditor.ECLEditor',
                    'attributes' => array(
                        'options' => array(
                            'width' => '500',
                            'height' => 250,
                            'useCSS' => true,
                            'controls' => _controls(),
                        ),
                    ),
                ),
                'show_in_menu' => array(
                    'type' => 'checkbox',
                ),
                'menu_title' => array(
                    'type' => 'text',
                ),
            ),
        ),
        '<hr />',
        '<h2>SEO</h2>',
        'seo' => array(
            'type' => 'form',
            'elements' => array(
                'url' => array(
                    'type' => 'text',
                ),
                'title' => array(
                    'type' => 'text',
                ),
                'description' => array(
                    'type' => 'text',
                ),
                'keywords' => array(
                    'type' => 'text',
                ),
            ),
        ),
    ),
    'buttons' => _formButtons(),
);