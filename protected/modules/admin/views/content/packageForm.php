<?php

return array(
    'attributes' => array(
        'id' => 'backForm',
        'enctype' => 'multipart/form-data'
    ),
    'elements' => array(
        'package' => array(
            'type' => 'form',
            'elements' => array(
                'name' => array(
                    'type' => 'text',
                ),
                'price' => array(
                    'type' => 'text',
                ),
                'small_description' => array(
					'type' => 'external.cleditor.ECLEditor',
                    'attributes' => array(
                        'options' => array(
                            'width' => '500',
                            'height' => 250,
                            'useCSS' => true,
                            'controls' => _controls(),
                        ),
                    ),
                ),
                'full_description' => array(
					'type' => 'external.cleditor.ECLEditor',
                    'attributes' => array(
                        'options' => array(
                            'width' => '500',
                            'height' => 250,
                            'useCSS' => true,
                            'controls' => _controls(),
                        ),
                    ),
                ),
            ),
        ),
        '<hr />',
        '<h2>SEO</h2>',
        'seo' => array(
            'type' => 'form',
            'elements' => array(
                'url' => array(
                    'type' => 'text',
                ),
                'title' => array(
                    'type' => 'text',
                ),
                'description' => array(
                    'type' => 'text',
                ),
                'keywords' => array(
                    'type' => 'text',
                ),
            ),
        ),
    ),
    'buttons' => _formButtons(),
);