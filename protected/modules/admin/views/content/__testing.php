<?php

d( $model->getExisting()->attributes );

print $form->render();
?>
<?php /* ?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="row">
		<?php $this->widget('external.erikuus.widgets.google.XGoogleInputMap', array(
			'googleApiKey' => Yii::app()->params['googleApiKey'],
			'form' => $form,
			//'model' => new Map,
			'model' => $model,
		)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>
<?php //*/ ?>