<div class="tableTop topToolbar">
    <a class="addButton" id="addButton" title="Add Supplier" href="<?php echo _aUrl('content/addSupplier'); ?>"><em class="icon16 addItem"></em>Add Supplier</a>
    <div class="ClearFloat"></div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $suppliers->search(),
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'name',
        ),
        array(
            'name' => 'address',
        ),
        array(
            'name' => 'email',
        ),
        array(
            'name' => 'phone',
        ),
        array(
            'header' => 'Pro',
            'value' => 'count($data->products)',
            'htmlOptions' => array('class' => 'grid-count-column')
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '100px'),
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => '',
            'updateButtonLabel' => '',
            'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
            'updateButtonUrl' => '_aUrl("content/editSupplier", array("id" => $data->id))',
            'deleteButtonImageUrl' => '',
            'deleteButtonLabel' => '',
            'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
            'deleteButtonUrl' => '_aUrl("content/deleteSupplier", array("id" => $data->id))',
            'deleteConfirmation' => "This will delete all supplier's products. Are you sure you wish to delete this supplier?",
        ),
    ),
));
?>