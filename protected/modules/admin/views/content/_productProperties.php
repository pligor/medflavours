<h3>Custom properties</h3>
<div id="properties">
    <?php
		$c = 0;			//PROFANWS TO c einai kati gia to id
	?>
    <?php if (isset($existing)) : ?>
        <?php foreach($existing as $property) : ?>

            <?php $this->renderPartial('_productPropertyLine', array('property' => $property, 'c' => $c)); ?>
            <?php ++$c; ?>

        <?php endforeach; ?>
    <?php endif; ?>
    
    <?php $this->renderPartial('_productPropertyLine', array('id' => $c)); ?>

    <?php echo CHtml::hiddenField('count', $c+1, array('id' => 'id')); ?>
</div>
<script>
    function addProperty()
    {
        var id = parseInt($("#id").val());

        $.ajax({
            url: "<?php echo _aUrl('content/productPropertyLine'); ?>",
            data: {
                id: id
            },
            method: 'GET',
            success: function(response)
            {
                $("#properties").append(response);
            }
        });

        $("#propertyName_"+(id-1)).parent().siblings("#link").html('<a href="#" onclick="js:removeProperty(\'propertyRow_'+(id-1)+'\'); return false;" class="add-period"><img alt="" src=\'<?php echo AdminUtils::adminImageUrl('remove.png'); ?>\' class="add-property-btn"></a>');

        $("#id").val(id+1);
    }
    function removeProperty(obj)
    {
        $("#"+obj).remove()
    }
    function removePropertyFromDB(obj, id)
    {
        $.ajax({
            url: "<?php echo _aUrl('content/removeProductProperty'); ?>",
            data: {
                id: id
            },
            method: 'GET',
            success: function(response) {
                $("#"+obj).remove();
            }
        })
    }
</script>