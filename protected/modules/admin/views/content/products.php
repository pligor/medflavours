<div class="tableTop topToolbar">
    <a class="addButton" id="addButton" title="Add Category" href="<?php echo _aUrl('content/addProduct'); ?>"><em class="icon16 addItem"></em>Add Product</a>
    <div class="ClearFloat"></div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $products->search(),
    'filter' => $products,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'name',
        ),
        array(
            'name' => 'category_id',
            'header' => 'Category',
            'value' => '$data->category->name',
            'filter' => Category::model()->getTree(),
        ),
        array(
            'name' => 'supplier_id',
            'header' => 'Supplier',
            'value' => '$data->supplier->name',
            'filter' => CHtml::listData(Supplier::model()->findAll(), 'id', 'name'),
        ),
        array(
            'name' => 'location_id',
            'header' => 'Location',
            'value' => '$data->location->name',
            'filter' => Location::model()->getTree(),
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '100px'),
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => '',
            'updateButtonLabel' => '',
            'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
            'updateButtonUrl' => '_aUrl("content/editProduct", array("id" => $data->id))',
            'deleteButtonImageUrl' => '',
            'deleteButtonLabel' => '',
            'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
            'deleteButtonUrl' => '_aUrl("content/deleteProduct", array("id" => $data->id))',
			'deleteConfirmation' => "This will delete the product along with all its packs. Are you sure you wish to delete this product?",
        ),
    ),
));
?>