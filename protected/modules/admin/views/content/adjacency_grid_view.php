<div class="tableTop topToolbar">
    <?php if ($model->parent_id) : ?>
        <a class="deleteButton" id="deleteButton" title="Go back" href="<?php print $this->createUrl('content/'.$this->action->id, array('parent_id' => $grandparent_id)); ?>"><em class="icon16 deleteAll"></em>Go back</a>
    <?php endif; ?>
    <a class="addButton" id="addButton" title="Add <?php print $this->action->model_name; ?>" href="<?php print $this->createUrl('content/add_cat',array('class'=>$this->action->model_name)); ?>"><em class="icon16 addItem"></em>Add <?php print $this->action->model_name; ?></a>
    <div class="ClearFloat"></div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    //'pager' => array('class' => 'CLinkPager', 'header' => ''),	//WE HAVE DISABLED THE PAGER
    //'template' => '{items}{pager}',
	'template' => '{items}',
    'cssFile' => '',
    'columns' => array(
        array(
			//'name' => 'name',
			//'value' => 'count($data->children) ? CHtml::link($data->name, $this->grid->controller->createUrl("content/'. $this->action->id .'", array("parent_id" => $data->id))) : $data->name',
			//'type' => 'raw',
			'class' => 'CLinkColumn',
			'header' => 'Name',
			'labelExpression' => '$data->name',
			'urlExpression' => '$this->grid->controller->createUrl("content/'. $this->action->id .'", array("parent_id" => $data->id))',
        ),
        array(
            'name' => 'description',
            'type' => 'raw',
        ),
        array(
            'header' => 'Sub',
            'value' => 'count($data->children)',
            'htmlOptions' => array('class' => 'grid-count-column'),
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '100px'),
            'template' => '{update}{delete}',
            
			'updateButtonImageUrl' => '',
            'updateButtonLabel' => '',
            'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
            'updateButtonUrl' => '$this->grid->controller->createUrl("content/edit_cat", array("id" => $data->id,"class"=>get_class($data)))',
			
            'deleteButtonImageUrl' => '',
            'deleteButtonLabel' => '',
            'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
            'deleteButtonUrl' => '$this->grid->controller->createUrl("content/delete_cat", array("id" => $data->id,"class"=>get_class($data)))',
            
			'deleteConfirmation' => "This will delete all children categories and all related entities. Are you sure you wish to delete this category?",
        ),
    ),
));
?>