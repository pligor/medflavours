<div class="tableTop topToolbar">
    <a class="addButton" id="addButton" title="Add Static Content" href="<?php echo _aUrl('content/addStaticContent'); ?>"><em class="icon16 addItem"></em>Add Static Content</a>
    <div class="ClearFloat"></div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $static->search(),
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'menu_title',
        ),
        array(
            'name' => 'content',
            'type' => 'raw',
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '100px'),
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => '',
            'updateButtonLabel' => '',
            'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
            'updateButtonUrl' => '_aUrl("content/editStaticContent", array("id" => $data->id))',
            'deleteButtonImageUrl' => '',
            'deleteButtonLabel' => '',
            'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
            'deleteButtonUrl' => '_aUrl("content/deleteStaticContent", array("id" => $data->id))',
            'deleteConfirmation' => "Are you sure you wish to delete this static content?",
        ),
    ),
));
?>