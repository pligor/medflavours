<?php
return array(
    'attributes' => array(
        'id' => 'backForm',
        'enctype' => 'multipart/form-data'
    ),
    'elements' => array(
        'product' => array(
            'type' => 'form',
            'elements' => array(
                'category_id' => array(
                    'type' => 'dropdownlist',
                    'items' => Category::getAllLeafs(),
                ),
                'location_id' => array(
                    'type' => 'dropdownlist',
                    'items' => Location::model()->getTree(),
                ),
                'supplier_id' => array(
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(Supplier::model()->findAll(), 'id', 'name'),
                ),
                'name' => array(
                    'type' => 'text',
                ),
                'is_active' => array(
                    'type' => 'checkbox',
                ),
				'featured' => array(
                    'type' => 'checkbox',
                ),
				'brand_name' => array(
					'type' => 'text',
				),
				'supplier_extra_info' => array(
					'type' => 'external.cleditor.ECLEditor',
					'attributes' => array(
						'options' => array(
							'width' => 500,
							'height' => 250,
							'useCSS' => true,
							'controls' => _controls(),
						),
					),
				),
            ),
        ),
        'productMeta' => array(
            'type' => 'form',
            'elements' => array(
                'small_description' => array(
					'type' => 'external.cleditor.ECLEditor',
                    'attributes' => array(
                        'options' => array(
                            'width' => '500',
                            'height' => 250,
                            'useCSS' => true,
                            'controls' => _controls(),
                        ),
                    ),
                ),
                'full_description' => array(
					'type' => 'external.cleditor.ECLEditor',
                    'attributes' => array(
                        'options' => array(
                            'width' => '500',
                            'height' => 250,
                            'useCSS' => true,
                            'controls' => _controls(),
                        ),
                    ),
                ),
                'discount' => array(
                    'type' => 'text',
					'hint' => 'percentage (%). Please type only numbers between 0-100',
                ),
                'vat' => array(
                    'type' => 'text',
                ),
            ),
        ),
        '<hr />',
        '<h2>SEO</h2>',
        'seo' => array(
            'type' => 'form',
            'elements' => array(
                'url' => array(
                    'type' => 'text',
                ),
                'title' => array(
                    'type' => 'text',
                ),
                'description' => array(
                    'type' => 'text',
                ),
                'keywords' => array(
                    'type' => 'text',
                ),
            ),
        ),
    ),
    'buttons' => _formButtons(),
);