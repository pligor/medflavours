<?php

return array(
    'attributes' => array(
        'id' => 'backForm',
        'enctype' => 'multipart/form-data'
    ),
    'elements' => array(
        'supplier' => array(
            'type' => 'form',
            'elements' => array(
                'location_id' => array(
                    'type' => 'dropdownlist',
                    'items' => Location::model()->getTree(),
                ),
                'name' => array(
                    'type' => 'text',
                ),
                'email' => array(
                    'type' => 'text',
                ),
                'address' => array(
                    'type' => 'text',
                ),
                'phone' => array(
                    'type' => 'text',
                ),
            ),
        ),
        '<hr />',
        '<h2>SEO</h2>',
        'seo' => array(
            'type' => 'form',
            'elements' => array(
                'url' => array(
                    'type' => 'text',
                ),
                'title' => array(
                    'type' => 'text',
                ),
                'description' => array(
                    'type' => 'text',
                ),
                'keywords' => array(
                    'type' => 'text',
                ),
            ),
        ),
    ),
    'buttons' => _formButtons(),
);