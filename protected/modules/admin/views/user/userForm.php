<?php

return array(
    'attributes' => array(
        'id' => 'backForm',
    ),
    'elements' => array(
        'user' => array(
            'type' => 'form',
            'elements' => array(
                'username' => array(
                    'type' => 'text',
                ),
                'password' => array(
                    'type' => 'password',
                ),
                'role' => array(
                    'type' => 'dropdownlist',
                    'items' => Role::getArray(),
                ),
            ),
        ),
    ),
    'buttons' => array(
        '<div class="tableBottom formSubmit">',
        'save' => array(
            'type' => 'submit',
            'label' => 'Save',
            'class' => 'myButton'
        ),
        '<div class="ClearFloat"></div></div>'
    ),
);