<div class="tableTop topToolbar">
    <a class="addButton" id="addButton" title="Add User" href="<?php echo _aUrl('user/add'); ?>"><em class="icon16 addItem"></em>Add User</a>
    <div class="ClearFloat"></div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $users->search(),
    'filter' => $users,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'username',
        ),
        array(
            'name' => 'role',
            'value' => 'Role::getText($data->role)',
            'filter' => false,
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '100px'),
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => '',
            'updateButtonLabel' => '',
            'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
            'deleteButtonImageUrl' => '',
            'deleteButtonLabel' => '',
            'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
        ),
    ),
));
?>