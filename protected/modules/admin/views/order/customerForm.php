<?php

return array(
    'attributes' => array(
        'id' => 'backForm',
    ),
    'elements' => array(
        'customer' => array(
            'type' => 'form',
            'elements' => array(
                'fullname' => array(
                    'type' => 'text',
                ),
                'email' => array(
                    'type' => 'text',
                ),
                'phone' => array(
                    'type' => 'text',
                ),
            ),
        ),
    ),
    'buttons' => array(
        '<div class="tableBottom formSubmit">',
        'save' => array(
            'type' => 'submit',
            'label' => 'Save',
            'class' => 'myButton'
        ),
        '<div class="ClearFloat"></div></div>'
    ),
);