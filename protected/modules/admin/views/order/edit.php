<div class="tableMain">

    <?php
    echo $form->renderBegin();
    echo $form->renderButtons();
    echo $form->renderElements();
    ?>

    <h2>Items</h2>
    <div id="order-items">
        <?php $this->renderPartial('_orderItems', array('items' => $items)); ?>
    </div>

    <?php
    echo $form->renderButtons();
    echo $form->renderEnd();
    ?>

</div>
<script>
    $(document).ready(function(){
        $(".order-item").live('click', function(){
            var id = $(this).attr("id");
            var model = $(this).attr("model");
            sendRequest(id, model);
        })
    })
    function sendRequest(id, model) {
        $.ajax({
            url: "<?php echo _aUrl('order/deleteOrderItem'); ?>",
            data: {
                id: id,
                model: model,
                order: <?php echo $orderId; ?>
            },
            type: 'POST',
            success: function(response) {
                if (response.error)
                    alert('Could not delete item from order');
                else
                    $("#order-items").html(response.data);
            }
        })
    }
</script>