<?php

return array(
    'attributes' => array(
        'id' => 'backForm',
        'enctype' => 'multipart/form-data'
    ),
    'elements' => array(
        'customer' => array(
            'type' => 'form',
            'elements' => array(
                'fullname' => array(
                    'type' => 'text',
                ),
                'email' => array(
                    'type' => 'text',
                ),
                'phone' => array(
                    'type' => 'text',
                ),
                'creditcard' => array(
                    'type' => 'textarea',
                ),
            ),
//        ),
//        'order' => array(
//            'type' => 'form',
//            'elements' => array(
//                'fullname' => array(
//                    'type' => 'text',
//                ),
//                'email' => array(
//                    'type' => 'text',
//                ),
//                'phone' => array(
//                    'type' => 'text',
//                ),
//                'creditcard' => array(
//                    'type' => 'textarea',
//                ),
//            ),
        ),
        '<hr />',
    ),
    'buttons' => array(
        '<div class="tableBottom formSubmit">',
        'save' => array(
            'type' => 'submit',
            'label' => 'Save',
            'class' => 'myButton'
        ),
        '<div class="ClearFloat"></div></div>'
    ),
);