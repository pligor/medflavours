<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $customers->search(),
    'filter' => $customers,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'fullname',
            'htmlOptions' => array('width' => '20%'),
        ),
        array(
            'name' => 'email',
            'htmlOptions' => array('width' => '20%'),
        ),
        array(
            'name' => 'phone',
            'htmlOptions' => array('width' => '20%'),
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '10%'),
            'template' => '{update}{delete}{view}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => '',
                    'label' => '',
                    'options' => array('class' => 'edit tooltip', 'original-title' => 'view/edit'),
                    'url' => '_aUrl("order/editCustomer", array("id" => $data->id))',
                ),
                'delete' => array(
                    'imageUrl' => '',
                    'label' => '',
                    'options' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
                    'url' => '_aUrl("order/deleteCustomer", array("id" => $data->id))',
                ),
                'view' => array(
                    'label' => '',
                    'imageUrl' => '',
                    'url' => '_aUrl("order/customerOrders", array("id" => $data->id))',
                    'options' => array('class' => 'view tooltip', 'original-title' => 'view orders'),
                    'visible' => 'count($data->orders)',
                ),
            ),
        ),
    ),
));
?>