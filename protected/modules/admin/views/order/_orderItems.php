<?php foreach ($items as $item) : ?>
    <div class="order-item" id="<?php echo $item['id']; ?>" model="<?php echo $item['model']; ?>">
        <span class="name"><?php echo $item['name']; ?></span>
        <span class="delete" title="remove this item">&nbsp;</span>
    </div>
<?php endforeach; ?>