<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $orders->search(),
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'date_entered',
            'header' => 'Date',
            'value' => 'Utils::date($data->date_entered)',
            'filter' => false,
            'htmlOptions' => array('class' => 'grid-15-column'),
        ),
        array(
            'name' => 'id',
            'filter' => false,
        ),
        array(
            'name' => 'total',
            'header' => 'Total (€)',
            'filter' => false,
            'htmlOptions' => array('class' => 'grid-amount-column'),
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '10%'),
            'template' => '{view}',
            'buttons' => array(
                'view' => array(
                    'imageUrl' => '',
                    'label' => '',
                    'options' => array('class' => 'edit tooltip', 'original-title' => 'view/edit order'),
                    'url' => '_aUrl("order/editOrder", array("id" => $data->id))',
                ),
            ),
        ),
    ),
));
?>