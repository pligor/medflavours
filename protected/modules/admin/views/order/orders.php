<div class="tableTop topToolbar">
    <a class="addButton <?php echo $status == OrderStatus::SENT_TO_CUSTOMER ? 'grey' : ''; ?>" id="addButton" title="Completed" href="<?php echo _aUrl('order/index', array('status' => OrderStatus::SENT_TO_CUSTOMER)); ?>"><em class="icon16 addItem"></em>Completed (<?php echo $counts['sentToCustomer']; ?>)</a>
    <a class="addButton <?php echo $status == OrderStatus::ARRIVED_AT_WAREHOUSE ? 'grey' : ''; ?>" id="addButton" title="Arrived at warehouse" href="<?php echo _aUrl('order/index', array('status' => OrderStatus::ARRIVED_AT_WAREHOUSE)); ?>"><em class="icon16 addItem"></em>Arrived at warehouse (<?php echo $counts['arrivedAtWarehouse']; ?>)</a>
    <a class="addButton <?php echo $status == OrderStatus::SENT_TO_WAREHOUSE ? 'grey' : ''; ?>" id="addButton" title="Sent to warehouse" href="<?php echo _aUrl('order/index', array('status' => OrderStatus::SENT_TO_WAREHOUSE)); ?>"><em class="icon16 addItem"></em>Sent to warehouse (<?php echo $counts['sentToWarehouse']; ?>)</a>
    <a class="addButton <?php echo $status == OrderStatus::SENT_TO_SUPPLIER ? 'grey' : ''; ?>" id="addButton" title="Sent to supplier" href="<?php echo _aUrl('order/index', array('status' => OrderStatus::SENT_TO_SUPPLIER)); ?>"><em class="icon16 addItem"></em>Sent to supplier (<?php echo $counts['sentToSupplier']; ?>)</a>
    <a class="addButton <?php echo $status == OrderStatus::OPEN ? 'grey' : ''; ?>" id="addButton" title="Open orders" href="<?php echo _aUrl('order/index', array('status' => OrderStatus::OPEN)); ?>"><em class="icon16 addItem"></em>Open orders (<?php echo $counts['open']; ?>)</a>
    <div class="ClearFloat"></div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $orders->search(),
    'filter' => $orders,
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'date_entered',
            'header' => 'Date',
            'value' => 'Utils::date($data->date_entered)',
            'filter' => false,
            'htmlOptions' => array('class' => 'grid-15-column'),
        ),
        array(
            'type' => 'raw',
            'name' => 'customer.fullname',
            'header' => 'Customer Name',
//            'value' => array($this, 'renderCustomerName'),
            'value' => 'CHtml::link($data->customer->fullname, _aUrl("order/editOrder", array("id" => $data->id)))',
            'filter' => CHtml::textField('Order[customer_fullname]'),
            'htmlOptions' => array('class' => 'grid-35-column'),
        ),
        array(
            'name' => 'customer.email',
            'header' => 'Customer Email',
//            'value' => array($this, 'renderCustomerEmail'),
            'value' => '$data->customer->email',
            'filter' => CHtml::textField('Order[customer_email]'),
            'htmlOptions' => array('class' => 'grid-40-column'),
        ),
        array(
            'name' => 'total',
            'header' => 'Total (€)',
            'filter' => false,
            'htmlOptions' => array('class' => 'grid-amount-column'),
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '11%'),
            'template' => '{update}{delete}{retract}{progress}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => '',
                    'label' => '',
                    'options' => array('class' => 'edit tooltip', 'original-title' => 'view/edit'),
                    'url' => '_aUrl("order/editOrder", array("id" => $data->id))',
                    'visible' => '$data->status != OrderStatus::SENT_TO_CUSTOMER',
                ),
                'delete' => array(
                    'imageUrl' => '',
                    'label' => '',
                    'options' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
                    'url' => '_aUrl("order/deleteOrder", array("id" => $data->id))',
                ),
                'progress' => array(
                    'label' => '',
                    'imageUrl' => '',
                    'url' => '_aUrl("order/progressOrder", array("id" => $data->id))',
                    'options' => array('class' => 'next', 'title' => 'progress order'),
                    'visible' => '$data->status != OrderStatus::SENT_TO_CUSTOMER',
                ),
                'retract' => array(
                    'label' => '',
                    'imageUrl' => '',
                    'url' => '_aUrl("order/retractOrder", array("id" => $data->id))',
                    'options' => array('class' => 'previous', 'title' => 'retract order'),
                    'visible' => '$data->status != OrderStatus::OPEN',
                ),
            ),
        ),
    ),
));
?>