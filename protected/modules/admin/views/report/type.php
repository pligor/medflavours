<div class="tableTop topToolbar">
    <div class="header">REPORTS BY TYPE</div>
</div>
<div class="tableMain">
    <?php echo CHtml::form('', 'post', array('id' => 'backForm')); ?>
    <div class="row">
        <?php echo CHtml::label('Select a category:', 'Order[type]'); ?>
        <?php echo CHtml::dropDownList('Order[type]', $search['type'], array('activity' => 'Activity', 'event' => 'Event', 'attraction' => 'Attraction', 'product' => 'Product', 'package' => 'Package')); ?>
    </div>
    <div class="row">
        <?php echo CHtml::label('Choose a period:', 'Order[start_date]'); ?>
        <?php echo CHtml::textField('Order[start_date]', $search['start_date'], array('style' => 'display: none')); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => "start_date",
            'value' => $search['start_date'] ? date('d-m-Y', strtotime($search['start_date'])) : '',
            'options' => array(
                'showAnim' => 'fold',
                'altFormat' => 'yy/mm/dd',
                'altField' => '#Order_start_date',
                'dateFormat' => 'dd-mm-yy',
            ),
            'htmlOptions' => array(
                'class' => 'short-text'
            ),
        ));
        ?>
        <?php echo CHtml::textField('Order[end_date]', $search['end_date'], array('style' => 'display: none')); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => "end_date",
            'value' => $search['end_date'] ? date('d-m-Y', strtotime($search['end_date'])) : '',
            'options' => array(
                'showAnim' => 'fold',
                'altFormat' => 'yy/mm/dd',
                'altField' => '#Order_end_date',
                'dateFormat' => 'dd-mm-yy',
                'showButtonPanel' => true,
            ),
            'htmlOptions' => array(
                'class' => 'short-text'
            ),
        ));
        ?>
        <?php echo CHtml::linkButton('Clear', array('onclick' => 'js:$("#Order_start_date, #Order_end_date").val(""); $("#start_date, #end_date").val(""); return false;')); ?>
    </div>
    <br /><br />
    <div class="row"> 
        <div class="tableBottom formSubmit">
            <?php echo CHtml::submitButton('Show', array('class' => 'myButton')); ?>
            <div class="ClearFloat"></div>
        </div>
    </div>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $orders->search(),
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'order.date_entered',
            'header' => 'Date',
            'value' => 'Utils::date($data->order->date_entered)',
            'filter' => false,
            'htmlOptions' => array('class' => 'grid-15-column'),
        ),
        array(
            'name' => "{$searchType}.name",
            'type' => 'raw',
            'value' => 'CHtml::link($data->'.$searchType.'->name, _aUrl("content/edit'.ucfirst($searchType).'", array("id" => $data->'.$searchType.'->id)))',
            'htmlOptions' => array('width' => '40%'),
        ),
        array(
            'name' => 'amount',
            'htmlOptions' => array('class' => 'grid-amount-column'),
        ),
        array(
            'name' => 'order.id',
            'header' => 'Order ID',
            'type' => 'raw',
            'value' => 'CHtml::link($data->order->id, _aUrl("order/editOrder", array("id" => $data->order->id)))',
            'htmlOptions' => array('class' => 'grid-amount-column'),
            'footer' => '<div style="text-align: right"><strong>TOTAL:</strong></div>',
        ),
        array(
            'class' => 'application.components.TotalColumn',
            'name' => 'total',
            'header' => 'Total (€)',
            'output' => '$value',
            'footer' => true,
            'type' => 'html',
            'htmlOptions' => array('class' => 'grid-amount-column'),
        ),
    ),
));
?>
