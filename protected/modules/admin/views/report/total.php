<div class="tableTop topToolbar">
    <?php echo CHtml::form(); ?>
    <span class="specTextLabel">Selecting a date will show you all sales around that date for 3 months: </span>
    <span class="specTextField">
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => "date",
            'value' => $date,
            'options' => array(
                'showAnim' => 'fold',
                'dateFormat' => 'yy/mm/dd',
                'numberOfMonths' => 3,
                'stepMonths' => 3,
            ),
            'htmlOptions' => array(
                'class' => 'short-text'
            ),
        ));
        ?>
    </span>
    <?php echo CHtml::submitButton('Show'); ?>
    <?php echo CHtml::endForm(); ?>
    <div class="ClearFloat"></div>
</div>
<?php if (empty($totals)) : ?>
    No sales yet!
    <?php
else :
    $this->Widget('ext.highcharts.HighchartsWidget', array(
        'options' => array(
            'credits' => array(
                'text' => 'Nelios.com',
                'href' => 'http://www.nelios.com',
            ),
            'title' => array('text' => 'Sales Per Day (90 days)'),
            'xAxis' => array(
                'categories' => $totals['date'],
            ),
            'yAxis' => array(
                'title' => array('text' => 'Sales (€)')
            ),
            'legend' => array(
                'layout' => 'vertical',
                'align' => 'right',
                'verticalAlign' => 'top',
                'x' => -10,
                'y' => 100,
            ),
            'tooltip' => array(
                'formatter' => "js:function() {
                        return '<b>'+ this.series.name +' Sales</b><br />'+this.x+'<br />€'+Highcharts.numberFormat(this.y,2);
                    }",
            ),
            'series' => array(
                array('name' => 'Total', 'data' => $totals['total']),
                array('name' => 'Activities', 'data' => $totals['act']),
                array('name' => 'Events', 'data' => $totals['evnt']),
                array('name' => 'Attractions', 'data' => $totals['att']),
                array('name' => 'Products', 'data' => $totals['pro']),
                array('name' => 'Packages', 'data' => $totals['pak']),
            ),
            'theme' => 'grid',
        )
    ));
endif;
?>