<div class="tableTop topToolbar">
    <?php echo CHtml::form(); ?>
    <span class="specTextLabel">Enter threshold limit (<99):</span>
    <span class="specTextField"><?php echo CHtml::textField('threshold', $threshold, array('size' => '1', 'maxlength' => '2')); ?></span>
    <span><?php echo CHtml::submitButton('Ok'); ?>
    <div class="ClearFloat"></div>
    <?php CHtml::endForm(); ?>
</div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $products->searchStock(),
    'pager' => array('class' => 'CLinkPager', 'header' => ''),
    'template' => '{items}{pager}',
    'cssFile' => '',
    'columns' => array(
        array(
            'name' => 'name',
        ),
        array(
            'name' => 'category_id',
            'header' => 'Category',
            'value' => '$data->category->name',
        ),
        array(
            'name' => 'supplier_id',
            'header' => 'Supplier',
            'value' => '$data->supplier->name',
        ),
        array(
            'name' => 'location_id',
            'header' => 'Location',
            'value' => '$data->location->name',
        ),
        array(
            'name' => 'stock',
        ),
        array(
            'class' => 'CButtonColumn',
            'header' => 'Tools',
            'headerHtmlOptions' => array('width' => '100px'),
            'template' => '{update}{delete}',
            'updateButtonImageUrl' => '',
            'updateButtonLabel' => '',
            'updateButtonOptions' => array('class' => 'edit tooltip', 'original-title' => 'edit'),
            'updateButtonUrl' => '_aUrl("content/editProduct", array("id" => $data->id))',
            'deleteButtonImageUrl' => '',
            'deleteButtonLabel' => '',
            'deleteButtonOptions' => array('class' => 'delete tooltip', 'original-title' => 'delete'),
            'deleteButtonUrl' => '_aUrl("content/deleteProduct", array("id" => $data->id))',
        ),
    ),
));
?>