<?php

class OrderController extends AdminController {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'), // Deny guests
            ),
            array(
                'allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    public function actionIndex($status = OrderStatus::OPEN) {

        $orders = new Order('search');

        if (isset($_GET['Order']))
            $orders->attributes = $_GET['Order'];

        $orders->status = $status;

        $orderCounts = array(
            'open' => Order::model()->count('status = :status', array(':status' => OrderStatus::OPEN)),
            'sentToSupplier' => Order::model()->count('status = :status', array(':status' => OrderStatus::SENT_TO_SUPPLIER)),
            'sentToWarehouse' => Order::model()->count('status = :status', array(':status' => OrderStatus::SENT_TO_WAREHOUSE)),
            'arrivedAtWarehouse' => Order::model()->count('status = :status', array(':status' => OrderStatus::ARRIVED_AT_WAREHOUSE)),
            'sentToCustomer' => Order::model()->count('status = :status', array(':status' => OrderStatus::SENT_TO_CUSTOMER)),
        );

        $this->render('orders', array('orders' => $orders, 'counts' => $orderCounts, 'status' => $status));
    }

    public function actionProgressOrder($id) {
        $order = Order::model()->findByPk($id);
        $order->status = OrderStatus::next($order->status);
        $order->update(array('status'));

        $this->redirect(_aUrl('order/index', array('status' => $order->status)));
    }

    public function actionRetractOrder($id) {
        $order = Order::model()->findByPk($id);
        $order->status = OrderStatus::previous($order->status);
        $order->update(array('status'));

        $this->redirect(_aUrl('order/index', array('status' => $order->status)));
    }

    public function actionEditOrder($id) {
        $order = Order::model()->findByPk($id);

        $form = new CForm('application.modules.admin.views.order.orderForm');
        $form['customer']->model = $order->customer;

        if ($form->submitted('save') && $form->validate()) {

            $product = $form['customer']->model;

            if ($product->save(false))
                $this->redirect(_aUrl('order/index'));
        }

        $items = $this->_getOrderItems($order);

        $this->render('edit', array('form' => $form, 'items' => $items, 'orderId' => $order->id));
    }
    
    public function actionDeleteOrder($id) {
        Order::model()->deleteByPk($id);
        
        return;
    }

    public function actionDeleteOrderItem() {
        if (Utils::isAjax()) {
            $data = array('error' => false);

            $model = $_POST['model'];

            switch ($model) {
                case 'OrderProduct':
                    $item = $model::model()->findByPk(array('product_id' => $_POST['id'], 'order_id' => $_POST['order']));
                    break;
                case 'OrderActivity':
                    $item = $model::model()->findByPk(array('activity_id' => $_POST['id'], 'order_id' => $_POST['order']));
                    break;
                case 'OrderPackage':
                    $item = $model::model()->findByPk(array('package_id' => $_POST['id'], 'order_id' => $_POST['order']));
                    break;
            }
            if ($item->amount > 1) {
                --$item->amount;
                $item->update('amount');
            } else {
                $item->delete();
            }


            $items = $this->_getOrderItems(Order::model()->findByPk($_POST['order']));
            $data['data'] = $this->renderPartial('_orderItems', array('items' => $items), true);

            Utils::jsonReturn($data);
        }
    }

    public function actionCustomers() {
        $customers = new Customer;
        if (isset($_GET['Customer']))
            $customers->attributes = $_GET['Customer'];

        $this->render('customers', array('customers' => $customers));
    }

    public function actionEditCustomer($id) {
        $form = new CForm('application.modules.admin.views.order.customerForm');
        $form['customer']->model = Customer::model()->findByPk($id);

        if ($form->submitted('save') && $form->validate()) {

            $product = $form['customer']->model;

            if ($product->save(false))
                $this->redirect(_aUrl('order/customers'));
        }

        $this->render('editCustomer', array('form' => $form));
    }

    public function actionDeleteCustomer($id) {
        Customer::model()->deleteByPk($id);

        $this->redirect(_aUrl('order/customers'));
    }

    public function actionCustomerOrders($id) {
        $orders = new Order;
        $orders->customer_id = $id;

        $this->render('customerOrders', array('orders' => $orders));
    }

    /**
     * Return all the items in an order non-grouped
     * @param Order $order
     * @return array A multidimensional array with all the items in an order
     */
    private function _getOrderItems(Order $order) {
        $items = array();
        $c = 0;
        foreach ($order->products as $product) {
            for ($k = 0; $k < $product->amount; ++$k) {
                $items[$c]['id'] = $product->product->id;
                $items[$c]['model'] = 'OrderProduct';
                $items[$c]['name'] = $product->product->name;
                ++$c;
            }
        }
        foreach ($order->activities as $activity) {
            for ($k = 0; $k < $activity->amount; ++$k) {
                $items[$c]['id'] = $activity->activity->id;
                $items[$c]['model'] = 'OrderActivity';
                $items[$c]['name'] = $activity->activity->name;
                ++$c;
            }
        }
        foreach ($order->packages as $package) {
            for ($k = 0; $k < $package->amount; ++$k) {
                $items[$c]['id'] = $package->package->id;
                $items[$c]['model'] = 'OrderPackage';
                $items[$c]['name'] = $package->package->name;
                ++$c;
            }
        }

        return $items;
    }

}