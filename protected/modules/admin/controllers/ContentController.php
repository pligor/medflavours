<?php

/**
 * The controller class for all the content management
 */
class ContentController extends AdminController {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'), // Deny guests
            ),
            array(
                'allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }
	
	public function actions() {
		// return external action classes, e.g.:
		return array(
			//'action1' => 'path.to.ActionClass',
			'ActCategory' => array(
				'class' => 'ext.actions.AdjacencyGrid',
				'model_name' => 'ActCategory',
			),
			'EventCategory' => array(
				'class' => 'ext.actions.AdjacencyGrid',
				'model_name' => 'EventCategory',
			),
			'AttractionCategory' => array(
				'class' => 'ext.actions.AdjacencyGrid',
				'model_name' => 'AttractionCategory',
			),
		);
	}
	
	/**
	 * @author pligor
     * The form for category management
     * @param ActCategory|EventCategory|AttractionCategory $model 
     */
    private function _catForm($model) {
		//$form = $model->form;
		$formConfig = $model->getFormConfig();
		$formConfig['attributes']['id'] = 'backForm';	//@todo Research if this plays any role, maybe in css
		
		$formConfig['buttons']['go_back'] = array(
			'type' => 'submit',
			'label' => Yii::t('', 'Save and go back'),
		);
		
		$form = new AdminForm($formConfig, $model);	//all subforms get the model from their parent
		
		if (($form->submitted('save',true) || $form->submitted('go_back', true)) && $form->validate()) {
			if( $model->save(false) )	{	//no need to revalidate
				if( $form->submitted('save',false)  ) {
					$this->redirect($this->createUrl('content/edit_cat', array('id' => $model->id,'class'=>get_class($model))));
				}
				elseif( $form->submitted('go_back',false) ) {
					$this->redirect($this->createUrl('content/'.get_class($model), array('parent_id' => $model->parent_id)));
				}
			}
			else {
				$model->addError('', 'model could not be saved in database');
			}
		}

        /*
		$form = new CForm('application.modules.admin.views.content.categoryForm');
        $form['category']->model = $model;
		$form['seo']->model = $model->seo ? : new Seo;
		if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {
			$category = $form['category']->model;
            $seo = $form['seo']->model;
            if ($seo->save()) {
                $category->seo_id = $seo->id;
                if ($category->save(false)) {
                    if ($form->submitted('goBack'))
                        $this->redirect(_aUrl('content/editCategory', array('id' => $category->id)));
                    else
                        $this->redirect(_aUrl('content/categories'));
                }
            }
        }*/

        $this->render('add', compact('form'));
    }
	
	public function actionAdd_cat($class) {
		//$class = substr($this->action->id, strlen('add'));
		$model = new $class();
		$this->_catForm($model);
    }

    public function actionEdit_cat($id,$class) {
		//$class = substr($this->action->id, strlen('edit'));
		$model = $class::model()->findByPk($id);
        $this->_catForm($model);
    }

    public function actionDelete_cat($id,$class) {
		//$class = substr($this->action->id, strlen('delete'));
		$deletedRows = $class::model()->deleteByPk($id);
		if($deletedRows != 1) { throw new CException('deletion of row has failed'); }
		if( !Yii::app()->getRequest()->isAjaxRequest ) { $this->redirect( $this->createUrl('content/'.$class) ); }
    }
	
	/*
	public function actionAddActCategory() {
		//$class = substr($this->action->id, strlen('add'));
		$model = new $class();
		$this->_catForm($model);
    }

    public function actionEditActCategory($id) {
		$class = substr($this->action->id, strlen('edit'));
		$model = $class::model()->findByPk($id);
        $this->_catForm($model);
    }

    public function actionDeleteActCategory($id) {
		$class = substr($this->action->id, strlen('delete'));
		$deletedRows = $class::model()->deleteByPk($id);
		if($deletedRows != 1) { throw new CException('deletion of row has failed'); }
		if( !Yii::app()->getRequest()->isAjaxRequest ) { $this->redirect( $this->createUrl('content/'.$class) ); }
    }
	//*/
	
	public function actionAjax_get_cat_loc_cont() {
		if(!Yii::app()->request->isAjaxRequest) {
			throw new CHttpException('this should be an ajax request');
		}
		$class = 'CategoryLocationContent';
		
		$model = $class::model()->findByPk($_GET[$class]);
		if($model==null) {
			$model = new $class();
			$model->attributes = $_GET[$class];
		}
		
		$this->renderPartial($this->action->id, compact('model'), false, true);
	}
	
	public function actionCat_loc_content() {
		$model = new CategoryLocationContent();
		
		$form = $model->getForm('AdminForm');
		
		if($form->submitted('submit_categorylocationcontent', true) && $form->validate()) {
			$old_model = CategoryLocationContent::model()->findByPk(array(
				'category_id' => $model->category_id,
				'location_id' => $model->location_id,
			));
			if( is_object($old_model) ) {	//is old ?
				$old_model->content = $model->content;
				$model = & $old_model;
			}
			
			if(!$model->save(false)) {
				$model->addError('', 'model could not be saved');
			}
		}
		else {
			$category_ids = array_keys( Category::getAllLeafs() );
			$category_id = reset($category_ids);
			$location_ids = array_keys( Location::model()->getTree() );
			$location_id = reset($location_ids);

			if($category_id != false && $location_id != false) {	//perform initialization if necessary
				$model = CategoryLocationContent::model()->findByPk(compact('category_id','location_id'));
			}
			$form = $model->getForm('AdminForm');
		}
		
		$this->render('category_location_content', compact('form'));
	}
	
	protected $modelNames = array(
		'left' => 'Category',
		'right' => 'Location',
	);
	
	/**
	 * H logiki exei ws eksis. Kane click se ena node kai tha emfanistoun ola ta nodes auths ths pleuras
	 * Sthn antipera oxthi tha emfanistoun ola ta antikeimena pou to antiprosopeuoun mazi me olous tous ancestors
	 * gia na ginei h provoli tou dentrou. @todo DEN EMFANIZONTAI OMWS KAI TA CHILDREN. prepei na allaksei elafrws o
	 * kodikas gia na ypostirizetai kai auto. An kai den fainetai poly logiko
	 * @param type $model_id
	 * @param type $model_clicked 
	 */
	public function actionNavigation($model_id=null, $model_clicked='Category') {
		//save sto session to clicked id an theloume array
		$selected_ids = array($model_id);
		
		$target_ids = null;
		
		$model_targeted = $this->modelNames['left'];
		if($model_clicked == $this->modelNames['left']) {
			$model_targeted = $this->modelNames['right'];
		}
		
		if($model_id === null) {
			$model_targeted = null;	//neutralize name of targeted model
		}
		else {
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $selected_ids);
			$models = $model_clicked::model()->findAll($criteria);
			
			$target_ids = array();
			foreach($models as $model) {
				$targets = $model->getAllRelated();
				
				foreach($targets as $target) {
					$target_ids[] = $target->id;
					$target_ids = array_merge($target_ids, $target->getAncestorIds());
				}
			}
			$target_ids = array_unique($target_ids);
		}
		
		$this->render('navigation', compact('model_clicked','model_targeted','target_ids'));
	}
	
	/**
	 * @deprecated by action navigation
	 * @param type $model_id
	 * @param type $model_clicked 
	 */
	public function actionNavigate($model_id=null,$model_clicked='Category') {
		//save sto session to clicked id an theloume array
		$selected_ids = array($model_id);
		
		if($model_clicked == $this->modelNames['left']) {
			$model_targeted = $this->modelNames['right'];
		}
		else {
			$model_targeted = $this->modelNames['left'];
		}
		
		if($model_id === null) {
			$target_models = $model_targeted::model()->findAll();
			$target_ids = array();
			foreach($target_models as $target_model) {
				$target_ids[] = $target_model->id;
			}
			$target_ancestor_ids = array();
		}
		else {
			$clickedModel = $model_clicked::model()->findByPk($selected_ids[0]);
			$products = $clickedModel->products;
			$target_ids = array();
			$attribute = strtolower($model_targeted) .'_id';
			foreach($products as $product) {
				$target_ids[] = $product->$attribute;
			}
			
			$criteria = new CDbCriteria();
			$criteria->addInCondition('id', $target_ids);
			$target_models = $model_targeted::model()->findAll($criteria);
			$target_ancestor_ids = $this->getAllAncestorIds($target_models);
			
			//$locationItems = $this->getItems('Location', $location_ids, $ancestor_ids);
		}
		//$locationItems = $this->getItems($model_targeted, $target_ids, $target_ancestor_ids);
		$itemsArray[$model_targeted] = $this->getItems($model_targeted, $target_ids, $target_ancestor_ids);
		
		$clicked_models = $model_clicked::model()->findAll();
		$clicked_ids = array();
		foreach($clicked_models as $clicked_model) {
			$clicked_ids[] = $clicked_model->id;
		}
		//$categoryItems = $this->getItems('Category', $category_ids,array(),$clicked_ids);
		$itemsArray[$model_clicked] = $this->getItems($model_clicked, $clicked_ids, array(), $selected_ids);
		
		if(!isset($var)) $var =null;
		$this->render('navigate',compact('itemsArray', 'var', 'categories','categoryItems', 'locations', 'locationItems'));
	}
	
	/**
	 * @deprecated
	 * @param type $modelName
	 * @param type $ids
	 * @param type $ancestor_ids
	 * @param type $selected_ids
	 * @return type 
	 */
	protected function getItems($modelName, $ids, $ancestor_ids=array(),$selected_ids=array()) {
		/*
		$locationItems = array(
			array('id'=>1,'parent_id'=>0,'name'=> CHtml::link('test 1') ),
			array('id'=>2,'parent_id'=>1,'name'=>'test 2'),
		);
		//*/
		$all_ids = array_unique( array_merge($ids,$ancestor_ids) );
		
		$criteria = new CDbCriteria();
		$criteria->addInCondition('id', $all_ids);
		$models = $modelName::model()->findAll($criteria);
		
		$items = array();
		
		foreach($models as $model) {
			//$name = CHtml::link($model->name,'#');
			$url = $this->createUrl('navigate',array(
				'model_id' => $model->id,
				'model_clicked' => $modelName,
			));
			$name = CHtml::link($model->name, $url);
			
			if(!empty($ancestor_ids)) {
				if(in_array($model->id, $ids)) {
					$name = CHtml::tag('b',array(),$name);
				}
			}
			
			if(in_array($model->id, $selected_ids)) {
				$name = CHtml::tag('b',array(),$name);
			}
			
			$items[] = array(
				'id'=> $model->id,
				'parent_id'=> $model->parent_id,
				'name' => $name,
			);
		}
		
		return $items;
	}
	
	/**
	 * @deprecated
	 * @param type $models
	 * @return type 
	 */
	protected function getAllAncestorIds($models) {
		//$merged_ids = $location_ids;
		$ids = array();
		foreach($models as $model) {
			$ancestors = $model->getAncestorIds();
			$ids = array_merge($ids,$ancestors);
		}
		return array_unique($ids);
	}

    /**
     * Product category management
     * @param int $parent_id 
     */
    public function actionCategories($parent_id = null) {

        $categories = new Category('search');
        $categories->parent_id = $parent_id;

        $previous = Category::model()->findByPk($parent_id);
        $previous_id = $previous ? $previous->parent_id : null;

        $this->render('categories', array('categories' => $categories, 'previous_id' => $previous_id, 'parent_id' => $parent_id));
    }

    public function actionAddCategory() {
        $this->_categoryForm(new Category);
    }

    public function actionEditCategory($id) {
        $this->_categoryForm(Category::model()->findByPk($id));
    }

    public function actionDeleteCategory($id) {
        Category::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/categories'));
    }

    /**
     * Location management
     * @param int $parent_id 
     */
    public function actionLocations($parent_id = null) {

        $locations = new Location('search');
        $locations->parent_id = $parent_id;

        $previous = Location::model()->findByPk($parent_id);
        $previous_id = $previous ? $previous->parent_id : null;

        $this->render('locations', array(
			'locations' => $locations,
			'previous_id' => $previous_id,
			'parent_id' => $parent_id,
		));
    }

    public function actionAddLocation() {
        $this->_locationForm(new Location);
    }

    public function actionEditLocation($id) {
        $this->_locationForm(Location::model()->findByPk($id));
    }

    public function actionDeleteLocation($id) {
        Location::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/locations'));
    }

    /**
     * Suppliers management
     */
    public function actionSuppliers() {
        $suppliers = new Supplier('search');

        $this->render('suppliers', array('suppliers' => $suppliers));
    }

    public function actionAddSupplier() {
        $this->_supplierForm(new Supplier);
    }

    public function actionEditSupplier($id) {
        $this->_supplierForm(Supplier::model()->findByPk($id));
    }

    public function actionDeleteSupplier($id) {
        Supplier::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/suppliers'));
    }

    /**
     * Product management
     */
    public function actionProducts() {
        $products = new Product('search');
        if (isset($_GET['Product']))
            $products->attributes = $_GET['Product'];

        $this->render('products', array('products' => $products));
    }

    public function actionAddProduct() {
        $this->_productForm(new Product, new ProductMeta);
    }

    public function actionEditProduct($id) {
        $this->_productForm($p = Product::model()->findByPk($id), ProductMeta::model()->findByAttributes(array('product_id' => $p ? $p->id : null)));
    }

    public function actionDeleteProduct($id) {
        Product::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/products'));
    }

    /**
     * All activities (activities, events, attractions) management
     * Activities are separated in activities, events and attractions
     * but all share the same form.
     */
    public function actionActivities() {
        $activities = new Activity('search');
        $activities->activity_type = ActivityType::ACTIVITY;

        if (isset($_GET['Activity']))
            $activities->attributes = $_GET['Activity'];

        $this->render('activities', array('activities' => $activities, 'type' => 'Activity'));
    }

    public function actionEvents() {
        $activities = new Activity('search');
        $activities->activity_type = ActivityType::EVENT;

        if (isset($_GET['Activity']))
            $activities->attributes = $_GET['Activity'];

        $this->render('activities', array('activities' => $activities, 'type' => 'Event'));
    }

    public function actionAttractions() {
        $activities = new Activity('search');
        $activities->activity_type = ActivityType::ATTRACTION;

        if (isset($_GET['Activity']))
            $activities->attributes = $_GET['Activity'];

        $this->render('activities', array('activities' => $activities, 'type' => 'Attraction'));
    }

    public function actionAddActivity() {
        $this->_activityForm(ActivityType::ACTIVITY, new Activity);
    }

    public function actionAddEvent() {
        $this->_activityForm(ActivityType::EVENT, new Activity, 'events', 'editEvent');
    }

    public function actionAddAttraction() {
        $this->_activityForm(ActivityType::ATTRACTION, new Activity, 'attractions', 'editAttraction');
    }

    public function actionEditActivity($id) {
        $this->_activityForm(ActivityType::ACTIVITY, Activity::model()->findByPk($id));
    }

    public function actionEditEvent($id) {
        $this->_activityForm(ActivityType::EVENT, Activity::model()->findByPk($id),  'events', 'editEvent');
    }

    public function actionEditAttraction($id) {
        $this->_activityForm(ActivityType::ATTRACTION, Activity::model()->findByPk($id), 'attractions', 'editAttraction');
    }

    public function actionDeleteActivity($id) {
        Activity::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/activities'));
    }

    public function actionDeleteEvent($id) {
        Activity::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/activities'));
    }

    public function actionDeleteAttraction($id) {
        Activity::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/activities'));
    }

    /**
     * Packages management
     */
    public function actionPackages() {
        $packages = new Package('search');
        if (isset($_GET['Package']))
            $packages->attributes = $_GET['Package'];

        $this->render('packages', array('packages' => $packages));
    }

    public function actionAddPackage() {
        $this->_packageForm(new Package);
    }

    public function actionEditPackage($id) {
        $this->_packageForm(Package::model()->findByPk($id));
    }

    public function actionDeletePackage($id) {
        Package::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/packages'));
    }

    /**
     * Product properties management
     */
    public function actionProperties() {
        $properties = new Property('search');
        if (isset($_GET['Property']))
            $properties->attributes = $_GET['Property'];

        $this->render('properties', array('properties' => $properties));
    }

    public function actionAddProperty() {
        $this->_propertyForm(new Property);
    }

    public function actionEditProperty($id) {
        $this->_propertyForm(Property::model()->findByPk($id));
    }

    public function actionDeleteProperty($id) {
        Property::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/properties'));
    }

    /**
     * Static content management
     */
    public function actionStaticContent() {
        $static = new StaticContent('search');

        $this->render('staticContent', array('static' => $static));
    }

    public function actionAddStaticContent() {
        $this->_staticContentForm(new StaticContent);
    }

    public function actionEditStaticContent($id) {
        $this->_staticContentForm(StaticContent::model()->findByPk($id));
    }

    public function actionDeleteStaticContent($id) {
        StaticContent::model()->deleteByPk($id);

        $this->redirect(_aUrl('content/staticContent'));
    }

    /**
     * Multiline AJAX rendering
     * @return mixed 
     */
    public function actionActivityPeriodLine() {
        $id = $_REQUEST['id'];

        $this->renderPartial('_activityPeriodLine', array('id' => $id));

        return;
    }

    public function actionRemoveActivityPeriod($id) {
        ActivityPeriod::model()->deleteByPk($id);

        Utils::jsonReturn(array());
    }

    public function actionProductPropertyLine() {
        $id = $_REQUEST['id'];

        $this->renderPartial('_productPropertyLine', array('id' => $id));

        return;
    }

    public function actionRemoveProductProperty($id) {
        ProductProperty::model()->deleteByPk($id);

        Utils::jsonReturn(array());
    }

    public function actionRemovePhoto($id, $class) {
        $ar = $class::model()->findByPk($id);

        if (unlink(Utils::photoUploadUrl($ar->photo)))
            $ar->delete();

        Utils::jsonReturn(array());
    }

    /*     * ************FORMS*****************
     * 
     *  All forms must render with 4 variables:
     * 
     * CForm|bool $form - The form
     * EGMap|bool $gMap - Google map object if there is one
     * array|bool $multiline - An array consisting of the model, the partial view and an object with existing values
     * array|bool $photo - An array consisting of the object containing the photos of the model
     * 
     */

    /**
     * The form for category management
     * @param Category $model 
     */
    private function _categoryForm(Category $model) {
        $form = new CForm('application.modules.admin.views.content.categoryForm');
        $form['category']->model = $model;
        $form['seo']->model = $model->seo ? : new Seo;

        if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {

            $category = $form['category']->model;

            $seo = $form['seo']->model;
            if ($seo->save()) {
                $category->seo_id = $seo->id;
                if ($category->save(false)) {
                    if ($form->submitted('goBack'))
                        $this->redirect(_aUrl('content/editCategory', array('id' => $category->id)));
                    else
                        $this->redirect(_aUrl('content/categories'));
                }
            }
        }

        $this->render('add', array('form' => $form, 'gMap' => false, 'multiline' => false, 'photo' => false));
    }

    /**
     * The form for location management
     * @param Location $model 
     */
    private function _locationForm(Location $model) {
        $form = new CForm('application.modules.admin.views.content.locationForm');
        $form['location']->model = $model;
        $form['seo']->model = $model->seo ? : new Seo;

        if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {

            $images = CUploadedFile::getInstancesByName('images');

            $location = $form['location']->model;

            $seo = $form['seo']->model;
            if ($seo->save()) {
                $location->seo_id = $seo->id;
                if ($location->save(false)) {
                    if (isset($images) && count($images) > 0) {
                        $this->_uploadImages($images, $location);
                    }
                }
            }

            if ($form->submitted('goBack'))
                $this->redirect(_aUrl('content/editLocation', array('id' => $location->id)));
            else
                $this->redirect(_aUrl('content/locations'));
        }

        $this->render('add', array(
			'form' => $form,
			'gMap' => false,
			'multiline' => false,
			'photo' => array(
				'photos' => $model->locationPhotos,
			),
		));
    }

    /**
     * The form for supplier management
     * @param Supplier $model 
     */
    private function _supplierForm(Supplier $model) {
        $form = new CForm('application.modules.admin.views.content.supplierForm');
        $form['supplier']->model = $model;
        $form['seo']->model = $model->seo ? : new Seo;

        if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {

            $supplier = $form['supplier']->model;

            $seo = $form['seo']->model;
            if ($seo->save()) {
                $supplier->seo_id = $seo->id;
                if ($supplier->save(false)) {
                    if ($form->submitted('goBack'))
                        $this->redirect(_aUrl('content/editSupplier', array('id' => $supplier->id)));
                    else
                        $this->redirect(_aUrl('content/suppliers'));
                }
            }
        }

        $this->render('add', array('form' => $form, 'gMap' => false, 'multiline' => false, 'photo' => false));
    }
	
	/**
	 * @deprecated ajax if used will be replaced by the RESTful API (backbone)
	 * AJAX
	 * @author pligor
	 * @throws CException 
	 */
	/*public function actionAdd_pack() {
		//print "hello test div";//var_dump($_GET);//var_dump($_GET['Pack']);
		$pack_values = $_GET['Pack'];
		$form_properties = $_GET['form_properties'];
		
		$product_id = $pack_values['product_id'];
		
		$model = new Pack();
		$model->attributes = $pack_values;
		$model->product_id = $product_id;
		
		if( !$model->save() ) {
			throw new CException();
		}
		
		$form_id = $model->id;
		$this->renderPartial('_packLine', compact('model','form_properties','form_id'), false, true);
		
		$model = new Pack();
		$model->product_id = $product_id;
		$form_id = substr(md5(time()*rand()),0,6);	//dummy id
		$this->renderPartial('_packLine', compact('model','form_properties','form_id'), false , true);
	}*/
	
	/**
	 * @deprecated ajax if used will be replaced by the RESTful API (backbone)
	 * AJAX
	 * @author pligor 
	 */
	/*public function actionRemove_pack() {
		$form_properties = $_GET['form_properties'];
		$model_id = $form_properties['id'];
		
		Pack::model()->deleteByPk($model_id);
	}*/
	
	public function actionCreate_pack($product_id) {
		$model = new Pack;
		$model->product_id = $product_id;
        $this->_packForm($model);
    }

    public function actionEdit_pack($id) {
        $this->_packForm(Pack::model()->findByPk($id));
    }

    public function actionDelete_pack($id) {
        $product_id = Pack::model()->findByPk($id)->product_id;
		
		Pack::model()->deleteByPk($id);
		
		$this->redirect($this->createUrl('content/editProduct', array('id' => $product_id)));
    }
	
	protected function _packForm(Pack $model) {
		$formConfig = $model->getFormConfig();
		$formConfig['attributes']['id'] = 'backForm';	//@todo Not really sure if this plays any role, maybe in css
		
		$formConfig['buttons']['submit_pack_and_goback'] = array(
			'type' => 'submit',
			'label' => Yii::t('', 'Submit Pack and go back'),
		);
		
		$form = new AdminForm($formConfig, $model);	//all subforms get the model from their parent
		
		if (($form->submitted('submit_pack',true) || $form->submitted('submit_pack_and_goback',true)) && $form->validate()) {
			if( !$model->save(false) )	{	//no need to revalidate
				$message = 'Pack was not saved';
				throw new CException($message);
			}
			
			if( $form->submitted('submit_pack',false)  ) {
				//dd('pack saved in db and now stay in page');
				$this->redirect($this->createUrl('content/edit_pack', array('id' => $model->id)));
			}
			elseif( $form->submitted('submit_pack_and_goback',false) ) {
				//dd('pack saved in db and go back to the product page');
				$this->redirect($this->createUrl('content/editProduct', array('id' => $model->product_id)));
			}
		}
		
		$this->render('add', array(
			'form' => $form,
			//'photo' => array('photos' => $model->productPhotos),
		));
	}

    /**
     * The form for product management
     * @param Product $model
     * @param mixed $modelMeta A ProductMeta object or null if there isn't one
     */
    private function _productForm(Product $model, $modelMeta) {
        $form = new CForm('application.modules.admin.views.content.productForm');
        $form['product']->model = $model;
        $form['productMeta']->model = $modelMeta ? : new ProductMeta;
        $form['seo']->model = $model->seo ? : new Seo;

        if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {

            $images = CUploadedFile::getInstancesByName('images');

            $product = $form['product']->model;
            $productMeta = $form['productMeta']->model;

            $seo = $form['seo']->model;
            if ($seo->save()) {
                $product->seo_id = $seo->id;
                if ($product->save(false)) {
                    $save = false;
                    foreach ($productMeta->attributes as $value) {
                        if ($value)
                            $save = true;
                    }

                    if ($save) {
                        $productMeta->product_id = $product->id;
                        $productMeta->save(false);
                    }

                    /*if (isset($_POST['propertyName'])) {
                        foreach ($_POST['propertyName'] as $key => $propertyName) {
                            if ($propertyName && $_POST['propertyValue']) {
                                $propertyValue = $_POST['propertyValue'][$key];
                                $property = new ProductProperty;
                                $property->product_id = $product->id;
                                $property->property_id = $propertyName;
                                $property->value = $propertyValue;
                                if (!$property->save()) {
                                    //                            CVarDumper::dump($property->getErrors());
                                }
                            }
                        }
                    }*/

                    if (isset($images) && count($images) > 0) {
                        $this->_uploadImages($images, $product);
                    }

                    if ($form->submitted('goBack'))
                        $this->redirect(_aUrl('content/editProduct', array('id' => $product->id)));
                    else
                        $this->redirect(_aUrl('content/products'));
                }
            }
        }
		
//        CVarDumper::dump($form['product']->model->getErrors());
//        CVarDumper::dump($form['productMeta']->model->getErrors());
		
		$url = $this->module->getJsUrl() .'/multiline_vtwo.js';
        Yii::app()->clientScript->registerScriptFile($url, CClientScript::POS_HEAD);
		
		$data = array(
			'form' => $form,
			'photo' => array(
				'photos' => $model->productPhotos,
			),
			/*'multiline' => array(
				'model' => new ProductProperty,
				'view' => '_productProperties',
				'existing' => $model->productProperties,
			),*/
			/*'packlines' => array(
				'model_name' => 'Pack',
				'existing' => $model->packs,
				'params' => array(
					'product_id' => $model->id,
				),
			),*/
			
		);
		
		if(!$model->isNewRecord) {
			
			Yii::app()->clientScript->registerCss('image_column', '
				.image_column img {
					max-height: 200px;
					max-width: 200px;
				}
			');
			
			
			$data['packs'] = $model->packs;
			
			$url = $this->module->getJsUrl() .'/multiline_vthree.js';
			Yii::app()->clientScript->registerScriptFile($url, CClientScript::POS_HEAD);
			
			$data['productProperties'] = array(
				'model_name' => 'ProductProperty',
				'models' => $model->productProperties,
				'params' => array(
					'product_id' => $model->id,
				),
			);
		}
		
        $this->render('add', $data);
    }
	
	public function actionTesting() {
		$model = new Gposition();
		$form = $model->getForm('GForm');
		
		$model->sw_lat = 28;
		$model->sw_lon = 99;
		$model->ne_lat = 88;
		$model->ne_lon = 77;
		
		$this->render('__testing',array(
			'model' => $model,
			'form' => $form,
		));
	}
	///*
	public function actionCreate_AddonGroup($activity_id) {
		$model = new AddonGroup();
		$model->activity_id = $activity_id;
        $this->_addonGroupForm($model);
    }

    public function actionEdit_AddonGroup($id) {
        $this->_addonGroupForm(AddonGroup::model()->findByPk($id));
    }

    public function actionDelete_AddonGroup($id) {
        $activity_id = AddonGroup::model()->findByPk($id)->activity_id;
		
		AddonGroup::model()->deleteByPk($id);
		
		if( !Yii::app()->getRequest()->isAjaxRequest ) {
			$this->redirect($this->createUrl('content/editActivity', array('id' => $activity_id)));
		}
    }
	
	protected function _addonGroupForm(AddonGroup $model) {
		$formConfig = $model->getFormConfig();
		$formConfig['attributes']['id'] = 'backForm';	//@todo Not really sure if this plays any role, maybe in css
		
		$formConfig['buttons']['submit_and_goback'] = array(
			'type' => 'submit',
			'label' => t('Submit and go back'),
		);
		
		$form = new AdminForm($formConfig, $model);	//all subforms get the model from their parent
		
		if(($form->submitted('submit',true) || $form->submitted('submit_and_goback',true)) && $form->validate()) {
			if( !$model->save(false) )	{	//no need to revalidate
				$message = 'Addon Group was not saved';
				throw new CException($message);
			}
			
			if( $form->submitted('submit',false)  ) {
				$this->redirect($this->createUrl('content/edit_AddonGroup', array('id' => $model->id)));
			}
			elseif( $form->submitted('submit_and_goback',false) ) {
				$this->redirect($this->createUrl('content/editActivity', array('id' => $model->activity_id)));
			}
		}
		
		$data = array(
			'form' => $form,
		);
		
		if( !$model->isNewRecord ) {
			
			$url = $this->module->getJsUrl() .'/multiline_vthree.js';
			Yii::app()->clientScript->registerScriptFile($url, CClientScript::POS_HEAD);
			
			$data['addons'] = array(
				'model_name' => 'Addon',
				'models' => $model->addons,
				'params' => array(
					'addon_group_id' => $model->id,
				),
			);
		}
		
		$this->render('add', $data);
	}
	//*/

    /**
     * The form for activity management
     * @param int $type A constant from ActivityType
     * @param Activity $model
     * @param string $return The action to return after finished with form
     */
    private function _activityForm($type, Activity $model, $return = 'activities', $returnEdit = 'editActivity') {
		//$this->performAjaxValidation($model);
		$model->activity_type = $type;	//whether new or old
		$seo = $model->seo ? : new Seo; //Since PHP 5.3, it is possible to leave out the middle part of the ternary operator. Expression expr1 ?: expr3 returns expr1 if expr1 evaluates to TRUE, and expr3 otherwise
		$gposition = $model->gposition ? : new Gposition();

		$formConfig = array(
			'attributes' => array(
				'id' => 'backForm',
				'enctype' => 'multipart/form-data'
			),
			'activeForm' => array(
				'class'=>'CActiveForm',
				//'enableAjaxValidation' => true,		//defaults to false
				//'enableClientValidation' => false,	//defaults to false
			),
			'elements' => array(
				'activity' => array(
					'type' => 'form',
					'showErrorSummary' => true,
					'elements' => $model->getFormElements(),
				),
				'<hr/><h2>SEO</h2>',
				'seo' => array(
					'type' => 'form',
					'showErrorSummary' => true,
					'elements' => $seo->getFormElements(),
				),
				'<hr/><h3>Position</h3>',
				'gposition' => array(
					'type' => 'form',
					'showErrorSummary' => true,
					'elements' => $gposition->getFormElements(),
				),
			),
			'buttons' => _formButtons(),
		);
		
        //$form = new CForm($formConfig);
		$form = new GForm($formConfig);
        $form['activity']->model = $model;
        $form['seo']->model = $seo;
		$form['gposition']->model = $gposition;
		//$this->elements['dummy']->attributes['form'] = $this->activeFormWidget;

        if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {

            $images = CUploadedFile::getInstancesByName('images');

            $activity = $form['activity']->model;

            $seo = $form['seo']->model;
            if ($seo->save()) {
                $activity->seo_id = $seo->id;
				
				$gposition = $gposition->getExisting();
				
				if( $gposition->save(false) ) {
				
					$activity->gposition_id = $gposition->id;
					
					if ($activity->save(false)) {

						if (isset($images) && count($images) > 0) {
							$this->_uploadImages($images, $activity);
						}
					}
				}
            }

            if ($form->submitted('goBack')) {
                $this->redirect(_aUrl('content/' . $returnEdit, array('id' => $activity->id)));
			}
            else {
                $this->redirect(_aUrl('content/' . $return));
			}
        }
		
		//$url = $this->module->getJsUrl() .'/formline.js';
        //Yii::app()->clientScript->registerScriptFile($url, CClientScript::POS_HEAD);
		//$url = $this->module->getJsUrl() .'/multiline.js';
        //Yii::app()->clientScript->registerScriptFile($url, CClientScript::POS_HEAD);
		
		$data = array(
			'form' => $form,
			//'gMap' => $this->_getGoogleMap($model->long_lat),
            'photo' => array('photos' => $model->activityPhotos),
        );
		
		if( !$model->isNewRecord ) {
			/*$data['addons'] = array(
				'model_name' => 'Addon',
				'models' => $model->addons,
				'params' => array(
					'activity_id' => $model->id,
				),
			);*/
			
			$data['addonGroups'] = $model->addonGroups;
			
			$url = $this->module->getJsUrl() .'/multiline_vthree.js';
			Yii::app()->clientScript->registerScriptFile($url, CClientScript::POS_HEAD);
			
			$data['activityProperties'] = array(
				'model_name' => 'ActivityProperty',
				'models' => $model->activityProperties,
				'params' => array(
					'activity_id' => $model->id,
				),
			);
			
			$data['activityPeriods'] = array(
				'model_name' => 'ActivityPeriod',
				'models' => $model->activityPeriods,
				'params' => array(
					'activity_id' => $model->id,
				),
			);
		}
		
        $this->render('add', $data);
    }
	
	public function actionAdd_addon() {
		//print "somewhere around here";
		$model = new Addon();
		$model->attributes = $_GET['Addon'];
		$model->activity_id = $_GET['activity_id'];
		//print json_encode( $model->getOnlyDbAttributes() );
		//print json_encode( $model->attributes );
		if($model->save()){
			print json_encode( array('id'=>$model->id, 'inputs' => $model->formInputsForJson()) );
		}
		//else null: empty string in ajax response
	}
	
	public function actionRemove_addon($id) {
		$result = Addon::model()->deleteByPk($id);
		//$result = 0;	//zero means failed to delete
		print json_encode($result);
	}

    /**
     * The form for package management
     * @param Package $model 
     */
    private function _packageForm(Package $model) {
        $form = new CForm('application.modules.admin.views.content.packageForm');
        $form['package']->model = $model;
        $form['seo']->model = $model->seo ? : new Seo;

        if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {

            $images = CUploadedFile::getInstancesByName('images');

            $package = $form['package']->model;

            $seo = $form['seo']->model;
            if ($seo->save()) {
                $package->seo_id = $seo->id;
                if ($package->save(false))
                    if (isset($images) && count($images) > 0) {
                        $this->_uploadImages($images, $package);
                    }

                if ($form->submitted('goBack'))
                    $this->redirect(_aUrl('content/editPackage', array('id' => $package->id)));
                else
                    $this->redirect(_aUrl('content/packages'));
            }
        }

        $this->render('add', array('form' => $form, 'gMap' => false, 'multiline' => false, 'photo' => array('photos' => $model->packagePhotos)));
    }

    /**
     * The form for product property management
     * @param Property $model 
     */
    private function _propertyForm(Property $model) {
        $form = new CForm('application.modules.admin.views.content.propertyForm');
        $form['property']->model = $model;

        if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {

            $property = $form['property']->model;

            if ($property->save(false)) {
                if ($form->submitted('goBack'))
                    $this->redirect(_aUrl('content/editProperty', array('id' => $property->id)));
                else
                    $this->redirect(_aUrl('content/properties'));
            }
        }

        $this->render('add', array('form' => $form, 'gMap' => false, 'multiline' => false, 'photo' => false));
    }

    /**
     * The form for static content management
     * @param StaticContent $model 
     */
    private function _staticContentForm(StaticContent $model) {
        $form = new CForm('application.modules.admin.views.content.staticContentForm');
        $form['static']->model = $model;
        $form['seo']->model = $model->seo ? : new Seo;

        if (($form->submitted('save') || $form->submitted('goBack')) && $form->validate()) {

            $static = $form['static']->model;

            $seo = $form['seo']->model;
            if ($seo->save()) {
                $static->seo_id = $seo->id;
                if ($static->save(false)) {
                    if ($form->submitted('goBack'))
                        $this->redirect(_aUrl('content/editStaticContent', array('id' => $static->id)));
                    else
                        $this->redirect(_aUrl('content/staticContent'));
                }
            }
        }

        $this->render('add', array('form' => $form, 'gMap' => false, 'multiline' => false, 'photo' => false));
    }

    /*     * *******HELPER FUNCTIONS************* */

    /**
     * Upload images and save them to the model.
     * All models are assumed to have a corresponding <model>Photo model.
     * 
     * @param array $images An array of CUploadedImages objects
     * @param mixed $model The model that needs the photos
     * @return boolean Successful image saving
     */
    private function _uploadImages($images, $model) {
        if ($model) {
            //Get photo model
            $photoModel = get_class($model) . 'Photo';
            $parent_id = strtolower(get_class($model)) . '_id';
        }
        foreach ($images as $img) {
            $photoDir = Utils::photoUploadUrl($img->name);
            if ($img->saveAs($photoDir)) {
                //Resize image proportionally to 800px width
                Utils::ImageResize($photoDir, 800, false, true, $photoDir);
                $locationPhoto = new $photoModel;
                $locationPhoto->$parent_id = $model->id;
                $locationPhoto->photo = $img->name;
                $locationPhoto->save();
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Return a google map object using EGMap extension
     * 
     * @param string $map The comma-separated longitude, lattitude
     * @return EGMap A google map object to be rendered
     */
    private function _getGoogleMap($map = false) {
        Yii::import('ext.EGMap.*');
        $gMap = new EGMap();
        $gMap->setHtmlOptions(array('class' => 'gmaps'));
        $gMap->setWidth(300);
        $gMap->setHeight(300);
        $gMap->zoom = 6;
        $mapTypeControlOptions = array(
            'position' => EGMapControlPosition::RIGHT_TOP,
            'style' => EGMap::MAPTYPECONTROL_STYLE_HORIZONTAL_BAR
        );

        $gMap->mapTypeId = EGMap::TYPE_ROADMAP;
        $gMap->mapTypeControlOptions = $mapTypeControlOptions;

// Preparing InfoWindow with information about our marker.
        $info_window_a = new EGMapInfoWindow("<div class='gmaps-label' style='color: #000;'></div>");

// Setting up an icon for marker.
        $icon = new EGMapMarkerImage("http://google-maps-icons.googlecode.com/files/info.png");

        $icon->setSize(32, 37);
        $icon->setAnchor(16, 16.5);
        $icon->setOrigin(0, 0);

// Saving coordinates after user dragged our marker.
        $dragevent = new EGMapEvent('dragend', "function (event) { 
                                            $('#Activity_long_lat').val(event.latLng.lat()+','+event.latLng.lng());
                                        }", false, EGMapEvent::TYPE_EVENT_DEFAULT);

// If we have already created marker - show it
        if ($map) {
            list($long, $lat) = explode(',', $map);

            $marker = new EGMapMarker($lat, $long, array('title' => 'Activity',
                        'icon' => $icon, 'draggable' => true), 'marker', array('dragevent' => $dragevent));
            $marker->addHtmlInfoWindow($info_window_a);
            $gMap->addMarker($marker);
            $gMap->setCenter($lat, $long);
            $gMap->zoom = 16;

// If we don't have marker in database - make sure user can create one
        } else {
            $gMap->setCenter(37.9756, 23.7345);

            // Setting up new event for user click on map, so marker will be created on place and respectful event added.
            $gMap->addEvent(new EGMapEvent('click',
                            'function (event) {var marker = new google.maps.Marker({position: event.latLng, map: ' . $gMap->getJsName() .
                            ', draggable: true, icon: ' . $icon->toJs() . '}); ' . $gMap->getJsName() .
                            '.setCenter(event.latLng); var dragevent = ' . $dragevent->toJs('marker') .
                            "; $('#Activity_long_lat').val(event.latLng.lat()+','+event.latLng.lng());
                                }", false, EGMapEvent::TYPE_EVENT_DEFAULT_ONCE));
        }

        return $gMap;
    }

}