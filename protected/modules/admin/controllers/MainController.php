<?php

/**
 *The main controller for index actions and login/logout. 
 */
class MainController extends AdminController {
    
    public function filters() {
        return array('accessControl',);
    }
    
    public function accessRules() {
        return array(
            array('allow',
                'users' => array('?'),
                'actions' => array('error', 'login'),
            ),
            array(
                'allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    public function actionIndex() {
        if(_user()->isGuest) {
            $this->redirect(_aUrl('main/login'));
		}
        else {
            $this->render('index');
		}
    }

    public function actionLogin() {
        $this->layout = 'login';
        $this->pageTitle = 'E-Tourism Platform | Login';

        $model = new LoginForm;

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login()) {
                //$this->redirect(AdminUtils::aUrl('index'));
				$this->redirect( array('main/index'));
				
			}
        }
        $this->render('login', array('model' => $model));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        //$this->redirect(AdminUtils::aUrl('index'));
		$this->redirect(array('index'));
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}