<?php

/**
 * The main controller for user management
 */
class UserController extends AdminController {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array(
                'allow',
                'expression' => '_user()->role == Role::ADMIN',
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    public function actionIndex() {
        $users = new User('search');
        
        if (isset($_GET['User']))
            $users->attributes = $_GET['User'];
        
        $this->render('index', array('users' => $users));
    }
    
    public function actionAdd() {
        $this->_userForm(new User);
    }
    
    public function actionUpdate($id) {
        $this->_userForm(User::model()->findByPk($id));
    }
    
    public function actionDelete($id) {
        User::model()->deleteByPk($id);
        
        $this->redirect(_aUrl('user/index'));
    }
    
    //The form for the user form
    private function _userForm(User $model) {
        $form = new CForm('application.modules.admin.views.user.userForm');
        $form['user']->model = $model;

        if ($form->submitted('save') && $form->validate()) {

            $user = $form['user']->model;

            if ($user->save(false))
                $this->redirect(_aUrl('user/index'));
        } else {
//            CVarDumper::dump($form['user']->model->getErrors()); die();
        }

        $this->render('add', array('form' => $form));
    }

}