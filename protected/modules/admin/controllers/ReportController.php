<?php

/**
 * The controller for all reports in admin 
 */
class ReportController extends AdminController {

    public function filters() {
        return array('accessControl',);
    }

    public function accessRules() {
        return array(
            array('deny',
                'users' => array('?'), // Deny guests
            ),
            array(
                'allow',
                'users' => array('@'),
            ),
            array('deny',
                'users' => array('*')
            )
        );
    }

    /**
     * All sales by supplier 
     */
    public function actionSupplier() {
        $orders = new OrderProduct('search');
        $suppliers = Supplier::model()->findAll(array('order' => 'name asc'));
        $date = new DateTime;
        $dateSub = new Datetime;
        $dateSub->sub(new DateInterval('P1D'));

        $orders->supplier_id = count($suppliers) > 0 ? $suppliers[0]->id : '';
        $orders->start_date = $dateSub->format('Y-m-d');
        $orders->start_date = $date->format('Y-m-d');
        $search = array('supplier_id' => $suppliers ? $suppliers[0]->id : '', 'start_date' => $dateSub->format('Y-m-d'), 'end_date' => $date->format('Y-m-d'));

        if (isset($_POST['OrderProduct'])) {
            $search = $_POST['OrderProduct'];
            $orders->attributes = $_POST['OrderProduct'];
        }

        $this->render('supplier', array('orders' => $orders, 'search' => $search, 'suppliers' => $suppliers));
    }

    /**
     * All sales by category 
     */
    public function actionCategory() {
        $orders = new OrderProduct('search');
        $categories = Category::model()->findAll(array('order' => 'name asc'));
        $date = new DateTime;
        $dateSub = new Datetime;
        $dateSub->sub(new DateInterval('P1D'));

        $orders->category_id = count($categories) > 0 ? $categories[0]->id : '';
        $orders->start_date = $dateSub->format('Y-m-d');
        $orders->start_date = $date->format('Y-m-d');
        $search = array('category_id' => $categories ? $categories[0]->id : '', 'start_date' => $dateSub->format('Y-m-d'), 'end_date' => $date->format('Y-m-d'));

        if (isset($_POST['OrderProduct'])) {
            $search = $_POST['OrderProduct'];
            $orders->attributes = $_POST['OrderProduct'];
        }

        $this->render('category', array('orders' => $orders, 'search' => $search, 'categories' => $categories));
    }

    /**
     * All sales by type of item 
     */
    public function actionType() {
        $date = new DateTime;
        $dateSub = new Datetime;
        $dateSub->sub(new DateInterval('P1D'));

        $search = array('type' => 'activity', 'start_date' => $dateSub->format('Y-m-d'), 'end_date' => $date->format('Y-m-d'));

        $orders = new OrderActivity('search');
        $orders->searchType = ActivityType::ACTIVITY;

        $orders->start_date = $dateSub->format('Y-m-d');
        $orders->end_date = $date->format('Y-m-d');

        if (isset($_POST['Order']['type'])) {
            switch ($_POST['Order']['type']) {
                case 'product' :
                    $orders = new OrderProduct('search');
                    break;
                case 'package' :
                    $orders = new OrderPackage('search');
                    break;
                case 'event':
                    $orders->searchType = ActivityType::EVENT;
                    break;
                case 'attraction':
                    $orders->searchType = ActivityType::ATTRACTION;
                    break;
                default :
                    break;
            }
            $search = $_POST['Order'];
            $orders->attributes = $_POST['Order'];
        }

        $this->render('type', array(
            'orders' => $orders,
            'search' => $search,
            'searchType' => $search['type'] == 'event' || $search['type'] == 'attraction' ? 'activity' : $search['type']
        ));
    }

    /**
     * All products that have low stock 
     */
    public function actionLowStock() {
        $threshold = 5;
        if (isset($_POST['threshold']))
            $threshold = $_POST['threshold'];

        $products = new Product('searchStock');
        $products->threshold = $threshold;

        $this->render('lowStock', array('products' => $products, 'threshold' => $threshold));
    }

    /**
     * A chart with total sales 
     */
    public function actionTotal() {
        $date = new DateTime;
        if (isset($_POST['date']))
            $date = new DateTime($_POST['date']);
        
        $db = _app()->db;
        $c = $db->createCommand(
                <<<SQL
        SELECT sum( activities.amount * activities.price ) AS act, sum( events.amount * events.price ) AS evnt, sum( attractions.amount * attractions.price ) AS att, 
            sum( op.amount * p.price ) AS pro, sum( ok.amount * k.price ) AS pak, DATE( date_entered ) AS date
        FROM `order` o
        LEFT JOIN ( SELECT * FROM order_activity oa LEFT JOIN activity a ON oa.activity_id = a.id WHERE a.activity_type = :act) AS activities ON o.id = activities.order_id
        LEFT JOIN ( SELECT * FROM order_activity oa LEFT JOIN activity a ON oa.activity_id = a.id WHERE a.activity_type = :evnt) AS events ON o.id = events.order_id
        LEFT JOIN ( SELECT * FROM order_activity oa LEFT JOIN activity a ON oa.activity_id = a.id WHERE a.activity_type = :att) AS attractions ON o.id = attractions.order_id
        LEFT JOIN ( order_product op LEFT JOIN product p ON op.product_id = p.id ) ON o.id = op.order_id
        LEFT JOIN ( order_package ok LEFT JOIN package k ON ok.package_id = k.id ) ON o.id = ok.order_id
        WHERE STATUS = :status
        AND date_entered BETWEEN :startDate AND :endDate
        GROUP BY DATE( date_entered ) ASC
SQL
        );
        $c->bindValue(':status', OrderStatus::SENT_TO_CUSTOMER);
        $c->bindValue(':startDate', $date->sub(new DateInterval('P45D'))->format('Y-m-d'));
        $c->bindValue(':endDate', $date->add(new DateInterval('P90D'))->format('Y-m-d'));
        $c->bindValue(':act', ActivityType::ACTIVITY);
        $c->bindValue(':evnt', ActivityType::EVENT);
        $c->bindValue(':att', ActivityType::ATTRACTION);
        $data = $c->queryAll();

        $i = 0;
        $totals = array();

        //Prepare data from query for highcharts object
        foreach ($data as $d) {
            $totals['date'][$i] = strftime('%e %b', strtotime($d['date']));
            $totals['act'][$i] = (float) $d['act'];
            $totals['evnt'][$i] = (float) $d['evnt'];
            $totals['att'][$i] = (float) $d['att'];
            $totals['pro'][$i] = (float) $d['pro'];
            $totals['pak'][$i] = (float) $d['pak'];
            $totals['total'][$i] = (float) ($d['pak'] + $d['pro'] + $d['act'] + $d['evnt'] + $d['att']);

            ++$i;
        }

        $this->render('total', array('totals' => $totals, 'date' => $date->sub(new DateInterval('P45D'))->format('Y/m/d')));
    }

}