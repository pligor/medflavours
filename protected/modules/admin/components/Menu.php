<?php

/**
 * Specificaitons:
 *
 * Must support:
 * label (optional [value callback]) default=none
 * url (optional) default=none
 * realUrl (optional) default=_url(this->url)
 * image (optional) default=none
 * visible (optional condition [boolean|expression]) default=true
 * enabled (optional condition [boolean|expression]) default=true
 * active (optional condition [boolean|expression]) NOTE: maybe this must be automatic. default=false. if active then force visible true
 * showItems (optional condition [boolean|expression]) default=true
 * subitems
 */
class Menu extends CWidget {

    /**
     *
     */
    public function init() {
        
    }

    /**
     *
     */
    public function run() {
        $this->render('menu');
    }

}
?>