<?php
/**
 * Description of AdminForm
 *
 * @author pligor
 */
class AdminForm extends CForm {
	//public $id = null;
	
	public function __construct($config, $model=null, $parent=null) {
		$this->buttonElementClass = 'SaveFormButtonElement';
		parent::__construct($config, $model, $parent);
	}
	
	public function init() {
		parent::init();
	}
	
	public function renderButtons() {
		$output='';
		$output .= '<div class="tableBottom formSubmit">';
		foreach($this->getButtons() as $button) {
			//print get_class($button);
			$output.=$this->renderElement($button);
		}
		$output .= '<div class="ClearFloat"></div></div>';
		
		return $output!=='' ? "<div class=\"row buttons\">".$output."</div>\n" : '';
	}
}