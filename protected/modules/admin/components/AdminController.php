<?php

/**
 * This is the controller class for the admin module.
 * It extends the Controller class from the components folder which in turn
 * extends the CController class.
 * 
 * This file defines all options for any controllers in the admin module
 * which must extend from this.
 */
class AdminController extends Controller {

    /**
     * The layout for all admin module
     * @var string
     */
    public $layout = 'admin';

    public function init() {
        parent::init();

        //Override the error handler for admin views
        _app()->errorHandler->errorAction = 'admin/main/error';
    }

}

?>
