<ul class="glossymenu">
    <li><a class="menuitem" href="#"><em class="dashboard"></em>Dashboard</a></li>
	<li><a href="<?php print _aUrl('content/navigation'); ?>">Navigation (debug only purposes)</a></li>
    <li><a class="menuitem submenuheader" href="#" ><em class="content"></em>Content</a></li>
    <li class="submenu">
        <ul>
			<!-- <li><a href="<?php echo _aUrl('content/testing'); ?>">Testing</a></li> -->
			
			<li><a href="<?php echo _aUrl('content/categories'); ?>">Product Categories</a></li>
			<li><a href="<?php echo _aUrl('content/ActCategory'); ?>">Activity Categories</a></li>
			<li><a href="<?php echo _aUrl('content/EventCategory'); ?>">Event Categories</a></li>
			<li><a href="<?php echo _aUrl('content/AttractionCategory'); ?>">Attraction Categories</a></li>
			
			<li><a href="<?php print _aUrl('content/cat_loc_content'); ?>">Category Location Content</a></li>
			
			<li><a href="<?php echo _aUrl('content/locations'); ?>">Locations</a></li>
			
            <li><a href="<?php echo _aUrl('content/suppliers'); ?>">Suppliers</a></li>
            
			<li><a href="<?php echo _aUrl('content/properties'); ?>">Product Properties</a></li>
			
			<li><a href="<?php echo _aUrl('content/products'); ?>">Products</a></li>
            
            <li><a href="<?php echo _aUrl('content/activities'); ?>">Activities</a></li>
            <li><a href="<?php echo _aUrl('content/events'); ?>">Events</a></li>
            <li><a href="<?php echo _aUrl('content/attractions'); ?>">Attractions</a></li>
            <li><a href="<?php echo _aUrl('content/packages'); ?>">Packages</a></li>
            <li><a href="<?php echo _aUrl('content/staticContent'); ?>">Static Content</a></li>
        </ul>
    </li>
    <li><a class="menuitem submenuheader" href="#"><em class="orders"></em>Orders</a></li>
    <li class="submenu">
        <ul>
            <li><a href="<?php echo _aUrl('order/index'); ?>">Orders</a></li>
            <li><a href="<?php echo _aUrl('order/customers'); ?>">Customers</a></li>
        </ul>
    </li>
    <li><a class="menuitem submenuheader" href="#"><em class="reports"></em>Reports</a></li>
    <li class="submenu">
        <ul>
            <li><a href="<?php echo _aUrl('report/supplier'); ?>">Sales by Supplier</a></li>
            <li><a href="<?php echo _aUrl('report/category'); ?>">Sales by Category</a></li>
            <li><a href="<?php echo _aUrl('report/type'); ?>">Sales by Type</a></li>
          <!--  <li><a href="<?php echo _aUrl('report/total'); ?>">Total Sales</a></li> -->
          <!--  <li><a href="<?php echo _aUrl('report/lowStock'); ?>">Low Stocks</a></li> -->
        </ul>
    </li>
    <?php if (_user()->role == Role::ADMIN) : ?>
        <li><a class="menuitem" href="<?php echo _aUrl('user/index'); ?>"><em class="users"></em>Users</a></li>
    <?php endif; ?>
</ul>