<?php

/**
 * This is the model class for table "activity_period".
 *
 * The followings are the available columns in table 'activity_period':
 * @property integer $id
 * @property integer $activity_id
 * @property string $from
 * @property string $to
 * @property double $price
 *
 * The followings are the available model relations:
 * @property Activity $activity
 */
class ActivityPeriod extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ActivityPeriod the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'activity_period';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('activity_id', 'required'),
            array('activity_id', 'numerical', 'integerOnly' => true),
            array('price', 'numerical'),
            array('from, to', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, activity_id, from, to, price', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'activity' => array(self::BELONGS_TO, 'Activity', 'activity_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'activity_id' => 'Activity',
            'from' => 'From',
            'to' => 'To',
            'price' => 'Price',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('activity_id', $this->activity_id);
        $criteria->compare('from', $this->from, true);
        $criteria->compare('to', $this->to, true);
        $criteria->compare('price', $this->price);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    protected function beforeSave() {
        if (parent::beforeSave()) {
            if ($this->from && $this->to) {
                $from = new DateTime($this->from);
                $this->from = $from->format(DATE_ISO8601);
                $to = new DateTime($this->to);
                $this->to = $to->format(DATE_ISO8601);
            }
            return true;
        }
        return false;
    }
	
	    /**
     * @return \CForm 
     */
    public function getForm($formClass='CForm') {
        $config = $this->getFormConfig();
        return new $formClass($config, $this);    //all subforms get the model from their parent
    }
    
    /**
     * @return array with the configuration of this form 
     */
    public function getFormConfig() {
        return array(
            //'title' => '',
            'showErrorSummary' => true,
            //'elements' => array(get_class() => $this->getFormElements(),),    //if you want subforms
            'elements' => $this->getFormElements(),
            'buttons' => array(
                'submit_'.strtolower(get_class()) => array(
                    'type' => 'submit',
                    'label' => Yii::t('', 'Submit '.get_class()),
                ),
            ),/*
            'attributes' => array(
                'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
            ),*/
            'activeForm' => array(
                'class' => 'CActiveForm',
            ),
        );
    }
    /**
     * @return array the elements of this form
     */
    public function getFormElements() {
        return array(
                /*'activity_id' => array(
                    'type' => 'hidden',
                    //'hint' => '',
                    'attributes' => array(
                    ),
                ),*/
                'from' => array(
                    'type' => 'zii.widgets.jui.CJuiDatePicker',
                    'hint' => 'date format: yyyy/mm/dd',
                    'attributes' => array(
						'id' => Hlp::veryRandom(),
						//'class' => 'short-text',
						'options' => array(
							'showAnim' => 'fold',
							'altFormat' => 'dd-mm-yy',
							'dateFormat' => 'yy/mm/dd',
							//'beforeShow' => 'js:function(input, inst){inst.dpDiv.css({marginTop: -input.offsetHeight + "px", marginLeft: input.offsetWidth + "px"});}'
						),
						
                    ),
                ),
                'to' => array(
                    'type' => 'zii.widgets.jui.CJuiDatePicker',
                    'hint' => 'date format: yyyy/mm/dd',
                    'attributes' => array(
						'id' => Hlp::veryRandom(),
						//'class' => 'short-text',
						'options' => array(
							'showAnim' => 'fold',
							'altFormat' => 'dd-mm-yy',
							'dateFormat' => 'yy/mm/dd',
							//'beforeShow' => 'js:function(input, inst){inst.dpDiv.css({marginTop: -input.offsetHeight + "px", marginLeft: input.offsetWidth + "px"});}'
						),
                    ),
                ),
                /*'price' => array(
                    'type' => 'decimal(11,2)',
                    //'hint' => '',
                    'attributes' => array(
                    ),
                ),*/
        );
	}
}