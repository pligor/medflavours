<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property integer $category_id
 * @property integer $location_id
 * @property integer $supplier_id
 * @property string $name
 * @property integer $is_active
 * @property integer $featured
 * @property string $supplier_extra_info
 *
 * The followings are the available model relations:
 * @property Order[] $orders
 * @property Category $category
 * @property Location $location
 * @property Supplier $supplier
 * @property ProductMeta $productMeta
 * @property ProductPhoto[] $productPhotos
 * @property ProductProperty[] $productProperties
 */
class Product extends ActiveRecord {

    public $threshold = 5;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Product the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{product}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(			
            array('category_id, location_id, supplier_id, name', 'required'),
            array('category_id, location_id, supplier_id, is_active, featured', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 45),
			array('brand_name, supplier_extra_info','safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, category_id, location_id, supplier_id, name, is_active, featured, brand_name, supplier_extra_info', 'safe', 'on' => 'search'),
            array('id, category_id, location_id, supplier_id, name, is_active, featured, brand_name, supplier_extra_info, threshold', 'safe', 'on'=>'searchStock'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
			'packs' => array(self::HAS_MANY, 'Pack', 'product_id'),
			
            'orders' => array(self::MANY_MANY, 'Order', 'order_product(product_id, order_id)'),
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
            'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplier_id'),
            'productMeta' => array(self::HAS_ONE, 'ProductMeta', 'product_id'),
            'productPhotos' => array(self::HAS_MANY, 'ProductPhoto', 'product_id'),
            'productProperties' => array(self::HAS_MANY, 'ProductProperty', 'product_id'),
            'seo' => array(self::BELONGS_TO, 'Seo', 'seo_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'category_id' => 'Category',
            'location_id' => 'Location',
            'supplier_id' => 'Supplier',
            'name' => 'Name',
            'is_active' => 'Is Active',
			'featured' => 'Featured',
			'brand_name' => 'Brand Name',
			'supplier_extra_info' => 'Supplier extra info for this flavour',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->with = array('category', 'location', 'supplier');

        $criteria->compare('category.id', $this->category_id);
        $criteria->compare('location.id', $this->location_id);
        $criteria->compare('supplier.id', $this->supplier_id);
        $criteria->compare('name', $this->name, true);

        $sort = new CSort;
        $sort->attributes = array(
            'name',
        );

        $sort->defaultOrder = array(
            'name' => true,
        );

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 50
                    ),
                    'sort' => $sort
                ));
    }

    public function searchStock() {
        $criteria = new CDbCriteria;

        $criteria->condition = 'stock BETWEEN 0 AND :threshold';
        $criteria->params = array(':threshold' => $this->threshold);

        $sort = new CSort;
        $sort->attributes = array(
//            'stock',
        );

        $sort->defaultOrder = 'stock asc';

        return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 50
			),
			'sort' => $sort
		));
    }

}