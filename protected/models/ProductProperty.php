<?php

/**
 * This is the model class for table "product_property".
 *
 * The followings are the available columns in table 'product_property':
 * @property integer $product_id
 * @property integer $property_id
 * @property string $value
 *
 * The followings are the available model relations:
 * @property Product $product
 * @property Property $property
 */
class ProductProperty extends ActiveRecord {

	protected $_id = null;
	
	public function getId() {
		if($this->_id === null) {
			if(!$this->isNewRecord) {
				$this->_id = Model::yii2str_id( $this->getPrimaryKey() );
			}
		}
		return $this->_id;
	}
	
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ProductProperty the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'product_property';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('product_id, property_id, value', 'required'),
            array('product_id, property_id', 'numerical', 'integerOnly' => true),
            array('value', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('product_id, property_id, value', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
            'property' => array(self::BELONGS_TO, 'Property', 'property_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'product_id' => 'Product',
            'property_id' => 'Property',
            'value' => 'Value',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('product_id', $this->product_id);
        $criteria->compare('property_id', $this->property_id);
        $criteria->compare('value', $this->value, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
	
	/**
	 * @return \CForm
	 */
	public function getForm($formClass='CForm') {
		$config = $this->getFormConfig();
		return new $formClass($config, $this);	//all subforms get their model the model of the parent
	}
	
	public function getFormConfig() {
		$modelName = get_class();
		return array(
		    //'title' => '..............',
		    'showErrorSummary' => true,
		    /*'elements' => array(
				get_class() => $this->getFormElements(),
			),*/
			'elements' => $this->getFormElements(),
		    'buttons' => array(
		        'submit_'.lcfirst($modelName) => array(
					'type' => 'submit',
					'label' => 'Submit '.$modelName,
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormElements() {
		return array(
			'property_id' => array(
				'type' => 'dropdownlist',
				'items' => CHtml::listData(Property::model()->findAll(), 'id', 'name'),
				//'hint' => $this->hint,
				'attributes' => array(
					//'id' => md5(microtime()*rand()),
				),
	        ),
	        'value' => array(
				'type' => 'text',
	        ),
			/*'product_id' => array(
				'type' => 'hidden',
			),*/
		);
	}
}