<?php

/**
 * This is the model class for table "category_location_content".
 * @author pligor
 * The followings are the available columns in table 'category_location_content':
 * @property integer $category_id
 * @property integer $location_id
 * @property string $content
 */
class CategoryLocationContent extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategoryLocationContent the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{category_location_content}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, location_id', 'required'),
			array('category_id, location_id', 'numerical', 'integerOnly'=>true),
			array('content', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('category_id, location_id, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'category_id' => t('Category'),
			'location_id' => t('Location'),
			'content' => t('Content'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * @return \CForm 
	 */
	public function getForm($formClass='CForm') {
		$config = $this->getFormConfig();
		return new $formClass($config, $this);	//all subforms get the model from their parent
	}
	
	/**
	 * @return array with the configuration of this form 
	 */
	public function getFormConfig() {
		return array(
		    //'title' => '',
		    'showErrorSummary' => true,
		    //'elements' => array(get_class() => $this->getFormElements(),),	//if you want subforms
			'elements' => $this->getFormElements(),
		    'buttons' => array(
		        'submit_'.strtolower(get_class()) => array(
					'type' => 'submit',
					'label' => Yii::t('', 'Save Content'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
			'activeForm' => array(
				'id' => 'cat_loc_content_form',
				'class' => 'CActiveForm',
			),
		);
	}
	/**
	 * @return array the elements of this form
	 */
	public function getFormElements() {
		return array(
				'category_id' => array(
					'type' => 'dropdownlist',
                    'items' => Category::getAllLeafs(),
					//'hint' => '',
					'attributes' => array(
						'onchange' => 'CombinationChanged()',
					),
				),
				'location_id' => array(
					'type' => 'dropdownlist',
                    'items' => Location::model()->getTree(),
					//'hint' => '',
					'attributes' => array(
						'onchange' => 'CombinationChanged()',
					),
				),
				'content' => array(
					'type' => 'external.cleditor.ECLEditor',
                    'attributes' => array(
                        'options' => array(
                            'width' => '500',
                            'height' => 250,
                            'useCSS' => true,
                            'controls' => _controls(),
                        ),
                    ),
				),
		);
	}
}
