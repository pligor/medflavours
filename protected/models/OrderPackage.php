<?php

/**
 * This is the model class for table "order_package".
 *
 * The followings are the available columns in table 'order_package':
 * @property integer $order_id
 * @property integer $package_id
 * @property integer $amount
 */
class OrderPackage extends CActiveRecord {

    public $total = 0;
    public $start_date;
    public $end_date;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OrderPackage the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'order_package';
    }

    public function rules() {
        return array(
            array('order_id, package_id', 'required'),
            array('order_id, package_id, amount', 'numerical', 'integerOnly' => true),
            array('amount', 'default', 'setOnEmpty' => true, 'value' => null),
            array('order_id, package_id, amount, start_date, end_date', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
            'package' => array(self::BELONGS_TO, 'Package', 'package_id'),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->with[] = 'order';
        $criteria->compare('order.status', OrderStatus::SENT_TO_CUSTOMER);

        if ($this->start_date && $this->end_date) {
            $criteria->addCondition('order.date_entered between :startDate and :endDate');
            $criteria->params[':startDate'] = $this->start_date;
            $criteria->params[':endDate'] = $this->end_date;
        }

        $sort = new CSort;
        $sort->attributes = array(
            'order.date_entered',
        );

        $sort->defaultOrder = array(
            'order.date_entered' => true,
        );

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 20
                    ),
                    'sort' => $sort
                ));
    }

    protected function afterFind() {
        $this->total += $this->package->price * $this->amount;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'order_id' => 'Order',
            'package_id' => 'Package',
            'amount' => 'Amount',
        );
    }

}