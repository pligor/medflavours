<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $parent_id
 *
 * The followings are the available model relations:
 * @property Category $parent
 * @property Category[] $categories
 * @property Product[] $products
 */
class Category extends ActiveRecord {

    private $_categories = array();
	
	public function behaviors() {
		return array_merge(parent::behaviors(),array(
			'tree' => array(
				'class' => 'beh.TreeBehavior',
				//'property1'=>'value1',
			),
		));
	}
	
	public function getAllRelated() {
		$relation = 'locations';
		$array1 = $this->getIndexedHasMany($relation);
		$array2 = $this->subchildrenRelations($relation);
		return $array1 + $array2;
	}
	
	public static function getAllLeafs() {
		return CHtml::listData(
			self::model()->findAll('NOT EXISTS (SELECT NULL FROM category WHERE parent_id = t.id)'),
			'id',
			'name'
		);
	}
	
	/**
	 * dummy function to solve kostas not usage of generality 
	 */
	public function getChildren() {
		return $this->subcategories;
	}

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Category the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{category}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('parent_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 45),
            array('description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, description, parent_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
			'products' => array(self::HAS_MANY, 'Product', 'category_id'),
			'locations' => array(self::HAS_MANY, 'Location', array('location_id'=>'id'),'through'=>'products'),
			
            'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
            'subcategories' => array(self::HAS_MANY, 'Category', 'parent_id'),	//kanonika auto to onomazoume children for generality :P
			
            'seo' => array(self::BELONGS_TO, 'Seo', 'seo_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Category'),
            'description' => Yii::t('app', 'Description'),
            'parent_id' => null,
            'parent' => null,
            'categories' => null,
            'products' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        if (!$this->parent_id)
            $criteria->condition = 'parent_id is null';
        else
            $criteria->compare('parent_id', $this->parent_id);

        $sort = new CSort;
        $sort->attributes = array(
            'name',
            'description'
        );

        $sort->defaultOrder = array(
            'name' => true,
        );

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 50
                    ),
                    'sort' => $sort
                ));
    }

    public function getTree() {
        //TODO: Improve this shit

        $criteria = new CDbCriteria;
        $criteria->condition = 'parent_id is null';
        $parents = $this->findAll($criteria);

        foreach ($parents as $parent) {
            $this->_categories[$parent->id] = $parent->name;
            $this->_genealogy($parent->id, 0);
        }

        return $this->_categories;
    }

    private function _genealogy($categorie, $level = 0) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'parent_id = ' . $categorie;
        $parents = $this->findAll($criteria);

        ++$level;

        foreach ($parents as $parent) {
            $this->_categories[$parent->id] = str_repeat('-', $level) . ' ' . $parent->name;
            $this->_genealogy($parent->id, $level, $this->_categories);
        }
    }

}