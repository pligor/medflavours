<?php
/**
 * Description of AddonUnit
 *
 * @author pligor
 */
class AddonUnit {
	//put your code here
	const PERSON = 1;
	const DAY = 2;
	const HOUR = 3;
	const ITEM = 4;
	const FIXED = 5;
	
	public static function listData() {
		return array(
			self::PERSON => 'person',
			self::DAY => 'day',
			self::HOUR => 'hour',
			self::ITEM => 'item',
			self::FIXED => 'FIXED',
		);
	}
}