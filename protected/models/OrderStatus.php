<?php
/**
 * Description of OrderStatus
 *
 * @author subdee
 */
class OrderStatus {
    const OPEN = 1;
    const SENT_TO_SUPPLIER = 2;
    const SENT_TO_WAREHOUSE = 3;
    const ARRIVED_AT_WAREHOUSE = 4;
    const SENT_TO_CUSTOMER = 5;
    
    public static function next($status) {
        switch ($status){
            case self::OPEN:
                return self::SENT_TO_SUPPLIER;
                break;
            case self::SENT_TO_SUPPLIER:
                return self::SENT_TO_WAREHOUSE;
                break;
            case self::SENT_TO_WAREHOUSE:
                return self::ARRIVED_AT_WAREHOUSE;
                break;
            case self::ARRIVED_AT_WAREHOUSE:
                return self::SENT_TO_CUSTOMER;
                break;
        }
    }
    
    public static function previous($status) {
        switch ($status){
            case self::SENT_TO_CUSTOMER:
                return self::ARRIVED_AT_WAREHOUSE;
                break;
            case self::ARRIVED_AT_WAREHOUSE:
                return self::SENT_TO_WAREHOUSE;
                break;
            case self::SENT_TO_WAREHOUSE:
                return self::SENT_TO_SUPPLIER;
                break;
            case self::SENT_TO_SUPPLIER:
                return self::OPEN;
                break;
        }
    }
}

?>
