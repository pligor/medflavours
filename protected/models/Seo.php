<?php

/**
 * This is the model class for table "seo".
 *
 * The followings are the available columns in table 'seo':
 * @property integer $id
 * @property string $url
 * @property string $title
 * @property string $description
 * @property string $keywords
 *
 * The followings are the available model relations:
 * @property Activity[] $activities
 * @property Category[] $categories
 * @property Location[] $locations
 * @property Package[] $packages
 * @property Product[] $products
 * @property Supplier[] $suppliers
 */
class Seo extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Seo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'seo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//            array('url, title, description, keywords', 'required'),
            array('url', 'length', 'max' => 40),
            array('title', 'length', 'max' => 100),
            array('url, title, description, keywords', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, url, title, description, keywords', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'activities' => array(self::HAS_MANY, 'Activity', 'seo_id'),
            'categories' => array(self::HAS_MANY, 'Category', 'seo_id'),
            'locations' => array(self::HAS_MANY, 'Location', 'seo_id'),
            'packages' => array(self::HAS_MANY, 'Package', 'seo_id'),
            'products' => array(self::HAS_MANY, 'Product', 'seo_id'),
            'suppliers' => array(self::HAS_MANY, 'Supplier', 'seo_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'url' => 'Url',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('keywords', $this->keywords, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

	/**
     * @return \CForm 
     */
    public function getForm($formClass='CForm') {
        $config = $this->getFormConfig();
        return new $formClass($config, $this);    //all subforms get the model from their parent
    }
    
    /**
     * @return array with the configuration of this form 
     */
    public function getFormConfig() {
        return array(
            //'title' => '',
            'showErrorSummary' => true,
            //'elements' => array(get_class() => $this->getFormElements(),),    //if you want subforms
            'elements' => $this->getFormElements(),
            'buttons' => array(
                'submit_'.strtolower(get_class()) => array(
                    'type' => 'submit',
                    'label' => Yii::t('', 'Submit '.get_class()),
                ),
            ),/*
            'attributes' => array(
                'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
            ),*/
            'activeForm' => array(
                'class' => 'CActiveForm',
            ),
        );
    }
	
    /**
     * @return array the elements of this form
     */
    public function getFormElements() {
        return array(
			'url' => array(
				'type' => 'text',
			),
			'title' => array(
				'type' => 'text',
			),
			'description' => array(
				'type' => 'text',
			),
			'keywords' => array(
				'type' => 'text',
			),
		);
    }
}