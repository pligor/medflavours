<?php

/**
 * This is the model class for table "static_content".
 *
 * The followings are the available columns in table 'static_content':
 * @property integer $id
 * @property string $content
 * @property string $menu_title
 * @property integer $show_in_menu
 * @property integer $seo_id
 *
 * The followings are the available model relations:
 * @property Seo $seo
 */
class StaticContent extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return StaticContent the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'static_content';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('content', 'required'),
            array('show_in_menu, seo_id', 'numerical', 'integerOnly' => true),
            array('menu_title', 'length', 'max' => 20),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, content, menu_title, show_in_menu, seo_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'seo' => array(self::BELONGS_TO, 'Seo', 'seo_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'content' => 'Content',
            'menu_title' => 'Menu Title',
            'show_in_menu' => 'Show In Menu',
            'seo_id' => 'Seo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
//
//        $criteria->compare('id', $this->id);
//        $criteria->compare('content', $this->content, true);
//        $criteria->compare('menu_title', $this->menu_title, true);
//        $criteria->compare('show_in_menu', $this->show_in_menu);
//        $criteria->compare('seo_id', $this->seo_id);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}