<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property integer $customer_id
 * @property integer $status
 * @property string $date_entered
 *
 * The followings are the available model relations:
 * @property Customer $customer
 * @property Activity[] $activities
 * @property Package[] $packages
 * @property Product[] $products
 */
class Order extends CActiveRecord {

    public $total = 0;
    public $customer_fullname;
    public $customer_email;
    public $type;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'order';
    }

    public function rules() {
        return array(
            array('id, customer_id, status, date_entered', 'required'),
            array('id, customer_id, status', 'numerical', 'integerOnly' => true),
            array('id, customer_id, status, date_entered, customer_fullname, customer_email', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(			
			'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
//            'orderActivities' => array(self::MANY_MANY, 'Activity', 'order_activity(order_id, activity_id)'),
//            'packages' => array(self::MANY_MANY, 'Package', 'order_package(order_id, package_id)'),
            'orderProducts' => array(self::MANY_MANY, 'Product', 'order_product(order_id, product_id)'),
            'activities' => array(self::HAS_MANY, 'OrderActivity', 'order_id'),
            'products' => array(self::HAS_MANY, 'OrderProduct', 'order_id'),
            'packages' => array(self::HAS_MANY, 'OrderPackage', 'order_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'customer_id' => 'Customer',
            'status' => 'Status',
            'date_entered' => 'Date Entered',
        );
    }

    protected function afterFind() {
        $this->_getTotals();
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->with = array('customer');
        $criteria->compare('customer.fullname', $this->customer_fullname, true);
        $criteria->compare('customer.email', $this->customer_email, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    private function _getTotals() {
        if (!$this->type || $this->type == 'activity') {
            foreach ($this->activities as $activity)
                $this->total += $activity->activity->price * $activity->amount;
        }
        if (!$this->type || $this->type == 'product') {
            foreach ($this->products as $product)
                $this->total += $product->product->price * $product->amount;
        }
        if (!$this->type || $this->type == 'package') {
            foreach ($this->packages as $package)
                $this->total += $package->package->price * $package->amount;
        }
    }

}