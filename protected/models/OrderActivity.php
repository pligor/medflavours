<?php

/**
 * This is the model class for table "order_activity".
 *
 * The followings are the available columns in table 'order_activity':
 * @property integer $order_id
 * @property integer $activity_id
 * @property integer $amount
 */
class OrderActivity extends CActiveRecord {

    public $total = 0;
    public $start_date;
    public $end_date;
    public $searchType;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OrderActivity the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'order_activity';
    }

    public function rules() {
        return array(
            array('order_id, activity_id', 'required'),
            array('order_id, activity_id, amount', 'numerical', 'integerOnly' => true),
            array('amount', 'default', 'setOnEmpty' => true, 'value' => null),
            array('order_id, activity_id, amount, start_date, end_date', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
            'activity' => array(self::BELONGS_TO, 'Activity', 'activity_id'),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->with[] = 'order';
        $criteria->with[] = 'activity';
        $criteria->compare('order.status', OrderStatus::SENT_TO_CUSTOMER);

        if ($this->start_date && $this->end_date) {
            $criteria->addCondition('order.date_entered between :startDate and :endDate');
            $criteria->params[':startDate'] = $this->start_date;
            $criteria->params[':endDate'] = $this->end_date;
        }

        $criteria->compare('activity.activity_type', $this->searchType);
        
        $sort = new CSort;
        $sort->attributes = array(
            'order.date_entered',
        );

        $sort->defaultOrder = array(
            'order.date_entered' => true,
        );

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 20
                    ),
                    'sort' => $sort
                ));
    }

    protected function afterFind() {
        $this->total += $this->activity->price * $this->amount;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'order_id' => 'Order',
            'activity_id' => 'Activity',
            'amount' => 'Amount',
        );
    }

}