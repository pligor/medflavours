<?php

/**
 * This is the model class for table "location".
 *
 * The followings are the available columns in table 'location':
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property string $description
 * @property integer $level
 *
 * The followings are the available model relations:
 * @property Activity[] $activities
 * @property Location $parent
 * @property Location[] $locations
 * @property LocationPhoto[] $locationPhotos
 * @property Product[] $products
 */
class Location extends ActiveRecord {
	private $_locations = array();
	
	public function behaviors() {
		return array_merge(parent::behaviors(),array(
			'tree' => array(
				'class' => 'beh.TreeBehavior',
				//'property1'=>'value1',
			),
		));
	}
	
	public function getAllRelated() {
		$relation = 'categories';
		$array1 = $this->getIndexedHasMany($relation);
		$array2 = $this->subchildrenRelations($relation);
		return $array1 + $array2;
	}
	
	/**
	 * dummy function to solve kostas not usage of generality 
	 */
	public function getChildren() {
		return $this->sublocations;
	}
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Location the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{location}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('parent_id, level', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 45),
            array('description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, parent_id, description, level', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
			'products' => array(self::HAS_MANY, 'Product', 'location_id'),
			'categories' => array(self::HAS_MANY, 'Category', array('category_id'=>'id'),'through'=>'products'),
			
            'activities' => array(self::HAS_MANY, 'Activity', 'location_id'),
            'parent' => array(self::BELONGS_TO, 'Location', 'parent_id'),
            'sublocations' => array(self::HAS_MANY, 'Location', 'parent_id'),
			
            'locationPhotos' => array(self::HAS_MANY, 'LocationPhoto', 'location_id'),
            'seo' => array(self::BELONGS_TO, 'Seo', 'seo_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Location'),
            'parent_id' => null,
            'description' => Yii::t('app', 'Description'),
            'activities' => null,
            'parent' => null,
            'locations' => null,
            'locationPhotos' => null,
            'products' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        if (!$this->parent_id)
            $criteria->condition = 'parent_id is null';
        else
            $criteria->compare('parent_id', $this->parent_id);

        $sort = new CSort;
        $sort->attributes = array(
            'name',
            'description'
        );

        $sort->defaultOrder = array(
            'name' => true,
        );

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 50
                    ),
                    'sort' => $sort
                ));
    }

    public function getTree() {
        //TODO: Improve this shit

        $criteria = new CDbCriteria;
        $criteria->condition = 'parent_id is null';
        $parents = $this->findAll($criteria);

        foreach ($parents as $parent) {
            $this->_locations[$parent->id] = $parent->name;
            $this->_genealogy($parent->id, 0);
        }
        
        return $this->_locations;
    }

    private function _genealogy($location, $level = 0) {
        $criteria = new CDbCriteria;
        $criteria->condition = 'parent_id = '.$location;
        $parents = $this->findAll($criteria);
        
        ++$level;
        
        foreach ($parents as $parent) {
            $this->_locations[$parent->id] = str_repeat('-',$level).' '.$parent->name;
            $this->_genealogy($parent->id, $level, $this->_locations);
        }
    }

}