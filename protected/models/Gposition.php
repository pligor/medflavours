<?php

/**
 * This is the model class for table "gposition".
 * @author pligor
 * The followings are the available columns in table 'gposition':
 * @property integer $id
 * @property double $ce_lat
 * @property double $ce_lon
 * @property integer $zoom
 * @property double $sw_lat
 * @property double $sw_lon
 * @property double $ne_lat
 * @property double $ne_lon
 *
 * The followings are the available model relations:
 * @property Activity[] $activities
 */
class Gposition extends CActiveRecord
{
	public $gposition;
	
	public function getExisting() {
		$criteria=new CDbCriteria;
		$criteria->compare('sw_lat',$this->sw_lat ?: ' ');	//a trick for empty elements
		$criteria->compare('sw_lon',$this->sw_lon ?: ' ');
		$criteria->compare('ne_lat',$this->ne_lat ?: ' ');
		$criteria->compare('ne_lon',$this->ne_lon ?: ' ');
		
		$models = self::model()->findAll($criteria);
		
		if( empty($models) ) {
			//print "hello";
			return $this;
		}
		else {
			//print "world";
			return reset($models);
		}
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Gposition the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{gposition}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ce_lat, ce_lon, zoom, sw_lat, sw_lon, ne_lat, ne_lon', 'required'),
			array('zoom', 'numerical', 'integerOnly'=>true),
			array('ce_lat, ce_lon, sw_lat, sw_lon, ne_lat, ne_lon', 'numerical'),
			array('gposition','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ce_lat, ce_lon, zoom, sw_lat, sw_lon, ne_lat, ne_lon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activities' => array(self::HAS_MANY, 'Activity', 'gposition_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => t('ID'),
			'ce_lat' => t('Ce Lat'),
			'ce_lon' => t('Ce Lon'),
			'zoom' => t('Zoom'),
			'sw_lat' => t('Sw Lat'),
			'sw_lon' => t('Sw Lon'),
			'ne_lat' => t('Ne Lat'),
			'ne_lon' => t('Ne Lon'),
			'gposition' => '',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ce_lat',$this->ce_lat);
		$criteria->compare('ce_lon',$this->ce_lon);
		$criteria->compare('zoom',$this->zoom);
		$criteria->compare('sw_lat',$this->sw_lat);
		$criteria->compare('sw_lon',$this->sw_lon);
		$criteria->compare('ne_lat',$this->ne_lat);
		$criteria->compare('ne_lon',$this->ne_lon);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * @return \CForm 
	 */
	public function getForm($formClass='CForm') {
		$config = $this->getFormConfig();
		return new $formClass($config, $this);	//all subforms get the model from their parent
	}
	
	/**
	 * @return array with the configuration of this form 
	 */
	public function getFormConfig() {
		return array(
		    //'title' => '',
		    'showErrorSummary' => true,
		    //'elements' => array(get_class() => $this->getFormElements(),),	//if you want subforms
			'elements' => $this->getFormElements(),
		    'buttons' => array(
		        'submit_'.strtolower(get_class()) => array(
					'type' => 'submit',
					'label' => Yii::t('', 'Submit '.get_class()),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
			'activeForm' => array(
				'class' => 'CActiveForm',
			),
		);
	}
	/**
	 * @return array the elements of this form
	 */
	public function getFormElements() {
		return array(
			'gposition' => array(
				'type' => 'external.erikuus.widgets.google.XGoogleInputMap',
				//'type' => 'text',
				//'hint' => '',
				'attributes' => array(
					'googleApiKey' => Yii::app()->params['googleApiKey'],
					//'model' => $this,
					//'form' => new CActiveForm,	//we created a new kind of CForm for this
				),
			),
		);
	}
}
