<?php

/**
 * This is the model class for table "addon".
 *
 * The followings are the available columns in table 'addon':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $addon_group_id
 *
 * The followings are the available model relations:
 * @property Activity $activity
 */
class Addon extends ActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Addon the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{addon}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, price, addon_group_id', 'required'),
            array('count, addon_group_id', 'numerical', 'integerOnly'=>true),
            array('price', 'length', 'max'=>11),
            array('unit', 'length', 'max'=>45),
            array('description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, description, price, count, unit, addon_group_id', 'safe', 'on'=>'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addonGroup' => array(self::BELONGS_TO, 'AddonGroup', 'addon_group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'id' => 'ID',
            'name' => 'Title',
            'description' => 'Description',
            'price' => 'Price',
            'count' => 'Count',
            'unit' => 'Unit',
            //'addon_group_id' => 'Addon Group Id',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('price',$this->price,true);
        $criteria->compare('count',$this->count);
        $criteria->compare('unit',$this->unit,true);
        $criteria->compare('addon_group_id',$this->addon_group_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
	
	public function formInputsForJson() {
		$elements = $this->getFormElements();
		$class = get_class();
		$labels = $this->attributeLabels();
		
		foreach($elements as $name => & $element) {
			if($element['type'] != 'text') {
				throw new CException('please provide functionality for other inputs. currently only text is supported');
			}
			
			$element['label'] = $labels[$name];
			$element['model_name'] = $class;
			$element['attr_name'] = $name;
			//type is already set
			$element['value'] = $this->$name;
		}
		
		return $elements;
	}
	
	/**
	 * @return \CForm 
	 */
	public function getForm($formClass='CForm') {
		$config = $this->getFormConfig();
		return new $formClass($config, $this);	//all subforms get as parent their model
	}
	
	public function getFormConfig() {
		$modelName = get_class();
		return array(
		    //'title' => '..............',
		    'showErrorSummary' => true,
		    /*'elements' => array(
				get_class() => $this->getFormElements(),
			),*/
			'elements' => $this->getFormElements(),
		    'buttons' => array(
		        'submit_'.lcfirst($modelName) => array(
					'type' => 'submit',
					'label' => 'Submit '.$modelName,
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormElements() {
		return array(
			'name' => array(
				'type' => 'text',
				//'hint' => '',
				/*'attributes' => array(
					'size' => 5,
					'maxlength' => 255,
	        	),*/
				'size' => 5,
	        ),
	        'description' => array(
				'type' => 'text',
				'size' => 5,
	        ),
			'price' => array(
				'type' => 'text',
				'size' => 5,
	        ),
			'count' => array(
				'type' => 'text',
				'size' => 5,
	        ),
			'unit' => array(
				'type' => 'dropdownlist',
				'items' => AddonUnit::listData(),
				//'size' => 5,
	        ),
			/*'addon_group_id' => array(
				'type' => 'hidden',
			),*/
		);
	}
}