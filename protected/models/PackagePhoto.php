<?php

/**
 * This is the model class for table "package_photo".
 *
 * The followings are the available columns in table 'package_photo':
 * @property integer $id
 * @property integer $package_id
 * @property string $photo
 *
 * The followings are the available model relations:
 * @property Package $package
 */
class PackagePhoto extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PackagePhoto the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'package_photo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('package_id, photo', 'required'),
            array('package_id', 'numerical', 'integerOnly' => true),
            array('photo', 'length', 'max' => 40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, package_id, photo', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'package' => array(self::BELONGS_TO, 'Package', 'package_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'package_id' => 'Package',
            'photo' => 'Photo',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('package_id', $this->package_id);
        $criteria->compare('photo', $this->photo, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}