<?php

/**
 * This is the model class for table "product_meta".
 *
 * The followings are the available columns in table 'product_meta':
 * @property integer $product_id
 * @property string $small_description
 * @property string $full_description
 * @property double $discount
 * @property integer $vat
 *
 * The followings are the available model relations:
 * @property Product $product
 */
class ProductMeta extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ProductMeta the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'product_meta';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
//            array('product_id', 'required'),
            array('product_id, vat, discount', 'numerical', 'integerOnly' => true),
			array(
				'discount',
				'in',
				'range' => range(0, 100),
			),
            array('small_description, full_description', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('product_id, small_description, full_description, discount, vat', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'product_id' => 'Product',
            'small_description' => 'Small Description',
            'full_description' => 'Full Description',
            'discount' => 'Discount',
            'vat' => 'Vat',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('product_id', $this->product_id);
        $criteria->compare('small_description', $this->small_description, true);
        $criteria->compare('full_description', $this->full_description, true);
        $criteria->compare('discount', $this->discount);
        $criteria->compare('vat', $this->vat);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}