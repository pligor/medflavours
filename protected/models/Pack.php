<?php

/**
 * This is the model class for table "pack".
 * @author pligor
 * The followings are the available columns in table 'pack':
 * @property integer $id
 * @property string $name
 * @property string $price_b2b
 * @property string $price_b2c
 * @property string $description
 * @property integer $stock
 * @property string $photo
 * @property double $weight
 * @property integer $product_id
 * @property integer $condition_id
 *
 * The followings are the available model relations:
 * @property Product $product
 * @property Condition $condition
 */
class Pack extends CActiveRecord
{
	public $photo_types;
	public $photo_dir;
	
	protected $photo_file_instance;
	
	public function init() {
		parent::init();
		$this->photo_types = array('png','jpg');
		
		$this->photo_dir = Yii::getPathOfAlias('webroot.images.photos.packs');
	}
	
	public function photoUrl() {
		return Yii::app()->baseUrl .'/images/photos/packs/'. $this->photo;
	}
	
	public function photoFullpath() {
		return $this->photo_dir .DIRECTORY_SEPARATOR. $this->photo;
	}
	
	public function beforeSave() {
		$isBeforeSaved = parent::beforeSave();
		
		$attribute = 'photo';
		$this->photo_file_instance = CUploadedFile::getInstance($this, $attribute);
		
		if($this->photo_file_instance !== null) {
			$this->photo = $this->photo_file_instance->name;
		}
		
		return $isBeforeSaved;
	}
	
	public function afterSave() {
		parent::afterSave();
		
		if($this->photo_file_instance === null) {
			return;
		}
		
		$file_instance = $this->photo_file_instance;
		
		$path = $this->photo_dir .'/'. $file_instance->name;
		
		if(is_file($path) && !unlink($path)) {
			$message = "Overwriting $path has failed";
			throw new CException($message);
		}
		
		if( !$file_instance->saveAs($path) ) {
			$message = "Saving $path has failed";
			throw new CException($message);
		}
		
		$this->photo=null;
	}

	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pack the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{pack}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, price_b2b, price_b2c, stock, product_id, condition_id', 'required'),
			array('stock, product_id, condition_id', 'numerical', 'integerOnly'=>true),
			array('weight', 'numerical'),
			array('price_b2b, price_b2c', 'length', 'max'=>11),
			array('photo', 'length', 'max'=>255),
			
			array(
				'photo',
				'file',
				'allowEmpty' => true,
				'maxFiles' => 1,
				//'tooMany' => 'some error message',
				//'maxSize' => $this->maxSize,
				//'tooLarge' => 'some error message',
				//'minSize' => $this->minSize,
				//'tooSmall' => 'some error message',
				'types' => $this->photo_types,	//extension names separated by space or comma
				//'wrongType' => 'some error message',
			),
			
			array('description', 'safe'),
			// The following rule is used by search().Please remove those attributes that should not be searched.
			array('id, name, price_b2b, price_b2c, description, stock, photo, weight, product_id, condition_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
			'condition' => array(self::BELONGS_TO, 'Condition', 'condition_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => t('ID'),
			'name' => t('Name'),
			'price_b2b' => t('Supplier Price'),
			'price_b2c' => t('Price b2c'),
			'description' => t('Description'),
			'stock' => t('Stock'),
			'photo' => t('Photo'),
			'weight' => t('Weight'),
			'product_id' => t('Product'),
			'condition_id' => t('Condition'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		//$criteria->compare('price_b2b',$this->price_b2b,true);
		//$criteria->compare('price_b2c',$this->price_b2c,true);
		//$criteria->compare('description',$this->description,true);
		$criteria->compare('stock',$this->stock);
		//$criteria->compare('photo',$this->photo,true);
		//$criteria->compare('weight',$this->weight);
		$criteria->compare('product_id',$this->product_id);
		//$criteria->compare('condition_id',$this->condition_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * @return \CForm 
	 */
	public function getForm($formClass='CForm') {
		$config = $this->getFormConfig();
		return new $formClass($config, $this);	//all subforms get the model from their parent
	}
	
	/**
	 * @return array with the configuration of this form 
	 */
	public function getFormConfig() {
		return array(
		    //'title' => '',
		    'showErrorSummary' => true,
		    //'elements' => array(get_class() => $this->getFormElements(),),	//if you want subforms
			'elements' => $this->getFormElements(),
		    'buttons' => array(
		        'submit_'.strtolower(get_class()) => array(
					'type' => 'submit',
					'label' => Yii::t('', 'Submit '.get_class()),
		        ),
		    ),
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),
		);
	}
	/**
	 * @return array the elements of this form
	 */
	public function getFormElements() {
		return array(
			'name' => array(
				'type' => 'text',
				//'hint' => '',
				'attributes' => array(
				),
			),
			'price_b2b' => array(
				'type' => 'text',
				//'hint' => '',
				'attributes' => array(
				),
			),
			'price_b2c' => array(
				'type' => 'text',
				//'hint' => '',
				'attributes' => array(
				),
			),
			'description' => array(
				'type' => 'text',
				//'hint' => '',
				'attributes' => array(
				),
			),
			'stock' => array(
				'type' => 'text',
				//'hint' => '',
				'attributes' => array(
				),
			),
			'photo' => array(
				'type' => 'file',
				'hint' => 'valid file extensions: '.implode(', ',$this->photo_types),
				'attributes' => array(
				),
			),
			'weight' => array(
				'type' => 'text',
				//'hint' => '',
				'attributes' => array(
				),
			),
			'condition_id' => array(
				'type' => 'dropdownlist',
				//'hint' => '',
				'items' => Condition::allListData(),
				'attributes' => array(
				),
			),
			/*'product_id' => array(
				'type' => 'text',
				//'hint' => '',
				'attributes' => array(
				),
			),*/
		);
	}
}
