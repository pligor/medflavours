<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActivityType
 *
 * @author subdee
 */
class ActivityType {
    const ACTIVITY = 1;
    const EVENT = 2;
    const ATTRACTION = 3;
	
	public static function getCats() {
		return array(
			self::ACTIVITY => 'Act',
			self::EVENT => 'Event',
			self::ATTRACTION => 'Attraction',
		);
	}
	
	public static function getModelByType($type) {
		$cats = self::getCats();
		return $cats[$type];
	}
	
	public static function getCatByType($type) {
		return self::getModelByType($type) . 'Category';
	}
	
	public static function getRelationByType($type) {
		return lcfirst(self::getModelByType($type));
	}
}
