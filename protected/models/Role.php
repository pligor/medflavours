<?php

/**
 * The roles of database users
 * used for controlling access.
 *
 * @author subdee
 */
class Role {
    
    const ADMIN = 1;
    
    const OPERATOR = 2;
    
    public static function getText($role) {
        switch ($role) {
            case self::ADMIN:
                return 'Administrator';
                break;
            case self::OPERATOR:
                return 'Operator';
                break;
            default:
                return 'Unknown';
                break;
        }
    }
    
    public static function getArray() {
        return array(
            self::ADMIN => 'Administrator',
            self::OPERATOR => 'Operator',
        );
    }
    
}

?>
