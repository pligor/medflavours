<?php

/**
 * This is the model class for table "activity_property".
 *
 * The followings are the available columns in table 'activity_property':
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property integer $activity_id
 *
 * The followings are the available model relations:
 * @property Activity $activity
 */
class ActivityProperty extends ActiveRecord {
	
	protected $_id = null;	
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ActivityProperty the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{activity_property}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, value, activity_id', 'required'),
			array('activity_id', 'numerical', 'integerOnly'=>true),
			
			// The following rule is used by search().Please remove those attributes that should not be searched.
			array('id, name, value, activity_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'activity' => array(self::BELONGS_TO, 'Activity', 'activity_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => 'ID',
			'name' => 'Title',
			'value' => 'Value',
			'activity_id' => 'Activity',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('activity_id',$this->activity_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * @return \CForm
	 */
	public function getForm($formClass='CForm') {
		$config = $this->getFormConfig();
		return new $formClass($config, $this);	//all subforms get as parent their model
	}
	
	public function getFormConfig() {
		$modelName = get_class();
		return array(
		    //'title' => '..............',
		    'showErrorSummary' => true,
		    /*'elements' => array(
				get_class() => $this->getFormElements(),
			),*/
			'elements' => $this->getFormElements(),
		    'buttons' => array(
		        'submit_'.lcfirst($modelName) => array(
					'type' => 'submit',
					'label' => 'Submit '.$modelName,
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
		);
	}
	
	/**
	 * @return array configuration to be used inside the configuration of a complex (nested) CForm model
	 */
	public function getFormElements() {
		return array(
			'name' => array(
				'type' => 'zii.widgets.jui.CJuiAutoComplete',
				//'hint' => $this->hint,
				'attributes' => array(
'id' => Hlp::veryRandom(), //this wouldn't be necessary if this: http://www.yiiframework.com/doc/api/1.1/CJuiAutoComplete#c7581
					'source' => array_values( array_unique($this->retrieveColumn('name')) ),	//array_values is necessary
					'options' => array(   //options reference: http://jqueryui.com/demos/autocomplete/
						'minLength' => 1,
						'delay' => 300,
					),
				),
	        ),
	        'value' => array(
				'type' => 'text',
	        ),
			/*'activity_id' => array(
				'type' => 'hidden',
			),*/
		);
	}
}