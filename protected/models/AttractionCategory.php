<?php

/**
 * This is the model class for table "attraction_category".
 * @author pligor
 * The followings are the available columns in table 'attraction_category':
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Attraction[] $attractions
 * @property AttractionCategory $parent
 * @property AttractionCategory[] $attractionCategories
 */
class AttractionCategory extends ActiveRecord
{
	public function behaviors() {
		return array_merge(parent::behaviors(),array(
			'tree' => array(
				'class' => 'beh.TreeBehavior',
				//'property1'=>'value1',
			),
		));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AttractionCategory the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName() {
		return '{{attraction_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('parent_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>45),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parent_id, name, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'attractions' => array(self::HAS_MANY, 'Attraction', 'attraction_category_id'),
			'parent' => array(self::BELONGS_TO, 'AttractionCategory', 'parent_id'),
			'children' => array(self::HAS_MANY, 'AttractionCategory', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'id' => t('ID'),
			'parent_id' => t('Parent'),
			'name' => t('Name'),
			'description' => t('Description'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		if( !$this->parent_id ) {	//case of top nodes
            $criteria->condition = 'parent_id IS NULL';
		}
        else {
            $criteria->compare('parent_id', $this->parent_id);
		}
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * @return \CForm 
	 */
	public function getForm($formClass='CForm') {
		$config = $this->getFormConfig();
		return new $formClass($config, $this);	//all subforms get the model from their parent
	}
	
	/**
	 * @return array with the configuration of this form 
	 */
	public function getFormConfig() {
		return array(
		    //'title' => '',
		    'showErrorSummary' => true,
		    //'elements' => array(get_class() => $this->getFormElements(),),	//if you want subforms
			'elements' => $this->getFormElements(),
		    'buttons' => array(
		        'save' => array(
					'type' => 'submit',
					'label' => t('Save'),
		        ),
		    ),/*
		    'attributes' => array(
		    	'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
		    ),*/
			'activeForm' => array(
				'class' => 'CActiveForm',
			),
		);
	}
	/**
	 * @return array the elements of this form
	 */
	public function getFormElements() {
		return array(
			'parent_id' => array(
				'type' => 'dropdownlist',
				'items' => $this->listData(),
				//'hint' => '',
				'attributes' => array(
					'prompt' => '----',
				),
			),
			'name' => array(
				'type' => 'text',
				//'hint' => '',
				'attributes' => array(
				),
			),
			'description' => array(
				'type' => 'external.cleditor.ECLEditor',
				'attributes' => array(
					'options' => array(
						'width' => '500',
						'height' => 250,
						'useCSS' => true,
						'controls' => _controls(),
					),
				),
			),
		);
	}
}
