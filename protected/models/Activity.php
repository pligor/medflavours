<?php
/**
 * This is the model class for table "activity".
 *
 * The followings are the available columns in table 'activity':
 * @property integer $id
 * @property integer $location_id
 * @property string $name
 * @property integer $is_active
 * @property double $price
 * @property string $voucher_info
 * @property integer $activity_type
 * @property string $supplier_extra_info
 *
 * The followings are the available model relations:
 * @property Location $location
 * @property ActivityPeriod[] $activityPeriods
 * @property ActivityPhoto[] $activityPhotos
 * @property Order[] $orders
 * @property Gposition $gposition
 * 
 * @property AddonGroup[] $addonGroups
 * @property Supplier $supplier
 * @property ActivityProperty $activityProperties
 * 
 * @property Act $act
 * @property Event $event
 * @property Attraction $attraction
 */
class Activity extends CActiveRecord {

	/**
	 * common attribute to save in memory before save on database
	 * @var type 
	 */
	public $_category_id = null;
	
	public function getCategory_id() {
		$child = $this->getChild();
		if($child) {
			return $child->category_id;
		}
		return $this->_category_id;
	}
	
	public function setCategory_id($value) {
		$this->_category_id = $value;
	}
	
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Activity the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'activity';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('location_id, name, price, supplier_id, category_id', 'required'),
            array('location_id, is_active, activity_type, supplier_id, category_id', 'numerical', 'integerOnly' => true),
            array('price', 'numerical'),
            array('name', 'length', 'max' => 45),
			//array('long_lat', 'length', 'max' => 45),
            array('voucher_info, description, supplier_extra_info', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, location_id, name, is_active, price, voucher_info, activity_type, supplier_extra_info, gposition_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
			'addonGroups' => array(self::HAS_MANY, 'AddonGroup', 'activity_id'),
			
			'gposition' => array(self::BELONGS_TO, 'Gposition', 'gposition_id'),
			
			'act' => array(self::HAS_ONE, 'Act', 'activity_id'),
            'event' => array(self::HAS_ONE, 'Event', 'activity_id'),
			'attraction' => array(self::HAS_ONE, 'Attraction', 'activity_id'),
			
			'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplier_id'),
			'activityProperties' => array(self::HAS_MANY, 'ActivityProperty', 'activity_id'),
			
            'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
            'activityPeriods' => array(self::HAS_MANY, 'ActivityPeriod', 'activity_id'),
            'activityPhotos' => array(self::HAS_MANY, 'ActivityPhoto', 'activity_id'),
            'orders' => array(self::MANY_MANY, 'Order', 'order_activity(activity_id, order_id)'),
            'seo' => array(self::BELONGS_TO, 'Seo', 'seo_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
			'supplier_id' => 'Supplier',
			
            'id' => 'ID',
            'location_id' => 'Location',
            'name' => 'Name',
            'is_active' => 'Is Active',
            'price' => 'Price',
            //'long_lat' => 'Long Lat',
            'voucher_info' => 'Voucher Info',
            'activity_type' => 'Activity Type',
			'category_id' => 'Category',
			'supplier_extra_info' => 'Supplier extra info for this flavour',
			'gposition_id' => t('Gposition'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('location_id', $this->location_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('price', $this->price);
        //$criteria->compare('long_lat', $this->long_lat, true);
        $criteria->compare('voucher_info', $this->voucher_info, true);
        $criteria->compare('activity_type', $this->activity_type);
		$criteria->compare('supplier_extra_info', $this->supplier_extra_info, true);
		$criteria->compare('gposition_id',$this->gposition_id);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
	
	/**
	 * Get the child model
	 * @return Act|Event|Attraction|null 
	 */
	public function getChild() {
		if($this->act) {
			return $this->act;
		}
		elseif($this->event) {
			return $this->event;
		}
		elseif($this->attraction) {
			return $this->attraction;
		}
		
		return null;
	}
	
	public function beforeSave() {
		$beforeSaved = parent::beforeSave();
		
		//delete child
		$check = false;
		$child = $this->getChild();
		
		if(!$child) {
			$check = true;
		}
		elseif($child->delete() == 1) {
			$check = true;
		}
		
		return $beforeSaved && $check;
	}
	
	public function afterSave() {
		parent::afterSave();
		
		$model_class = ActivityType::getModelByType($this->activity_type);
		
		$child = new $model_class();
		$child->activity_id = $this->id;		
		$child->category_id = $this->_category_id;
		$child->save();
	}
	
	/**
     * @return \CForm 
     */
    public function getForm($formClass='CForm') {
        $config = $this->getFormConfig();
        return new $formClass($config, $this);    //all subforms get the model from their parent
    }
    
    /**
     * @return array with the configuration of this form 
     */
    public function getFormConfig() {
        return array(
            //'title' => '',
            'showErrorSummary' => true,
            //'elements' => array(get_class() => $this->getFormElements(),),    //if you want subforms
            'elements' => $this->getFormElements(),
            'buttons' => array(
                'submit_'.strtolower(get_class()) => array(
                    'type' => 'submit',
                    'label' => Yii::t('', 'Submit '.get_class()),
                ),
            ),/*
            'attributes' => array(
                'enctype' => 'multipart/form-data', //ALWAYS REMEMBER TO DEFINE THIS WHEN HANDLING FILES
            ),*/
            'activeForm' => array(
                'class' => 'CActiveForm',
            ),
        );
    }
	
    /**
     * @return array the elements of this form
     */
    public function getFormElements() {
		$categoryClass = ActivityType::getCatByType($this->activity_type);
		
		$elements = array(
			'category_id' => array(
				'type' => 'dropdownlist',
				'items' => $categoryClass::model()->listData(),
			),
		);
		
        $elements = $elements + array(
			'supplier_id' => array(
				'type' => 'dropdownlist',
				'items' => Supplier::allListData(),
			),
			'supplier_extra_info' => array(
				'type' => 'external.cleditor.ECLEditor',
				'attributes' => array(
					'options' => array(
						'width' => 500,
						'height' => 250,
						'useCSS' => true,
						'controls' => _controls(),
					),
				),
			),
			'location_id' => array(
				'type' => 'dropdownlist',
				'items' => Location::model()->getTree(),
			),
			'name' => array(
				'type' => 'text',
			),
			'description' => array(
				'type' => 'external.cleditor.ECLEditor',
				'attributes' => array(
					'options' => array(
						'width' => 500,
						'height' => 250,
						'useCSS' => true,
						'controls' => _controls(),
					),
				),
			),
			'price' => array(
				'type' => 'text',
			),
			'is_active' => array(
				'type' => 'checkbox',
			),
			'voucher_info' => array(
				'type' => 'external.cleditor.ECLEditor',
				'attributes' => array(
					'options' => array(
						'width' => '500',
						'height' => 250,
						'useCSS' => true,
						'controls' => _controls(),
					),
				),
			),
			/*'gposition_id' => array(
				'type' => 'hidden',
				//'hint' => '',
				'attributes' => array(
				),
			),*/
			/*'long_lat' => array(
				'type' => 'hidden',
				//'type' => 'text',
				//'label' => 'Coordinates',
				//'attributes' => array('onclick' => '$("#dialog").dialog("open"); google.maps.event.trigger(EGMap0, "resize"); return false;'),
			),*/
		);
		
		return $elements;
    }
}