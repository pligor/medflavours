<?php

/**
 * This is the model class for table "order_product".
 *
 * The followings are the available columns in table 'order_product':
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $amount
 */
class OrderProduct extends CActiveRecord {

    public $total = 0;
    public $start_date;
    public $end_date;
    public $supplier_id;
    public $category_id;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ActivityPeriod the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'order_product';
    }

    public function rules() {
        return array(
            array('order_id, product_id', 'required'),
            array('order_id, product_id, amount', 'numerical', 'integerOnly' => true),
            array('amount', 'default', 'setOnEmpty' => true, 'value' => null),
            array('order_id, product_id, amount, supplier_id, category_id, start_date, end_date', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
            'product' => array(self::BELONGS_TO, 'Product', 'product_id'),
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->with[] = 'order';
        $criteria->compare('order.status', OrderStatus::SENT_TO_CUSTOMER);

        if ($this->supplier_id) {
            $criteria->with[] = 'product';
            $criteria->compare('product.supplier_id', $this->supplier_id);
        }

        if ($this->category_id) {
            $criteria->with[] = 'product';
            $criteria->compare('product.category_id', $this->category_id);
        }

        if ($this->start_date && $this->end_date) {
            $criteria->addCondition('order.date_entered between :startDate and :endDate');
            $criteria->params[':startDate'] = $this->start_date;
            $criteria->params[':endDate'] = $this->end_date;
        }

        $sort = new CSort;
        $sort->attributes = array(
            'order.date_entered',
        );

        $sort->defaultOrder = array(
            'order.date_entered' => true,
        );

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 20
                    ),
                    'sort' => $sort
                ));
    }

    protected function afterFind() {
        $this->total += $this->product->price * $this->amount;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'order_id' => 'Order',
            'product_id' => 'Product',
            'amount' => 'Amount',
        );
    }

}