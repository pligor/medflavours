<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SelectionStyle
 *
 * @author pligor
 */
class SelectionStyle {
	//put your code here
	const CHECKBOX = 1;
	const RADIO = 2;
	
	public static function getText($id) {
		$listData = self::listData();
		return $listData[$id];
	}
	
	public static function listData() {
		return array(
			self::CHECKBOX => 'check box',
			self::RADIO => 'radio button',
		);
	}
}