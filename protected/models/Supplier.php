<?php

/**
 * This is the model class for table "supplier".
 *
 * The followings are the available columns in table 'supplier':
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $email
 * @property string $phone
 *
 * The followings are the available model relations:
 * @property Product[] $products
 */
class Supplier extends CActiveRecord {

	public static function allListData() {
		return CHtml::listData(self::model()->findAll(), 'id', 'name');
	}
	
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Supplier the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{supplier}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, location_id', 'required'),
            array('name, address, email', 'length', 'max' => 45),
            array('phone', 'length', 'max' => 16),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, address, email, phone', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
			'activities' => array(self::HAS_MANY, 'Activity', 'supplier_id'),
			
            'products' => array(self::HAS_MANY, 'Product', 'supplier_id'),
            'seo' => array(self::BELONGS_TO, 'Seo', 'seo_id'),
            'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Supplier'),
            'address' => Yii::t('app', 'Address'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'products' => null,
            'location_id' => 'Location',
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $sort = new CSort;
        $sort->attributes = array(
            'name',
            'email'
        );

        $sort->defaultOrder = array(
            'name' => true,
        );

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => array(
                        'pageSize' => 50
                    ),
                    'sort' => $sort
                ));
    }

}