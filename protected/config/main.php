<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
$basePath = realpath( dirname(__FILE__).'/..' );

Yii::setPathOfAlias('beh', $basePath .'/extensions/behaviors');

$yii_path = realpath( $basePath.'/../../yii' );
Yii::setPathOfAlias('yii', $yii_path);

$externalPath = realpath( $yii_path .'/extensions' );
Yii::setPathOfAlias('external', $externalPath);

return array(
	'defaultController' => 'admin',	
	
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Mediterranean Flavours',
    'preload' => array(
		'log',
		'kint', //this is only for declaring the shortcut functions: 'd' and 'dd'
	),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'ext.giix-components.*',
        
        'yii.components.*',
    ),
	'behaviors' => array(
        'app-config-beh' => array(
            'class' => 'beh.ApplicationConfigBehavior',
            //'property1'=>'value1',
        )
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'generatorPaths' => array(
                'ext.giix-core',
            ),
            'password' => 'gii',
        ),
        'admin' => array(
        ),
    ),
    'components' => array(
		'kint' => array(
			'class' => 'external.Kint.Kint',
		),
        'user' => array(
            'allowAutoLogin' => true,
            'loginUrl'=>array('admin/main/login'),
        ),/*
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
				//'(^admin)/<controller:\w+>/<action:\w+>' => '(^admin)/<controller>/<action>',
				'admin/<action:\w+>' => 'admin/main/<action>',
            ),
            'showScriptName' => false,
        ),*/
        'db' => array(
		'connectionString' => 'mysql:host=127.0.0.1;port=3306;dbname=medflavours',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'qr21131s35Uy4mT',
            'charset' => 'utf8',
            'enableProfiling' => true,
            'enableParamLogging' => true,
		'autoConnect' => true,
		'tablePrefix' => '',
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                /*array(
                    'class' => 'CProfileLogRoute',
                    'showInFireBug'=>true,
                ),*/
				/*array(
					'class'=>'CWebLogRoute',
                ),*/
            ),
        ),
        'assetManager' => array(
            'linkAssets' => false,	//PROBLEMATIC WHEN true
        ),
    ),
    'params' => array(
		'imageFolder' => 'images',
		'googleApiKey' => 'AIzaSyDI20ZYXdNdilyrRyQOuKXvruPmTq_8qH4',
    ),
);
