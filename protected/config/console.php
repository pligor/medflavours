<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'My Console Application',
    // application components
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=medflavours',
            'emulatePrepare' => true,
            'username' => 'medflavours',
            'password' => 'mflavours123',
            'charset' => 'utf8',
        ),
        'testdb' => array(
            'connectionString' => 'mysql:host=localhost;dbname=medflavours_test',
            'emulatePrepare' => true,
            'username' => 'medflavours',
            'password' => 'mflavours123',
            'charset' => 'utf8',
            'class' => 'CDbConnection'
        ),
    ),
    'commandMap' => array(
        'migrate' => array(
            'class' => 'system.cli.commands.MigrateCommand',
            'migrationTable' => 'yii_migration',
            'connectionID' => 'db',
        ),
    ),)
;