<?php
//ini_set('post_max_size', '120M');
//ini_set('upload_max_filesize', '30M');
//ini_set('max_file_uploads', '28');

ini_set('date.timezone', 'Europe/Athens');

//never define this from ini_set. causes the reverse effect. errors are NOT shown!
//ini_set('error_reporting', 'E_ALL | E_STRICT');

ini_set('display_errors', 'On');
ini_set('display_startup_errors', 'On');

ini_set('memory_limit', '4095M');	//max 32bit memory

//This sets the maximum time in seconds a script is allowed to run before it is terminated by the parser
//You can not change this setting with ini_set() when running in safe mode
ini_set('max_execution_time', '300');	//0 means forever

//This sets the maximum time in seconds a script is allowed to parse input data, like POST and GET.
//It is measured from the moment of receiving all data on the server to the start of script execution.
ini_set('max_input_time', '3600');	//-1 means forever
