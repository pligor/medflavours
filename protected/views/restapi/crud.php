<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><?php print $status .' '. $this->getStatusCodeMessage($status); ?></title>
	</head>
	<body>
		<h1><?php print $this->getStatusCodeMessage($status); ?></h1>
		<p><?php print $message; ?></p>
		<hr />
		<address><?php print $signature; ?></address>
	</body>
</html>