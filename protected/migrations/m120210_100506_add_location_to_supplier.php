<?php

class m120210_100506_add_location_to_supplier extends CDbMigration {

    public function up() {
        $this->addColumn('supplier', 'location_id', 'INT(11) NOT NULL');
        $this->execute('UPDATE supplier SET location_id = (SELECT MAX(id) FROM location)');
        $this->addForeignKey('fk_supplier_location', 'supplier', 'location_id', 'location', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropForeignKey('fk_supplier_location', 'supplier');
        $this->dropColumn('supplier', 'location_id');
    }
}