<?php

class m120130_082259_alter_activity_table extends CDbMigration {

    public function safeUp() {
        $this->addColumn('activity', 'description', 'text');
    }

    public function safeDown() {
        $this->dropColumn('activity', 'description');
    }

}