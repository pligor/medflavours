<?php

class m120213_132306_modify_customer_table extends CDbMigration {

    public function up() {
        $this->addColumn('customer', 'password', 'CHAR(64) NOT NULL');
        $this->addColumn('customer','registered','timestamp');
    }

    public function down() {
        $this->dropColumn('customer', 'password');
        $this->dropColumn('customer', 'registered');
    }

}