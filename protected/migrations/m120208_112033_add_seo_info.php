<?php

class m120208_112033_add_seo_info extends CDbMigration {

//    public function up() {
//        
//    }
//
//    public function down() {
//        echo "m120208_112033_add_seo_info does not support migration down.\n";
//        return false;
//    }

    public function safeUp() {
        $this->createTable('seo', array(
            'id' =>'int(11) NOT NULL AUTO_INCREMENT',
            'url' => 'varchar(40) NOT NULL',
            'title' => 'varchar(100) NOT NULL',
            'description' => 'text NOT NULL',
            'keywords' => 'text NOT NULL',
            'PRIMARY KEY (`id`)'
        ),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        
        $this->addColumn('product', 'seo_id', 'int(11)');
        $this->addForeignKey('fk_product_seo', 'product', 'seo_id', 'seo', 'id', 'CASCADE', 'CASCADE');
        
        $this->addColumn('package', 'seo_id', 'int(11)');
        $this->addForeignKey('fk_package_seo', 'package', 'seo_id', 'seo', 'id', 'CASCADE', 'CASCADE');
        
        $this->addColumn('activity', 'seo_id', 'int(11)');
        $this->addForeignKey('fk_activity_seo', 'activity', 'seo_id', 'seo', 'id', 'CASCADE', 'CASCADE');
        
        $this->addColumn('category', 'seo_id', 'int(11)');
        $this->addForeignKey('fk_category_seo', 'category', 'seo_id', 'seo', 'id', 'CASCADE', 'CASCADE');
        
        $this->addColumn('location', 'seo_id', 'int(11)');
        $this->addForeignKey('fk_location_seo', 'location', 'seo_id', 'seo', 'id', 'CASCADE', 'CASCADE');
        
        $this->addColumn('supplier', 'seo_id', 'int(11)');
        $this->addForeignKey('fk_supplier_seo', 'supplier', 'seo_id', 'seo', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
        $this->dropForeignKey('fk_supplier_seo', 'supplier');
        $this->dropForeignKey('fk_location_seo', 'location');
        $this->dropForeignKey('fk_category_seo', 'category');
        $this->dropForeignKey('fk_activity_seo', 'activity');
        $this->dropForeignKey('fk_package_seo', 'package');
        $this->dropForeignKey('fk_product_seo', 'product');
        
        $this->dropColumn('seo_id', 'supplier');
        $this->dropColumn('seo_id', 'location');
        $this->dropColumn('seo_id', 'category');
        $this->dropColumn('seo_id', 'activity');
        $this->dropColumn('seo_id', 'package');
        $this->dropColumn('seo_id', 'product');
        
        $this->dropTable('seo');
    }

}