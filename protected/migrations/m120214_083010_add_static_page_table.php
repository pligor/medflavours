<?php

class m120214_083010_add_static_page_table extends CDbMigration {

    public function up() {
        $this->createTable('static_content', array(
            'id' =>'pk',
            'content' => 'text',
            'menu_title' => 'varchar(20)',
            'show_in_menu' => 'boolean',
            'seo_id' => 'int(11)',
        ),'ENGINE=InnoDB DEFAULT CHARSET=utf8');
        
        $this->addForeignKey('fk_static_content_seo', 'static_content', 'seo_id', 'seo', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropTable('static_content');
    }

}