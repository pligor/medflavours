<?php

class m120207_133303_add_order_ai extends CDbMigration {

    public function up() {
        $this->alterColumn('order', 'id', 'INT(11) NOT NULL AUTO_INCREMENT');
    }

    public function down() {
        echo "m120207_133303_add_order_ai does not support migration down.\n";
        return false;
    }

    /*
      // Use safeUp/safeDown to do migration with transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}