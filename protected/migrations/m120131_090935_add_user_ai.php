<?php

class m120131_090935_add_user_ai extends CDbMigration {

    public function up() {
        $this->alterColumn('user', 'id', 'INT(11) NOT NULL AUTO_INCREMENT');
        $this->update('user', array('id' => 1));
    }

    public function down() {
        echo "m120131_090935_add_user_ai does not support migration down.\n";
        return false;
    }

}