<?php ///* ?>
<?php print CHtml::tag('h3', array(), $this->title); ?>
<div id="<?php print $this->id; ?>">
<?php ///* ?>
<?php
	$models = $this->models;
	$viewPath = 'application.views.'.lcfirst($this->model_name).'.'.$this->renderOuter;
	
	$newModel = new $this->model_name();
	$newModel->attributes = $this->params;
	$this->render($viewPath, array('model'=>$newModel));
	
	if( !Hlp::isEmpty($models) ) {
		foreach($models as $model) {
			$model->attributes = $this->params; //pass some hidden attributes (parent ids etc.)
			$this->render($viewPath, compact('model'));
		}
	}
?>
</div>
<?php
//overwrite id, necessary for models with composite keys
$attributesArray = array();
foreach($this->models as $model) {
	$temporary = $model->attributes;
	$temporary['id'] = $model->id;
	$attributesArray[] = $temporary;
}
?>

<script type="text/javascript">
//namespace object
var NamespaceObj_<?php print $this->id; ?> = {
	items_view: null
}

$(document).ready(function() {
	var multiLines = new MultiLine_vthrees();

	multiLines.model_name = '<?php print $this->model_name; ?>';

	multiLines.url = '<?php print Yii::app()->baseUrl; ?>' + '/index.php?r=restapi&class=<?php print $this->model_name; ?>';

	multiLines.render_action_url = '<?php print Yii::app()->createUrl($this->renderRoute); ?>';

	multiLines.add_icon_url = '<?php print Hlp::icon('add.png'); ?>';
	multiLines.remove_icon_url = '<?php print Hlp::icon('remove.png'); ?>';

	NamespaceObj_<?php print $this->id; ?>.items_view = new MultiLine_vthreesView({
		el: 'div#<?php print $this->id; ?>',
		collection: multiLines
	});
	
	var jsonArray = <?php print CJSON::encode($attributesArray); ?>;
	NamespaceObj_<?php print $this->id; ?>.items_view.collection.reset(jsonArray);
	
	var model = new MultiLine_vthree(<?php print CJSON::encode($newModel->attributes); ?>);	//DYNAMICALLY CREATE A MODEL FROM PHP ;)
	delete model.id;	//just make sure it is a new model
	NamespaceObj_<?php print $this->id; ?>.items_view.collection.add(model,{silent: true});
	model.attach_view();
});
</script>

<script type="text/javascript">
$(document).ready(function() {

});
</script>

<script id="button_tmpl" type="text/x-jquery-tmpl">
	<a href="#">
		<img src="${src}" alt="${alt}">
	</a>
</script>