<?php
/**
 * For this to work we need:
 * Create a controller which has the same name as the model class but with the first letter as lowercase (it is like that by yii..)
 * Inside the above controller create two actions to render the line and the line with the div
 * For a new model leave the id null, meaning empty
 * For an old model the id is its number. On composite key the id is the implode of composite key with glue the underscore _
 * The line must have this format:
 * <div id=".....">		//this is for the model
 *     <div class="row field_ATTRIBUTENAME">
 *         <input id="MODELNAME_ATTRIBUTENAME" name="MODELNAME[ATTRIBUTENAME]" .....>	//I do NOT imply that the modelname and attrname are uppercase
 *         or <select .....>
 *         etc
 *     </div>
 * </div>
 * 
 * @author pligor
 */
class MultilineWidget_vthree extends CWidget {
	public $title;
	public $model_name;
	public $models;
	public $params;
	
	public $renderAction = 'render_line';
	public $renderOuter = 'render';
	
	protected $renderRoute;
	
	public function init() {
		parent::init();
		$this->renderRoute = '/'. lcfirst($this->model_name) .'/'. $this->renderAction;
	}
	
	public function run() {
		$this->render('multiline_vthree');
	}
}