<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AdjacencyGrid
 *
 * @author pligor
 */
class AdjacencyGrid extends CAction {
	
	/**
	 * This is the name of the model that has adjacency tree behavior
	 * @var string
	 */
	public $model_name = null;
	
	public function run($parent_id = null) {
		//$this->controller->render('__testing');
		
		$class = $this->model_name;
		
		//$previous = $class::model()->findByPk($parent_id);
        //$previous_id = $previous ? $previous->parent_id : null;	//this is actually previous parent id
		
		$parent = $class::model()->findByPk($parent_id);
		//$grandparent = $parent ? $parent->parent : null;
		//$grandparent_id = $grandparent ? $grandparent->id : null;
		$grandparent_id = $parent ? $parent->parent_id : null;
		
		$model = new $class('search');
        $model->parent_id = $parent_id;

        $this->controller->render('adjacency_grid_view', array(
			'model' => $model,
			'grandparent_id' => $grandparent_id,
			//'parent_id' => $parent_id,
		));
	}
}