<?php
/**
 * Tree Behavior
 * The following conventions must be met:
 * primary key is: id
 * //....
 * @author pligor
 */
class TreeBehavior extends CBehavior {
	public function getDepth($depth=0) {
		if($this->owner->parent === null) {
			return $depth;
		}
		return $this->owner->parent->getDepth(++$depth);
	}
	
	/**
	 * get all parent ids recursively
	 * @param array $parent_ids
	 * @return type 
	 */
	public function getAncestorIds($parent_ids=array()) {
		if($this->owner->parent === null) {
			return $parent_ids;
		}		
		$parent_ids[] = $this->owner->parent_id;
		return $this->owner->parent->getAncestorIds($parent_ids);
	}
	
	/**
	 * get ALL children and their subchildren and so on
	 * level defines the depth, use null for maximum depth leafs
	 */
	public function subchildren($subchildren=array(),$level=null) {
		//dd(4);
		
		$children = $this->owner->children;
		if($level===0) {
			return array();
		}
		
		if(empty($children)) {
			return array();
		}
		//d(get_class($this->owner));
		//d($this->owner->id);
		//assign by id
		foreach($children as & $child) {
			$id = $child->id;
			//if( !isset($subchildren[$id]) ) {
				$subchildren[$id] = $child;
				$restchildren = $child->subchildren($subchildren,--$level);
				if(is_array($restchildren)) {
					$subchildren = $subchildren + $restchildren;
					//$subchildren = array_merge($subchildren, $restchildren);
				}
			//}
		}
		//d($subchildren);
		return $subchildren;
	}
	
	public function hasChildren() {
		$children = $this->owner->children;
		return !empty($children);
	}
	
	public function subchildrenRelations($relation) {
		$subchildren = $this->subchildren();
		//dd($relation);
		$models = array();
		if(is_array($subchildren)) {
			foreach($subchildren as & $subchild) {
				$relateds = $subchild->$relation;
				foreach($relateds as & $related) {
					$id = $related->id;
					if( !isset($models[$id]) ) {
						$models[$id] = & $related;
					}
				}
			}
		}
		return $models;
	}
	
	public function topNodes() {
		$criteria = new CDbCriteria;
		$criteria->condition = 'parent_id IS NULL';
		return $this->owner->findAll($criteria);
	}
	
	/**
	 * //CHtml::listData($models, $valueField, $textField)
	 * maybe the variable list data could be a protected property of tree behavior (global style to make life easier)
	 * @return array
	 */
	public function listData() {
		$topNodes = $this->topNodes();

		$listData = array();
        foreach ($topNodes as $topNode) {
			$listData[$topNode->id] = $topNode->name;
            //$this->_categories[$parent->id] = $parent->name;
            //$this->_genealogy($parent->id, 0);
			$listData = $listData + $topNode->listSubchildren($listData);
        }
		
        return $listData;
    }
	
	public function listSubchildren($listData) {
		$children = $this->owner->children;
		
		foreach($children as $child) {
			$listData[$child->id] = str_repeat('-', $child->getDepth()) .' '. $child->name;
			$listData = $listData + $child->listSubchildren($listData);
		}
		
		return $listData;
	}
}