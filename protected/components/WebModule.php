<?php
/**
 * Description of WebModule
 *
 * @author pligor
 */
abstract class WebModule extends CWebModule {
    /**
     * typically the assets are stores inside module 'assets' folder
     * @var string
     */
    protected $_assetsUrl = null;
    
    /**
     * uses asset manager to publish the files and return the published url
     * force copy is enabled by default
     * @return string
     */
    public function getAssetsUrl() {
        if($this->_assetsUrl === null) {
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish( //http://www.yiiframework.com/doc/api/1.1/CAssetManager#publish-detail
                    Yii::getPathOfAlias($this->id .'.assets'),  //path
                    false, //hashByName
                    -1, //level
                    true //forceCopy
            );
        }
        return $this->_assetsUrl;
    }
}

?>
