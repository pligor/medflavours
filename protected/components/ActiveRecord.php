<?php
/**
 * Description of ActiveRecord
 *
 * @author pligor
 */
class ActiveRecord extends CActiveRecord {
	
	public function retrieveColumn($column='id') {
		return Yii::app()->db->createCommand()
		->select($column)
		->from( $this->getTableName() )
		->queryColumn();
	}
	
	public function getIndexedHasMany($relation) {
		$relations = $this->$relation;
		
		$indexed = array();
		if(is_array($relations)) {
			foreach($relations as & $relation) {
				$indexed[$relation->id] = & $relation;
			}
		}
		else {
			throw new CException('do not use '.__FUNCTION__.' with relations other than HAS MANY');
		}
		return $indexed;
	}
	
	/**
	 * @deprecated version, correct before use
	 * @return type 
	 */
	public function getOnlyDbAttributes() {
		$allAttrs = $this->getAttributes(true);
		$noDbAttrs = $this->getAttributes(null);
		return array_diff_assoc($allAttrs, $noDbAttrs);
	}
	
	public function getTableName() {
		return Yii::app()->db->getSchema()->getTable( $this->tableName() )->name;
	}
	
	/**
	 *
	 * @return array the loaded fixture rows indexed by row aliases (if any). False is returned if the table does not have a fixture 
	 */
	public function loadFixtureData() {
		$fixtureManager = new CDbFixtureManager();
		$fixtureManager->basePath = Yii::getPathOfAlias('application.data');
		
		$tableName = $this->getTableName();
		
		$fixtureManager->truncateTable($tableName);
		return $fixtureManager->loadFixture($tableName);
	}
	
	/**
	 * Note: no problem to alter a table which does not have an auto increment column, nothing happens
	 * @param string $tableName
	 * @throws Exception
	 */
	public static function resetAutoIncrement($tableName) {
		
		if( !in_array($tableName, Yii::app()->db->getSchema()->tableNames) ) {
			$message = Yii::t('', 'You are trying to alter a table which does not belongs inside the database');
			throw new Exception($message);
		}
		
		$based = 1;
		$sql = 'ALTER TABLE '. $tableName .' AUTO_INCREMENT = :based ;';
		
		$command = Yii::app()->db->createCommand($sql);
		$command->bindValue(':based', $based);
		$command->execute();
	}
}