<?php
/**
 * Description of AdminForm
 *
 * @author pligor
 */
class GForm extends CForm {
	//public $id = null;
	
	public function __construct($config, $model=null, $parent=null) {
		//$this->buttonElementClass = 'SaveFormButtonElement';
		parent::__construct($config, $model, $parent);
	}
	
	public function init() {
		parent::init();
	}
	
	public function render() {
		$output = $this->renderBegin();
		if(isset($this->elements['gposition'])) {
			$this->elements['gposition']->attributes['form'] = $this->activeFormWidget;
		}
		//$this->elements['dummy']->attributes['form'] = $this->activeFormWidget;
		//d($this->elements);
		/*foreach($this->elements as $key => $element) {
			print $key;
			print "<br/>";
		}
		print "<hr/>";*/
		$output .= $this->renderBody();
		$output .= $this->renderEnd();
		return $output;
	}
}