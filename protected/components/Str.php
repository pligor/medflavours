<?php
/**
 * Description of Str
 *
 * @author pligor
 */
class Str extends CComponent {
	/**
	 * eliminate whitespace
	 * @param string $str
	 */
	public static function noWhitespace($str) {
		$str = self::noRedundantSpaces($str);
		return str_replace(' ', '', $str);
	}
	
	/**
	 * remove all redundant spaces
	 * @param string $str
	 */
	public static function noRedundantSpaces($str) {
		$strArray= array();
		$tok = strtok($str, self::spaces);
		while($tok !== false) {
			$strArray[] = $tok;
			$tok = strtok(self::spaces);
		}
		return implode(' ', $strArray);
	}
}