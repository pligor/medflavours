<?php
/**
 * Description of Helper
 *
 * @author pligor
 */
class Hlp extends CComponent {
	public static function veryRandom() {
		return md5( microtime(true)*mt_rand() );
	}
	
	/**
	 * A more suited isEmtpy function than php empty(): http://kr2.php.net/manual/en/function.empty.php
	 * The following things are considered to be empty:
	 * whitespace
	 * NULL
	 * FALSE
	 * array()
	 * 
	 * @param mixed $var
	 */
	public static function isEmpty($var) {
		if(is_object($var)) return false;
		if($var===null) return true;
		if($var===false) return true;
		if(is_array($var)) return empty($var);
		$var = trim($var);
		return ($var=='');
	}
	
	public static function icon($imagefile) {
		return Yii::app()->baseUrl .'/'. Yii::app()->params['imageFolder'] .'/'. 'icons' . '/'. $imagefile;
	}
}