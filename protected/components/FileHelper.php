<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileHelper
 *
 * @author pligor
 */
class FileHelper extends CComponent {
	/**
	 * SLOW, use with caution 
	 * @param string $url
	 * @return \CClientScript
	 * @throws CException 
	 */
	public static function safeRegisterScriptFile($url) {
		$filepath = self::getPathFromUrl($url);
		if(is_file($filepath)) {
			return Yii::app()->clientScript->registerScriptFile($url);
		}
		throw new CException($url.' was not published successfully');
	}
	
	/**
	 * get real path from url
	 * @param string $url 
	 */
	public static function getPathFromUrl($url) {
		$basepath = Yii::app()->getAssetManager()->basePath;

		$c=0;
		$found = false;
		for($repeats=strlen($basepath);$repeats>0;$repeats--) {
			$basepath_copy = substr($basepath,$c++);
			//d($basepath_copy);
			if( strpos($url, $basepath_copy) !== false) {
				$found=true;
				break;
			}
			//$url_copy = ;
		}
		
		if($found === false) {
			throw new CException('please correct this '. __FUNCTION__);
		}
		
		$finalpath = $basepath . substr($url, strlen($basepath_copy));
		//d($finalpath);
		return $finalpath;
	}
}