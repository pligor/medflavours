<?php
/**
 * Test class for Product model
 *
 * @author subdee
 */

class ProductTest extends CDbTestCase{
    
    public $fixtures = array(
        'categories' => 'Category',
        'suppliers' => 'Supplier',
        'locations' => 'Location',
    );
    
    public function testSearchStock() {
        
        $product = new Product;
        $product->setAttributes(array(
            'category_id' => $this->categories['sample1']['id'],
            'supplier_id' => $this->suppliers['sample1']['id'],
            'location_id' => $this->locations['sample1']['id'],
            'name' => 'Product1',
            'price' => 100,
            'is_active' => true,
            'stock' => 2,
        ),false);
        $this->assertTrue($product->save(false));
        
        $product = Product::model()->findByPk($product->id);
        $this->assertTrue($product instanceof Product);
        $this->assertEquals(2, $product->stock);
        
        $data = $product->searchStock();
        $this->assertTrue($data instanceof CActiveDataProvider);
        $this->assertTrue($data->itemCount == 1);
    }
}

?>
