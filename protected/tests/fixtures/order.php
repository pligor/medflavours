<?php

return array(
    'order_sts' => array(
        'customer_id' => 1,
        'status' => 2,
        'date_entered' => date(DATE_ISO8601),
    ),
    'order_stw' => array(
        'customer_id' => 2,
        'status' => 3,
        'date_entered' => date(DATE_ISO8601),
    ),
    'order_atw' => array(
        'customer_id' => 3,
        'status' => 4,
        'date_entered' => date(DATE_ISO8601),
    ),
    'order_open' => array(
        'customer_id' => 4,
        'status' => 1,
        'date_entered' => date(DATE_ISO8601),
    ),
    'order_completed' => array(
        'customer_id' => 5,
        'status' => 5,
        'date_entered' => date(DATE_ISO8601),
    ),
);
