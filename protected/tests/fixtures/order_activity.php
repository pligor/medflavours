<?php

return array(
    'sample1' => array(
        'order_id' => 1,
        'activity_id' => 1,
        'amount' => 10,
    ),
    'sample2' => array(
        'order_id' => 2,
        'activity_id' => 1,
        'amount' => 7,
    ),
    'sample3' => array(
        'order_id' => 3,
        'activity_id' => 1,
        'amount' => 1,
    ),
    'sample_pending' => array(
        'order_id' => 5,
        'activity_id' => 1,
        'amount' => 9,
    ),
);
