<?php

return array(
    'sample1' => array(
        'order_id' => 1,
        'product_id' => 1,
        'amount' => 10,
    ),
    'sample2' => array(
        'order_id' => 2,
        'product_id' => 1,
        'amount' => 5,
    ),
    'sample_open' => array(
        'order_id' => 4,
        'product_id' => 1,
        'amount' => 4,
    ),
);
