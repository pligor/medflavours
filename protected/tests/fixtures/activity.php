<?php

return array(
    'sample1' => array(
        'name' => 'activity 1',
        'description' => 'description 1',
        'price' => '99.99',
        'is_active' => true,
        'long_lat' => '37.9756, 23.7345',
        'voucher_info' => 'voucher 1',
        'activity_type' => 1,
        'location_id' => 1,
    ),
);
