<?php

return array(
    'customer1' => array(
        'email' => 'customer1@example.com',
        'fullname' => 'customer one',
        'phone' => '1234567890',
    ),
    'customer2' => array(
        'email' => 'customer2@example.com',
        'fullname' => 'customer two',
        'phone' => '0987654321',
    ),
    'customer3' => array(
        'email' => 'customer3@example.com',
        'fullname' => 'customer three',
        'phone' => '0147258369',
    ),
    'customer_open' => array(
        'email' => 'customer4@example.com',
        'fullname' => 'customer four',
        'phone' => '0001112223',
    ),
    'customer_pending' => array(
        'email' => 'customer5@example.com',
        'fullname' => 'customer five',
        'phone' => '3344455566',
    ),
);
