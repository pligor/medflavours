<?php

class AdminTest extends WebTestCase {

    public function setUp() {
        parent::setUp();
//        $this->markTestSkipped();
    }
    
    public function testLogin() {
        $this->open('admin');

        $this->assertElementPresent('name=LoginForm[username]');
        $this->type('name=LoginForm[username]', 'admin');
        $this->click("//input[@id='submit']");
        $this->waitForTextPresent('Password cannot be blank.');
        $this->type('name=LoginForm[password]', 'U5VsER6774b7fbi');
        $this->clickAndWait("//input[@id='submit']");
        $this->assertTextNotPresent('Password cannot be blank.');
        $this->assertTextPresent('ver 3.0');
    }

}
