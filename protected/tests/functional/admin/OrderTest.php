<?php

class OrderTest extends WebTestCase {

    public $fixtures = array(
        'categories' => 'Category',
        'locations' => 'Location',
        'suppliers' => 'Supplier',
        'products' => 'Product',
        'activities' => 'Activity',
        'packages' => 'Package',
        'customers' => 'Customer',
        'orders' => 'Order',
        'orderActivities' => 'OrderActivity',
        'orderProducts' => 'OrderProduct',
        'orderPackages' => 'OrderPackage',
    );

    public function setUp() {
        parent::setUp();
//        $this->markTestSkipped();
    }

    public function testOrders() {
        $this->_login();

        //View open orders
        $this->open('admin/order/index');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order was not found');
        
        //Move open order to pending
        $this->clickAndWait('css=a.next');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order in sts was not found');
        
        //Move open order to pending
        $this->clickAndWait('css=tr.even > td.button-column > a.next');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order in stw was not found');
        
        //Move open order to pending
        $this->clickAndWait('css=tr.even > td.button-column > a.next');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order in atw was not found');
        
        //Move pending order to complete
        $this->clickAndWait('css=tr.even > td.button-column > a.next');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order in complete was not found');
        
        //Move completed order back
        $this->clickAndWait('css=tr.odd > td.button-column > a.previous');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order in atw was not found');
        
        //Move completed order back
        $this->clickAndWait('css=tr.even > td.button-column > a.previous');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order in stw was not found');
        
        //Move completed order back
        $this->clickAndWait('css=tr.even > td.button-column > a.previous');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order in sts was not found');
        
        //Move completed order back
        $this->clickAndWait('css=tr.even > td.button-column > a.previous');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order in open was not found');
        
        //Delete complete order
        $this->click('css=a.delete.tooltip');
        $this->assertConfirmation('Are you sure you want to delete this item?');
        sleep(1);
        $this->assertTextNotPresent($this->customers['customer_open']['fullname'], 'Customer with name: '.$this->customers['customer_open']['fullname'].' in complete was found');
    }
    
    public function testCustomers() {
        $this->_login();
        
        //View customers
        $this->open('admin/order/customers');
        $this->assertTextPresent($this->customers['customer_open']['fullname'], 'Customer with open order was not found');
        
        //Edit customer two
        $this->clickAndWait('css=.items tbody tr:nth-child(2) .button-column a:nth-child(1)');
        $this->assertTextPresent('Fullname');
        
        $this->type('name=Customer[fullname]','customer six');
        $this->type('name=Customer[email]','customer6@example.com');
        $this->clickAndWait("//input[@type='submit']");
        $this->assertTextPresent('customer six');
        
        //View customer six's orders
        $this->clickAndWait('css=.items tbody tr:nth-child(2) .button-column a:nth-child(3)');
        $this->assertTextPresent('2');
    }

    private function _login() {
        $this->open('admin');

        $this->assertElementPresent('name=LoginForm[username]');
        $this->type('name=LoginForm[username]', 'admin');
        $this->type('name=LoginForm[password]', '123');
        $this->clickAndWait("//input[@id='submit']");
        
        $this->assertTextPresent('Administration Panel for Mediterranean Flavours');
    }

}
