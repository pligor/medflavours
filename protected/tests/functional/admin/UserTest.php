<?php

class UserTest extends WebTestCase {

    public $fixtures = array(
    );

    public function setUp() {
        parent::setUp();
//        $this->markTestSkipped();
    }

    public function testCreateUser() {
        $this->_login();
        
        $this->open('admin/user/index');
        $this->assertTextPresent('admin');
        
        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Username');
        
        $this->type('name=User[username]', 'tester');
        $this->type('name=User[password]', 'tester');
        $this->select('name=User[role]', 'value=2');
        $this->clickAndWait("//input[@type='submit']");
        $this->assertTextPresent('tester');
    }
    
    public function testLoginUser() {
        $this->open('admin');
        $this->assertTextPresent('Remember Me');
        
        $this->type('name=LoginForm[username]', 'tester');
        $this->type('name=LoginForm[password]', 'tester');
        $this->clickAndWait("//input[@type='submit']");
        $this->assertTextNotPresent('Users');
        
        $this->clickAndWait('link=Log out');
        $this->assertTextPresent('Remember Me');
    }

    private function _login() {
        $this->open('admin');

        $this->assertElementPresent('name=LoginForm[username]');
        $this->type('name=LoginForm[username]', 'admin');
        $this->type('name=LoginForm[password]', '123');
        $this->clickAndWait("//input[@id='submit']");
        
        $this->assertTextPresent('Administration Panel for Mediterranean Flavours');
    }

}
