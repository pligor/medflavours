<?php

class ContentTest extends WebTestCase {

    public $fixtures = array(
        'categories' => 'Category',
        'locations' => 'Location',
        'suppliers' => 'Supplier',
        'products' => 'Product',
        'activities' => 'Activity',
        'packages' => 'Package',
        'staticContent' => 'StaticContent',
    );

    public function setUp() {
        parent::setUp();
//        $this->markTestSkipped();
    }

    public function testCategories() {
        $this->_login();

        $this->open('admin/content/categories');
        $this->assertTextPresent($this->categories['sample1']['name'], 'Text category 1 was not present');

        //Add category 2
        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Parent');

        $this->type('name=Category[name]', 'category 2');
        $this->type('name=Category[description]', 'description 2');
        $this->clickAndWait("//input[@name='goBack']");
        $this->assertValue('name=Category[name]', 'category 2');

        //Edit category 2 and make it category 3
        $this->type('name=Category[name]', 'category 3');
        $this->type('name=Category[description]', 'description 3');
        $this->clickAndWait("//input[@name='save']");
        $this->assertTextPresent('description 3');

        //Delete category 3
        $this->click("css=div#yw0.grid-view table.items tbody tr.odd td.button-column a.delete");
        $this->assertConfirmation('This will delete all subcategories and all related products. Are you sure you wish to delete this category?');
        sleep(1);
        $this->assertTextNotPresent('category 3');

        //Add category 4 child of category 1
        $this->click('id=addButton');
        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Parent');

        $this->select('name=Category[parent_id]', 'value=1');
        $this->type('name=Category[name]', 'category 4');
        $this->type('name=Category[description]', 'description 4');
        $this->clickAndWait("//input[@name='save']");
        $this->assertTextPresent('description 1');

        $this->clickAndWait("link=category 1");
        $this->assertTextPresent('category 4');

        $this->clickAndWait("link=Go back");
        $this->click("css=div#yw0.grid-view table.items tbody tr.odd td.button-column a.delete");
        $this->assertConfirmation('This will delete all subcategories and all related products. Are you sure you wish to delete this category?');
        sleep(1);
        $this->assertTextNotPresent('category 2');
    }

    public function testLocations() {
        $this->_login();

        $this->open('admin/content/locations');
        $this->assertTextPresent($this->locations['sample1']['name']);

        //Add location
        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Parent');

        $this->type('name=Location[name]', 'location 2');
        $this->type('name=Location[description]', 'description 2');
        $this->clickAndWait("//input[@name='goBack']");
        $this->assertValue('name=Location[name]', 'location 2');

        //Edit location
        $this->type('name=Location[name]', 'location 3');
        $this->type('name=Location[description]', 'description 3');
        $this->clickAndWait("//input[@name='save']");
        $this->assertTextPresent('description 3');

        //Delete location
        $this->click("css=div#yw0.grid-view table.items tbody tr.odd td.button-column a.delete");
        $this->assertConfirmation('This will delete all sublocations and all related products. Are you sure you wish to delete this location?');
        sleep(1);
        $this->assertTextNotPresent('description 3');
    }

    public function testSuppliers() {
        $this->_login();

        $this->open('admin/content/suppliers');
        $this->assertTextPresent($this->suppliers['sample1']['name']);

        //Add supplier
        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Supplier');

        $this->type('name=Supplier[name]', 'supplier 2');
        $this->type('name=Supplier[email]', 'email 2');
        $this->type('name=Supplier[address]', 'address 2');
        $this->type('name=Supplier[phone]', 'phone 2');
        $this->clickAndWait("//input[@name='goBack']");
        $this->assertValue('name=Supplier[name]', 'supplier 2');

        //Edit supplier
        $this->type('name=Supplier[name]', 'supplier 3');
        $this->type('name=Supplier[email]', 'email 3');
        $this->type('name=Supplier[address]', 'address 3');
        $this->type('name=Supplier[phone]', 'phone 3');
        $this->clickAndWait("//input[@name='save']");
        $this->assertTextPresent('address 3');

        //Delete supplier
        $this->click("css=div#yw0.grid-view table.items tbody tr.odd td.button-column a.delete");
        $this->assertConfirmation('This will delete all supplier\'s products. Are you sure you wish to delete this supplier?');
        sleep(1);
        $this->assertTextNotPresent('phone 3');
    }

    public function testProducts() {
        $this->_login();

        $this->open('admin/content/products');
        $this->assertTextPresent($this->products['sample1']['name']);

        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Name');

        $this->type('name=Product[name]', 'product 2');
        $this->type('name=Product[price]', '100.00');
        $this->check('name=Product[is_active]');
        $this->clickAndWait("//input[@name='goBack']");
        $this->assertValue('name=Product[name]', 'product 2');

        //Edit Product
        $this->type('name=Product[name]', 'product 3');
        $this->type('name=Product[price]', '100.00');
        $this->check('name=Product[is_active]');
        $this->clickAndWait("//input[@name='save']");
        $this->assertTextPresent('product 3');

        //Delete Product
        $this->click("css=div#yw0.grid-view table.items tbody tr.odd td.button-column a.delete");
        $this->assertConfirmation('Are you sure you want to delete this item?');
        sleep(1);
        $this->assertTextNotPresent('product 3');
    }

    public function testActivities() {
        $this->_login();

        $this->open('admin/content/activities');
        $this->assertTextPresent($this->activities['sample1']['name']);

        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Location');

        $this->type('Activity[name]', 'activity 2');
        $this->type('Activity[description]', 'description 2');
        $this->type('Activity[price]', '199.99');
        $this->type('Activity[voucher_info]', 'voucher 2');
        $this->type('Activity[long_lat]', '37.9756, 23.7345');
        $this->click('id=periodFrom_0');
        $this->click('link=25');
        $this->click('id=periodTo_0');
        $this->click('link=28');
        $this->clickAndWait("//input[@name='goBack']");
        $this->assertValue('name=Activity[name]', 'activity 2');

        //Edit Activity
        $this->type('Activity[name]', 'activity 3');
        $this->type('Activity[description]', 'description 3');
        $this->type('Activity[price]', '159.99');
        $this->type('Activity[voucher_info]', 'voucher 3');
        $this->type('Activity[long_lat]', '37.9756, 23.7345');
        $this->click('id=periodFrom_0');
        $this->click('link=21');
        $this->click('id=periodTo_0');
        $this->click('link=25');
        $this->clickAndWait("//input[@name='save']");
        $this->assertTextPresent('activity 3');

        //Delete Activity
        $this->click("css=div#yw0.grid-view table.items tbody tr.even td.button-column a.delete");
        $this->assertConfirmation('Are you sure you want to delete this item?');
        sleep(1);
        $this->assertTextNotPresent('activity 3', 'Could not delete activity 3');
    }

    public function testPackages() {
        $this->_login();

        $this->open('admin/content/packages');
        $this->assertTextPresent($this->packages['sample1']['name']);

        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Name');

        $this->type('name=Package[name]', 'package 2');
        $this->type('name=Package[price]', '100.00');
        $this->type('name=Package[small_description]', 'small 2');
        $this->type('name=Package[full_description]', 'large 2');
        $this->clickAndWait("//input[@name='goBack']");
        $this->assertValue('name=Package[name]', 'package 2');

        //Edit Product
        $this->type('name=Package[name]', 'package 3');
        $this->type('name=Package[price]', '200.00');
        $this->type('name=Package[small_description]', 'small 3');
        $this->type('name=Package[full_description]', 'large 3');
        $this->clickAndWait("//input[@name='save']");
        $this->assertTextPresent('package 3');

        //Delete Product
        $this->click("css=div#yw0.grid-view table.items tbody tr.even td.button-column a.delete");
        $this->assertConfirmation('Are you sure you wish to delete this package?');
        sleep(1);
        $this->assertTextNotPresent('package 3');
    }

    public function testStaticContent() {
        $this->_login();

        $this->open('admin/content/staticContent');
        $this->assertTextPresent($this->staticContent['sample1']['content']);

        $this->clickAndWait('id=addButton');
        $this->assertTextPresent('Content');

        $this->type('name=StaticContent[content]', 'static content 2');
        $this->check('name=StaticContent[show_in_menu]');
        $this->type('name=StaticContent[menu_title]', 'static content menu 2');
        $this->clickAndWait("//input[@name='goBack']");
        $this->assertValue('name=StaticContent[content]', 'static content 2');

        //Edit Static Content
        $this->type('name=StaticContent[content]', 'static content 3');
        $this->check('name=StaticContent[show_in_menu]');
        $this->type('name=StaticContent[menu_title]', 'static content menu 3');
        $this->clickAndWait("//input[@name='save']");
        $this->assertTextPresent('static content 3');

        //Delete Static Content
        $this->click("css=div#yw0.grid-view table.items tbody tr.even td.button-column a.delete");
        $this->assertConfirmation('Are you sure you wish to delete this static content?');
        sleep(1);
        $this->assertTextNotPresent('static content 3');
    }

    private function _login() {
        $this->open('admin');

        $this->assertElementPresent('name=LoginForm[username]');
        $this->type('name=LoginForm[username]', 'admin');
        $this->type('name=LoginForm[password]', '123');
        $this->clickAndWait("//input[@id='submit']");

        $this->assertTextPresent('Administration Panel for Mediterranean Flavours');
    }

}
